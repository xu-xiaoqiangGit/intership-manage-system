/*
 Navicat Premium Data Transfer

 Source Server         : xxx
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : localhost:3306
 Source Schema         : internship_system

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 15/04/2024 22:49:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for classes
-- ----------------------------
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `classes_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级名称',
  `classes_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级编号',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unx_classes_code`(`classes_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of classes
-- ----------------------------
INSERT INTO `classes` VALUES (1, '计算机学院一班', 'class_jsj_1', b'0', '2023-11-18 16:39:17', '2023-11-18 16:46:11');
INSERT INTO `classes` VALUES (2, '计算机学院二班', 'class_jsj_2', b'0', '2023-11-18 16:39:38', '2023-11-18 16:46:11');
INSERT INTO `classes` VALUES (3, '艺术学院学院二班', 'c_ys_2', b'0', '2023-11-18 16:39:38', '2023-11-18 16:48:13');
INSERT INTO `classes` VALUES (4, '财经学院学院二班', 'c_cj_2', b'0', '2023-11-18 16:39:38', '2023-11-18 16:48:13');
INSERT INTO `classes` VALUES (5, '会计系一班', 'claess-kj-1', b'0', '2023-11-18 16:48:46', '2023-11-18 16:48:46');
INSERT INTO `classes` VALUES (7, '计算机1班', 'CLASS_JSJ_01', b'0', '2023-12-26 23:21:16', '2023-12-26 23:21:16');
INSERT INTO `classes` VALUES (8, '计算机3班', 'class_jsj_3', b'0', '2023-12-26 23:54:22', '2023-12-26 23:54:22');

-- ----------------------------
-- Table structure for enterprise
-- ----------------------------
DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE `enterprise`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `enterprise_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '企业名称',
  `industry` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '行业',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `enterprise_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业logo',
  `phone_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unx_enterprise_name`(`enterprise_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '企业表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enterprise
-- ----------------------------
INSERT INTO `enterprise` VALUES (1, '阿里巴巴集团', '计算机服务业', '浙江省杭州市萧山区', 'http://localhost:8080/internship/file/download/f06a86dd10854b74b46d922d33e7276f.jpg', '13624972614', b'0', '2023-11-14 23:50:12', '2023-11-14 23:50:12');
INSERT INTO `enterprise` VALUES (2, '腾讯计算机有限公司', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'0', '2023-11-15 00:10:23', '2023-11-15 00:10:23');
INSERT INTO `enterprise` VALUES (4, '腾讯计算机有限公司1', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 23:40:27');
INSERT INTO `enterprise` VALUES (5, '腾讯计算机有限公司2', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 23:40:25');
INSERT INTO `enterprise` VALUES (6, '腾讯计算机有限公司3', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 15:34:23');
INSERT INTO `enterprise` VALUES (7, '腾讯计算机有限公司4', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 15:34:22');
INSERT INTO `enterprise` VALUES (8, '腾讯计算机有限公司5', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 15:34:22');
INSERT INTO `enterprise` VALUES (9, '腾讯计算机有限公司6', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 15:34:21');
INSERT INTO `enterprise` VALUES (10, '腾讯计算机有限公司7', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 15:34:21');
INSERT INTO `enterprise` VALUES (11, '腾讯计算机有限公司8', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 15:34:17');
INSERT INTO `enterprise` VALUES (12, '腾讯计算机有限公司9', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 15:34:19');
INSERT INTO `enterprise` VALUES (13, '腾讯计算机有限公司10', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 15:34:20');
INSERT INTO `enterprise` VALUES (14, '腾讯计算机有限公司11', '互联网服务业', '广东省深圳市南山区', 'http://localhost:8080/internship/file/download/35fa495c244b410893c63cd40254b963.jpg', '15888113546', b'1', '2023-11-15 00:10:23', '2023-12-03 23:40:19');

-- ----------------------------
-- Table structure for enterprise_position
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_position`;
CREATE TABLE `enterprise_position`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `position_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `enterprise_id` bigint UNSIGNED NOT NULL COMMENT '所属企业',
  `salary_start` decimal(10, 2) UNSIGNED NOT NULL COMMENT '起薪',
  `salary_max` decimal(10, 2) UNSIGNED NOT NULL COMMENT '最高薪',
  `position_description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位描述',
  `position_city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位城市',
  `position_address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位地址',
  `phone_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `welfare` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位福利',
  `position_status` tinyint UNSIGNED NOT NULL DEFAULT 1 COMMENT '岗位状态（0-已停止, 1-招聘中）',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `enterprise_id_fk`(`enterprise_id` ASC) USING BTREE,
  CONSTRAINT `enterprise_id_fk` FOREIGN KEY (`enterprise_id`) REFERENCES `enterprise` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '企业岗位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enterprise_position
-- ----------------------------
INSERT INTO `enterprise_position` VALUES (1, 'Java开发工程师', 1, 20000.00, 100000.00, '工作职责 1、参与信贷领域（消费金融、供应链金融、小微企业金融、支付钱包）等系统分析与设计工作； 2、根据开发规范与流程独立完成模块的设计、编码、测试以及相关文档； 3、独立解决开发中遇到的难点问题； 4、参与系统稳定性、性能和扩展性调试； 任职资格 1、计算机或相关专业全日制本科及以上学历，3年以上Java开发经验，Java基础扎实，良好的代码风格，熟悉基于J2EE的相关开源技术以及框架（springboot,Spring,springmvc,springcloud,Struts2, mybatis），能够使用Junit编写测试代码； 2、熟悉HTML、CSS、XML、JavaScript、JSON、WebSocket等Web开发技术； 3、熟悉SQL，熟悉Oracle/MySQL等数据库，并具有一定的SQL优化能力； 4、熟悉Memcached、Redis、MongoDB等常用NoSQL解决方案、了解各自的优缺点以及使用场景者优先； 5、熟悉Tomcat等应用服务器的部署和配置；熟悉Linux操作系统常用命令； 6、具有消费金融项目经验者优先； 7、强烈的责任心和团队合作能力，性格开朗，善于沟通，良好的学习能力，逻辑思维能力并且敢于创新和接受挑战；', '深圳', '深圳南山区深圳湾科技生态园10栋A座5层OFC', '15962621651', '五险一金 带薪年假 免费班车 餐补 交通补助 包吃 节日福利', 0, b'0', '2023-11-18 11:10:33', '2024-02-14 00:28:25');
INSERT INTO `enterprise_position` VALUES (2, 'Java开发工程师', 1, 20000.00, 100000.00, '工作职责 1、参与信贷领域（消费金融、供应链金融、小微企业金融、支付钱包）等系统分析与设计工作； 2、根据开发规范与流程独立完成模块的设计、编码、测试以及相关文档； 3、独立解决开发中遇到的难点问题； 4、参与系统稳定性、性能和扩展性调试； 任职资格 1、计算机或相关专业全日制本科及以上学历，3年以上Java开发经验，Java基础扎实，良好的代码风格，熟悉基于J2EE的相关开源技术以及框架（springboot,Spring,springmvc,springcloud,Struts2, mybatis），能够使用Junit编写测试代码； 2、熟悉HTML、CSS、XML、JavaScript、JSON、WebSocket等Web开发技术； 3、熟悉SQL，熟悉Oracle/MySQL等数据库，并具有一定的SQL优化能力； 4、熟悉Memcached、Redis、MongoDB等常用NoSQL解决方案、了解各自的优缺点以及使用场景者优先； 5、熟悉Tomcat等应用服务器的部署和配置；熟悉Linux操作系统常用命令； 6、具有消费金融项目经验者优先； 7、强烈的责任心和团队合作能力，性格开朗，善于沟通，良好的学习能力，逻辑思维能力并且敢于创新和接受挑战；', '深圳', '深圳南山区深圳湾科技生态园10栋A座5层OFC', '15962621651', '五险一金 带薪年假 免费班车 餐补 交通补助 包吃 节日福利', 0, b'0', '2023-11-18 11:10:33', '2024-02-14 00:28:25');
INSERT INTO `enterprise_position` VALUES (3, '前端开发工程师', 2, 10000.00, 20000.00, '【岗位职责】\n1、负责前端项目的设计、开发与维护；\n2、推动建立前端的技术标准和规范，推动开发、测试、部署等最佳实践，执行并帮助改进；\n3、参与前端框架和工具的设计和维护；\n4、技术攻坚，指导和帮助团队成员在业务中成长；\n【能力要求】\n1、HTML/CSS/JavaScript 基础扎实，对前端领域一个或多个方向有研究\n2、深入理解 MVVM 框架（如：Vue、React 等），并基于上述框架主导过大中型移动端或者中后台项目的架构及开发\n3、熟悉 NPM/Webpack/Rollup 等工具，对工程化有深入理解\n4、熟悉至少一种服务端开发语言，如 Node.js、Python、Java、Go，了解 HTTP 协议相关的基础知识\n5、对用户体验有较强的敏感度\n6、具备良好的沟通、规划和项目推动能力，以及独立解决技术难题的能力\n【加分项】\n1、有支付金融业务研发经验\n2、前端基础设施/框架开发经验\n3、推动老旧系统技术改造的经验\n4、有前端作品和开源项目（附上 Github 地址）', '深圳', '深圳福田区深圳新一代产业园2栋19楼', '15962621651', '五险一金 带薪年假', 1, b'0', '2023-11-18 11:10:33', '2023-11-18 11:41:01');
INSERT INTO `enterprise_position` VALUES (4, 'Java开发实习生', 1, 5000.00, 10000.00, '工作职责 1、参与信贷领域（消费金融、供应链金融、小微企业金融、支付钱包）等系统分析与设计工作； 2、根据开发规范与流程独立完成模块的设计、编码、测试以及相关文档； 3、独立解决开发中遇到的难点问题； 4、参与系统稳定性、性能和扩展性调试； 任职资格 1、计算机或相关专业全日制本科及以上学历，3年以上Java开发经验，Java基础扎实，良好的代码风格，熟悉基于J2EE的相关开源技术以及框架（springboot,Spring,springmvc,springcloud,Struts2, mybatis），能够使用Junit编写测试代码； 2、熟悉HTML、CSS、XML、JavaScript、JSON、WebSocket等Web开发技术； 3、熟悉SQL，熟悉Oracle/MySQL等数据库，并具有一定的SQL优化能力； 4、熟悉Memcached、Redis、MongoDB等常用NoSQL解决方案、了解各自的优缺点以及使用场景者优先； 5、熟悉Tomcat等应用服务器的部署和配置；熟悉Linux操作系统常用命令； 6、具有消费金融项目经验者优先； 7、强烈的责任心和团队合作能力，性格开朗，善于沟通，良好的学习能力，逻辑思维能力并且敢于创新和接受挑战；', '深圳', '深圳南山区深圳湾科技生态园10栋A座5层OFC', '15962621651', '五险一金 带薪年假 免费班车 餐补 交通补助 包吃 节日福利', 1, b'0', '2023-11-18 11:10:33', '2023-11-18 11:41:01');
INSERT INTO `enterprise_position` VALUES (5, 'Java开发工程师', 1, 20000.00, 100000.00, '工作职责 1、参与信贷领域（消费金融、供应链金融、小微企业金融、支付钱包）等系统分析与设计工作； 2、根据开发规范与流程独立完成模块的设计、编码、测试以及相关文档； 3、独立解决开发中遇到的难点问题； 4、参与系统稳定性、性能和扩展性调试； 任职资格 1、计算机或相关专业全日制本科及以上学历，3年以上Java开发经验，Java基础扎实，良好的代码风格，熟悉基于J2EE的相关开源技术以及框架（springboot,Spring,springmvc,springcloud,Struts2, mybatis），能够使用Junit编写测试代码； 2、熟悉HTML、CSS、XML、JavaScript、JSON、WebSocket等Web开发技术； 3、熟悉SQL，熟悉Oracle/MySQL等数据库，并具有一定的SQL优化能力； 4、熟悉Memcached、Redis、MongoDB等常用NoSQL解决方案、了解各自的优缺点以及使用场景者优先； 5、熟悉Tomcat等应用服务器的部署和配置；熟悉Linux操作系统常用命令； 6、具有消费金融项目经验者优先； 7、强烈的责任心和团队合作能力，性格开朗，善于沟通，良好的学习能力，逻辑思维能力并且敢于创新和接受挑战；', '深圳', '深圳南山区深圳湾科技生态园10栋A座5层OFC', '15962621651', '五险一金 带薪年假 免费班车 餐补 交通补助 包吃 节日福利', 1, b'0', '2023-11-18 11:10:33', '2024-02-14 00:28:25');
INSERT INTO `enterprise_position` VALUES (6, 'Java开发工程师', 4, 20000.00, 100000.00, '工作职责 1、参与信贷领域（消费金融、供应链金融、小微企业金融、支付钱包）等系统分析与设计工作； 2、根据开发规范与流程独立完成模块的设计、编码、测试以及相关文档； 3、独立解决开发中遇到的难点问题； 4、参与系统稳定性、性能和扩展性调试； 任职资格 1、计算机或相关专业全日制本科及以上学历，3年以上Java开发经验，Java基础扎实，良好的代码风格，熟悉基于J2EE的相关开源技术以及框架（springboot,Spring,springmvc,springcloud,Struts2, mybatis），能够使用Junit编写测试代码； 2、熟悉HTML、CSS、XML、JavaScript、JSON、WebSocket等Web开发技术； 3、熟悉SQL，熟悉Oracle/MySQL等数据库，并具有一定的SQL优化能力； 4、熟悉Memcached、Redis、MongoDB等常用NoSQL解决方案、了解各自的优缺点以及使用场景者优先； 5、熟悉Tomcat等应用服务器的部署和配置；熟悉Linux操作系统常用命令； 6、具有消费金融项目经验者优先； 7、强烈的责任心和团队合作能力，性格开朗，善于沟通，良好的学习能力，逻辑思维能力并且敢于创新和接受挑战；', '深圳', '深圳南山区深圳湾科技生态园10栋A座5层OFC', '15962621651', '五险一金 带薪年假 免费班车 餐补 交通补助 包吃 节日福利', 1, b'0', '2023-11-18 11:10:33', '2024-02-14 00:28:25');
INSERT INTO `enterprise_position` VALUES (7, '中高级java开发工程师', 1, 10000.00, 20000.00, '岗位职责：\n1、参与智能服务的引擎建设(执行引擎，流程引擎，规则引擎)，负责服务端技术方案设计、需求把控及核心功能开发，细化拆解业务需求并实施；\n2、搭建智能化的服务平台，提升用户交互体验。\n3、参与智能数据能力建设，帮助业务精细化运营工具。\n4、指导及参与产品架构规划、性能优化、故障排查和解决、安全加固等工作；\n5、指导初级、中级工程师，组织团队技术分享，促进团队成员共同进步。协调及驱动业务团队间的配合协作；\n6、能够深入理解复杂的业务场景，不断优化重构现有服务架构；围绕现有体系，探索构建智能服务体系平台的可行方案。\n任职要求：\n1、统招本科及以上学历且学信网可查，具备扎实的编程基础和数据结构算法基础，优秀的编程能力和问题解决能力；\n2、5年以上互联网行业研发经验，具备业务核心系统或高并发系统项目开发经验；\n3、精通Java及面向对象设计开发，深入理解面向服务的架构，精通java web开发经验，熟悉JavaWeb应用框架和工具链，熟悉Linux/MySQL/NoSQL等；\n4、优秀的模块设计、抽象能力，能独立的进行系统设计与思考；\n5、熟练掌握网络通信原理，熟悉服务器端，数据端的主流技术或框架；\n6、善于交流，具备良好的团队合作精神和协调沟通能力；\n7、自我驱动，善于钻研，勤于思考总结，结果导向。\n有系统安全防护经验优先，该岗位工作地点在大亚湾', '深圳', '深圳大亚湾西区科技创新园科技路1号创新大厦', '19563151351', '生日福利节日福利团建聚餐带薪年假节假日加班费', 1, b'0', '2023-12-04 00:14:08', '2023-12-04 00:14:08');
INSERT INTO `enterprise_position` VALUES (8, '高级java开发工程师', 2, 20000.00, 100000.00, '参与公司系统建设迭代，保证系统正常运行', '深圳', '深圳福田区财富大厦2208', '15689562131', '生日福利节日福利团建聚餐带薪年假节假日加班费', 1, b'0', '2023-12-04 00:38:40', '2023-12-04 00:38:40');

-- ----------------------------
-- Table structure for internship_apply
-- ----------------------------
DROP TABLE IF EXISTS `internship_apply`;
CREATE TABLE `internship_apply`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `enterprise_position_id` bigint UNSIGNED NOT NULL COMMENT '企业岗位',
  `enterprise_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '企业名称',
  `position_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `student_id` bigint UNSIGNED NOT NULL COMMENT '实习学生',
  `student_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学生学号',
  `teacher_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '执教教师',
  `student_resume_id` bigint UNSIGNED NOT NULL COMMENT '投递简历',
  `audit_reply` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核回复',
  `audit_status` tinyint UNSIGNED NOT NULL DEFAULT 2 COMMENT '审核状态（0-拒绝, 1-通过, 2-审核中）',
  `submit_audit_status` tinyint UNSIGNED NOT NULL DEFAULT 0 COMMENT '送审状态（0-未送审, 1-已送审）',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `enterprise_position_id_fk`(`enterprise_position_id` ASC) USING BTREE,
  INDEX `student_id_fk`(`student_id` ASC) USING BTREE,
  INDEX `student_resume_id_fk`(`student_resume_id` ASC) USING BTREE,
  INDEX `teacher_id_fk`(`teacher_id` ASC) USING BTREE,
  CONSTRAINT `enterprise_position_id_fk` FOREIGN KEY (`enterprise_position_id`) REFERENCES `enterprise_position` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `student_id_fk` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `student_resume_id_fk` FOREIGN KEY (`student_resume_id`) REFERENCES `student_resume` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teacher_id_fk` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实习申请表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of internship_apply
-- ----------------------------
INSERT INTO `internship_apply` VALUES (2, 1, '阿里巴巴集团', 'Java开发工程师', 2, 'C1_lisi', 2, 2, '表现不错，予以通过', 1, 2, b'0', '2023-12-02 22:56:29', '2024-02-14 00:29:12');
INSERT INTO `internship_apply` VALUES (3, 1, '阿里巴巴集团', 'Java开发工程师', 2, 'C1_lisi', 1, 1, NULL, 0, 0, b'0', '2023-12-02 22:56:54', '2023-12-02 23:47:13');
INSERT INTO `internship_apply` VALUES (4, 6, '腾讯计算机有限公司1', 'Java开发工程师', 2, 'C1_lisi', 1, 1, NULL, 1, 0, b'0', '2023-12-02 23:12:44', '2023-12-02 23:48:33');
INSERT INTO `internship_apply` VALUES (5, 2, '阿里巴巴集团', 'Java开发工程师', 2, 'C1_lisi', 2, 1, NULL, 2, 0, b'0', '2023-12-02 23:39:01', '2023-12-02 23:39:01');
INSERT INTO `internship_apply` VALUES (6, 4, '阿里巴巴集团', 'Java开发工程师', 1, 'C1_zhangsan', 1, 3, NULL, 1, 2, b'0', '2023-12-03 17:48:49', '2024-02-14 00:05:33');
INSERT INTO `internship_apply` VALUES (7, 3, '腾讯计算机有限公司', '前端开发工程师', 1, 'C1_zhangsan', 1, 3, NULL, 1, 1, b'0', '2023-12-03 23:32:41', '2024-03-19 17:20:00');
INSERT INTO `internship_apply` VALUES (8, 3, '腾讯计算机有限公司', '前端开发工程师', 8, 'C1_liuwu', 1, 4, '表现不错', 1, 2, b'0', '2023-12-03 23:43:09', '2024-02-13 23:52:52');
INSERT INTO `internship_apply` VALUES (9, 7, '阿里巴巴集团', '中高级java开发工程师', 9, 'test_studnet_01', 4, 6, '测试学生：表现不错，继续加油', 1, 0, b'0', '2023-12-04 00:18:53', '2024-02-13 20:47:56');
INSERT INTO `internship_apply` VALUES (10, 8, '腾讯计算机有限公司', '高级java开发工程师', 2, 'C1_lisi', 5, 7, '演示回复信息：表现不错，再接再厉', 1, 3, b'0', '2023-12-04 00:48:00', '2024-02-13 23:43:28');
INSERT INTO `internship_apply` VALUES (11, 7, '阿里巴巴集团', '中高级java开发工程师', 1, 'C1_zhangsan', 4, 3, '表现不错，再接再厉', 2, 0, b'0', '2024-02-12 14:55:29', '2024-02-12 14:55:29');
INSERT INTO `internship_apply` VALUES (12, 6, '腾讯计算机有限公司1', 'Java开发工程师', 1, 'C1_zhangsan', 5, 3, NULL, 2, 0, b'0', '2024-03-16 22:36:23', '2024-03-16 22:36:23');
INSERT INTO `internship_apply` VALUES (13, 4, '阿里巴巴集团', 'Java开发实习生', 10, 'stu001', 6, 9, NULL, 1, 2, b'0', '2024-04-15 17:31:19', '2024-04-15 22:42:47');
INSERT INTO `internship_apply` VALUES (14, 6, '腾讯计算机有限公司1', 'Java开发工程师', 10, 'stu001', 6, 9, NULL, 2, 0, b'0', '2024-04-15 17:31:34', '2024-04-15 17:31:34');
INSERT INTO `internship_apply` VALUES (15, 3, '腾讯计算机有限公司', '前端开发工程师', 10, 'stu001', 6, 10, NULL, 2, 0, b'0', '2024-04-15 17:37:55', '2024-04-15 17:37:55');
INSERT INTO `internship_apply` VALUES (16, 5, '阿里巴巴集团', 'Java开发工程师', 10, 'stu001', 6, 9, NULL, 1, 0, b'0', '2024-04-15 17:46:35', '2024-04-15 22:38:34');

-- ----------------------------
-- Table structure for internship_audit
-- ----------------------------
DROP TABLE IF EXISTS `internship_audit`;
CREATE TABLE `internship_audit`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `enterprise_position_id` bigint UNSIGNED NOT NULL COMMENT '企业岗位id ',
  `internship_apply_id` bigint UNSIGNED NOT NULL COMMENT '实习申请id ',
  `student_id` bigint UNSIGNED NOT NULL COMMENT '学生id ',
  `teacher_id` bigint UNSIGNED NOT NULL COMMENT '教师id ',
  `audit_status` tinyint UNSIGNED NOT NULL DEFAULT 0 COMMENT '审核状态（0-待审核, 1-通过, 2-不通过, 3-已取消）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `audit_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核备注',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `internship_apply_id_fk`(`internship_apply_id` ASC) USING BTREE,
  INDEX `internship_audit_enterprise_position_id_fk`(`enterprise_position_id` ASC) USING BTREE,
  INDEX `internship_audit_student_id_fk`(`student_id` ASC) USING BTREE,
  INDEX `internship_audit_teacher_id_fk`(`teacher_id` ASC) USING BTREE,
  CONSTRAINT `internship_apply_id_fk` FOREIGN KEY (`internship_apply_id`) REFERENCES `internship_apply` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `internship_audit_enterprise_position_id_fk` FOREIGN KEY (`enterprise_position_id`) REFERENCES `enterprise_position` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `internship_audit_student_id_fk` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `internship_audit_teacher_id_fk` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实习审核表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of internship_audit
-- ----------------------------
INSERT INTO `internship_audit` VALUES (1, 3, 8, 8, 1, 1, NULL, NULL, b'0', '2024-02-13 20:34:09', '2024-02-13 23:52:52');
INSERT INTO `internship_audit` VALUES (2, 8, 10, 2, 5, 0, NULL, '999', b'0', '2024-02-13 21:02:46', '2024-02-13 23:52:42');
INSERT INTO `internship_audit` VALUES (3, 4, 6, 1, 5, 1, NULL, '表现不错，继续加油', b'0', '2024-02-14 00:03:40', '2024-02-14 00:05:33');
INSERT INTO `internship_audit` VALUES (4, 1, 2, 2, 5, 3, NULL, NULL, b'0', '2024-02-14 00:19:01', '2024-02-14 00:28:48');
INSERT INTO `internship_audit` VALUES (5, 1, 2, 2, 5, 1, '麻烦通过吧，许老师', '表现不错，继续加油', b'0', '2024-02-14 00:29:05', '2024-02-14 00:29:30');
INSERT INTO `internship_audit` VALUES (6, 3, 7, 1, 1, 0, NULL, NULL, b'0', '2024-03-19 17:20:00', '2024-03-19 17:20:00');
INSERT INTO `internship_audit` VALUES (7, 4, 13, 10, 6, 1, NULL, NULL, b'0', '2024-04-15 22:42:12', '2024-04-15 22:42:47');

-- ----------------------------
-- Table structure for internship_interview
-- ----------------------------
DROP TABLE IF EXISTS `internship_interview`;
CREATE TABLE `internship_interview`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `enterprise_position_id` bigint UNSIGNED NOT NULL COMMENT '企业岗位id ',
  `internship_apply_id` bigint UNSIGNED NOT NULL COMMENT '实习申请id ',
  `student_id` bigint UNSIGNED NOT NULL COMMENT '学生id ',
  `interview_status` tinyint UNSIGNED NOT NULL DEFAULT 0 COMMENT '面试状态（0-待面试, 1-面试通过, 2-面试不合格, 3-面试已取消）',
  `interview_time` datetime NOT NULL COMMENT '面试时间',
  `contact_person` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `contact_phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `interview_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '面试地址',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `internship_interview_internship_apply_id_fk`(`internship_apply_id` ASC) USING BTREE,
  CONSTRAINT `internship_interview_internship_apply_id_fk` FOREIGN KEY (`internship_apply_id`) REFERENCES `internship_apply` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实习面试表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of internship_interview
-- ----------------------------
INSERT INTO `internship_interview` VALUES (1, 7, 11, 1, 3, '2024-02-12 22:40:47', '张家豪', '18198165165', '深圳宝安区明禧创意园D座', NULL, b'0', '2024-02-12 22:40:54', '2024-02-13 00:55:43');
INSERT INTO `internship_interview` VALUES (2, 7, 11, 1, 2, '2024-02-12 22:41:15', '张家豪', '18198165165', '深圳宝安区明禧创意园D座', NULL, b'0', '2024-02-12 22:41:15', '2024-02-13 00:54:48');
INSERT INTO `internship_interview` VALUES (3, 2, 5, 2, 1, '2024-02-13 01:14:28', '徐佳丽', '13025438446', '深圳福田区深科技一单元', '请带好简历', b'0', '2024-02-13 01:14:28', '2024-02-13 01:17:01');
INSERT INTO `internship_interview` VALUES (4, 4, 13, 10, 1, '2024-04-15 17:33:28', '经理', '14442524522', '青秀区某某', NULL, b'0', '2024-04-15 17:33:28', '2024-04-15 17:40:07');
INSERT INTO `internship_interview` VALUES (5, 4, 13, 10, 3, '2024-04-15 17:33:28', '经理', '14442524522', '青秀区某某', NULL, b'0', '2024-04-15 17:33:28', '2024-04-15 17:34:16');
INSERT INTO `internship_interview` VALUES (6, 3, 15, 10, 2, '2024-04-15 17:39:00', '郝建立', '15244686225', '江南区莫某', NULL, b'0', '2024-04-15 17:39:00', '2024-04-15 17:40:26');
INSERT INTO `internship_interview` VALUES (7, 5, 16, 10, 1, '2024-04-15 17:47:44', '1111111111', '15488857855', '111111111', NULL, b'0', '2024-04-15 17:47:44', '2024-04-15 17:48:19');
INSERT INTO `internship_interview` VALUES (8, 4, 13, 10, 1, '2024-04-15 22:08:51', '1', '18777386002', '广西民族大学相思湖学院', '1', b'0', '2024-04-15 22:08:51', '2024-04-15 22:09:47');

-- ----------------------------
-- Table structure for internship_weekly
-- ----------------------------
DROP TABLE IF EXISTS `internship_weekly`;
CREATE TABLE `internship_weekly`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `internship_apply_id` bigint UNSIGNED NOT NULL COMMENT '实习申请记录id',
  `enterprise_position_id` bigint UNSIGNED NOT NULL COMMENT '企业岗位',
  `enterprise_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '企业名称',
  `position_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `student_id` bigint UNSIGNED NOT NULL COMMENT '实习学生',
  `student_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学生学号',
  `teacher_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '执教教师',
  `this_weekly_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '本周内容',
  `next_weekly_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下周内容',
  `annex_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '周报附件',
  `score_status` tinyint UNSIGNED NOT NULL DEFAULT 0 COMMENT '周报评分状态（0-未评分，1-已评分）',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `internship_weekly_internship_apply_id_fk`(`internship_apply_id` ASC) USING BTREE,
  CONSTRAINT `internship_weekly_internship_apply_id_fk` FOREIGN KEY (`internship_apply_id`) REFERENCES `internship_apply` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实习周报表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of internship_weekly
-- ----------------------------
INSERT INTO `internship_weekly` VALUES (1, 7, 3, '腾讯计算机有限公司', '前端开发工程师', 1, 'C1_zhangsan', 1, '实习周报管理系统可以提供以下功能：\n\n提交周报：实习生可以提交实习周报，包括本周工作内容、遇到的问题及解决方案、下周工作计划等。\n\n提交时间：实习生需要在规定的时间内提交周报，例如每周五下班前。\n\n自动提醒：系统可以设置自动提醒功能，在实习生应提交周报的时间点发送提醒通知。\n\n进度跟踪：系统可以跟踪实习生的工作进度，包括已完成的任务和未完成的任务，以及实习生对工作的反馈。\n\n任务分配：上级领导可以分配任务给实习生，并查看任务的完成情况。\n\n报告统计：系统可以统计实习生的周报完成情况、任务完成情况、工作效率等数据，为管理者的决策提供依据。\n\n权限管理：系统可以设置不同的权限，例如只有管理员才能分配任务和查看统计数据，实习生只能提交周报和查看自己的任务。\n\n反馈与评价：实习生和上级领导可以在系统中对彼此的工作进行反馈和评价，以便于更好地了解彼此的需求和期望。\n\n报表导出：系统可以导出实习生的周报报表，方便管理者进行数据分析和决策。', '实习周报管理系统可以提供以下功能：\n\n提交周报：实习生可以提交实习周报，包括本周工作内容、遇到的问题及解决方案、下周工作计划等。\n\n提交时间：实习生需要在规定的时间内提交周报，例如每周五下班前。\n\n自动提醒：系统可以设置自动提醒功能，在实习生应提交周报的时间点发送提醒通知。\n\n进度跟踪：系统可以跟踪实习生的工作进度，包括已完成的任务和未完成的任务，以及实习生对工作的反馈。\n\n任务分配：上级领导可以分配任务给实习生，并查看任务的完成情况。\n\n报告统计：系统可以统计实习生的周报完成情况、任务完成情况、工作效率等数据，为管理者的决策提供依据。\n\n权限管理：系统可以设置不同的权限，例如只有管理员才能分配任务和查看统计数据，实习生只能提交周报和查看自己的任务。\n\n反馈与评价：实习生和上级领导可以在系统中对彼此的工作进行反馈和评价，以便于更好地了解彼此的需求和期望。\n\n报表导出：系统可以导出实习生的周报报表，方便管理者进行数据分析和决策。', 'http://localhost:8080/internship/file/download/7eec9fc3770d4067bc7191db0bd5948c.pdf', 1, b'0', '2023-12-09 16:12:16', '2023-12-10 15:20:01');
INSERT INTO `internship_weekly` VALUES (2, 6, 4, '阿里巴巴集团', 'Java开发工程师', 1, 'C1_zhangsan', 1, '本周工作如下。。。。', '下周工作计划如下。。。。', 'http://localhost:8080/internship/file/download/41569f8e47be4dc8a6ca264b01429f88.docx', 1, b'0', '2023-12-10 01:35:22', '2023-12-10 15:49:56');
INSERT INTO `internship_weekly` VALUES (3, 7, 3, '腾讯计算机有限公司', '前端开发工程师', 1, 'C1_zhangsan', 1, '今天星期天', '明天星期一', 'http://localhost:8080/internship/file/download/a7ae78b3913a4d2199f9f672fbc8e1ed.pdf', 0, b'0', '2024-01-06 16:00:00', '2024-01-07 22:01:12');
INSERT INTO `internship_weekly` VALUES (4, 6, 4, '阿里巴巴集团', 'Java开发工程师', 1, 'C1_zhangsan', 1, '今天星期天', '明天星期一', NULL, 0, b'0', '2024-01-07 00:00:00', '2024-01-07 22:09:27');
INSERT INTO `internship_weekly` VALUES (5, 6, 4, '阿里巴巴集团', 'Java开发工程师', 1, 'C1_zhangsan', 5, '今年是龙年666', '年后再说', NULL, 1, b'0', '2024-02-14 00:00:00', '2024-02-14 00:17:29');
INSERT INTO `internship_weekly` VALUES (6, 2, 1, '阿里巴巴集团', 'Java开发工程师', 2, 'C1_lisi', 5, '春节放假中。。', '年后再说', NULL, 0, b'0', '2024-02-14 00:00:00', '2024-02-14 00:30:06');
INSERT INTO `internship_weekly` VALUES (7, 6, 4, '阿里巴巴集团', 'Java开发实习生', 1, 'C1_zhangsan', 5, '2024/3/16', '2024/3/24', 'http://localhost:8080/internship/file/download/656f993b38204d209b1a7c170d0e4527.docx', 0, b'0', '2024-03-17 00:00:00', '2024-03-17 10:48:17');
INSERT INTO `internship_weekly` VALUES (8, 13, 4, '阿里巴巴集团', 'Java开发实习生', 10, 'stu001', 6, '11', '11', 'http://localhost:8080/internship/file/download/dd481f319c074cf1bbf422ce6e4f24f6.pdf', 0, b'0', '2024-04-10 00:00:00', '2024-04-15 22:44:39');

-- ----------------------------
-- Table structure for internship_weekly_score
-- ----------------------------
DROP TABLE IF EXISTS `internship_weekly_score`;
CREATE TABLE `internship_weekly_score`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `internship_weekly_id` bigint UNSIGNED NOT NULL COMMENT '实习周报id',
  `score_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评分内容',
  `score` decimal(5, 2) NOT NULL COMMENT '评分',
  `score_man` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评分人',
  `score_id` bigint UNSIGNED NOT NULL COMMENT '评分人id',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `internship_weekly_score_internship_weekly_id_fk`(`internship_weekly_id` ASC) USING BTREE,
  CONSTRAINT `internship_weekly_score_internship_weekly_id_fk` FOREIGN KEY (`internship_weekly_id`) REFERENCES `internship_weekly` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实习周报评分表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of internship_weekly_score
-- ----------------------------
INSERT INTO `internship_weekly_score` VALUES (1, 2, '表现不错，希望继续努力', 8.00, '许老师', 3, b'0', '2023-12-10 15:16:14', '2023-12-10 15:16:14');
INSERT INTO `internship_weekly_score` VALUES (2, 1, '表现的很好！', 10.00, '许老师', 3, b'0', '2023-12-10 15:20:01', '2023-12-10 15:20:01');
INSERT INTO `internship_weekly_score` VALUES (3, 2, '表现的不错，希望再接再厉。\n表现的不错，希望再接再厉。\n表现的不错，希望再接再厉。\n表现的不错，希望再接再厉。\n表现的不错，希望再接再厉。', 8.50, '企业-阿里巴巴集团', 12, b'0', '2023-12-10 15:49:56', '2023-12-10 15:49:56');
INSERT INTO `internship_weekly_score` VALUES (4, 5, '干的不错，希望继续加油', 9.50, '许老师', 3, b'0', '2024-02-14 00:17:29', '2024-02-14 00:17:29');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `notice_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公告标题',
  `notice_banner` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公告海报',
  `notice_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告内容',
  `publisher_id` bigint UNSIGNED NOT NULL COMMENT '发布人用户id',
  `recipient` tinyint NOT NULL DEFAULT 0 COMMENT '接收对象 0-全体, 1-学生, 2-教师, 3-企业',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '公告状态（0停用 1正常）',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES (1, '重要通知', 'http://localhost:8080/internship/file/download/ce570e5507a84eb986ded0070d4a83e4.png', '一般情况下，一次面试时间在30min左右，如果聊得比较深入，可能会拉长到一个小时左右，速度如果快一点，也基本上有20min。除此之外，建议答主，如果有经历的话，还可以再关注下自己的面试节奏哦。比如如何', 1, 0, 1, b'0', '2023-12-21 00:18:25', '2023-12-21 23:24:14');
INSERT INTO `notice` VALUES (2, '临时通知', 'http://localhost:8080/internship/file/download/191ea4ba329d4ba19e21311433f7e1b5.png', '又到了传说中金三银四的黄金求职期，想必大家在这段时间多多少少都听说过“内推”这个词。对于已经有过打工经历的打工人来说，这个词想必并不陌生；但对于咱们刚刚毕业的应届生来说，对传闻\n又到了传说中金三银四的黄金求职期，想必大家在这段时间多多少少都听说过“内推”这个词。对于已经有过打工经历的打工人来说，这个词想必并不陌生；但对于咱们刚刚毕业的应届生来说，对传闻\n又到了传说中金三银四的黄金求职期，想必大家在这段时间多多少少都听说过“内推”这个词。对于已经有过打工经历的打工人来说，这个词想必并不陌生；但对于咱们刚刚毕业的应届生来说，对传闻\n又到了传说中金三银四的黄金求职期，想必大家在这段时间多多少少都听说过“内推”这个词。对于已经有过打工经历的打工人来说，这个词想必并不陌生；但对于咱们刚刚毕业的应届生来说，对传闻', 1, 0, 1, b'0', '2023-12-21 23:52:24', '2023-12-21 23:52:24');
INSERT INTO `notice` VALUES (3, '【海归必读】零经验？不慌！', 'http://localhost:8080/internship/file/download/380ba69ac7c64f938bbd7d917501d178.png', '对于部分应届生，读书课程安排紧张，只是应付学业就已经非常吃力，再加上其它各种因素，根本没有时间、也没有机会实习。缺少实习经验，求职遇到最直接、最“致命”的影响是简历内容不充实', 1, 0, 1, b'0', '2023-12-24 22:47:13', '2023-12-24 22:47:13');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学生姓名',
  `student_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学生学号',
  `gender` bigint UNSIGNED NOT NULL DEFAULT 1 COMMENT '性别：1-男,2-女',
  `classes_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '班级',
  `phone_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `major` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '专业',
  `graduation_year` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '毕业年份',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '家庭地址',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unx_student_code`(`student_code` ASC) USING BTREE,
  INDEX `classes_id_fk`(`classes_id` ASC) USING BTREE,
  CONSTRAINT `classes_id_fk` FOREIGN KEY (`classes_id`) REFERENCES `classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, '张三', 'C1_zhangsan', 1, 1, '13025438443', '计算机', '2023', 'zhangsan@qq.com', '广东省深圳市', b'0', '2023-11-13 00:13:41', '2023-11-13 00:15:39');
INSERT INTO `student` VALUES (2, '李四', 'C1_lisi', 1, 3, '13025438666', '会计', '2024', '28146167261@qq.com', '广东省广州市', b'0', '2023-11-13 00:18:45', '2023-12-02 22:43:37');
INSERT INTO `student` VALUES (8, '刘五', 'C1_liuwu', 2, 2, '15577324666', '计算机', '2025', NULL, '2022', b'0', '2023-11-13 00:21:04', '2023-11-13 00:31:12');
INSERT INTO `student` VALUES (9, '测试学生', 'test_studnet_01', 2, 1, '19212424242', '计算机', '2024', NULL, '广东省广州市', b'0', '2023-12-04 00:17:46', '2023-12-04 00:17:46');
INSERT INTO `student` VALUES (10, '小王', 'stu001', 2, 4, '18777859667', '财务管理', '2024', NULL, 'aa', b'0', '2024-04-15 17:29:28', '2024-04-15 17:29:28');

-- ----------------------------
-- Table structure for student_resume
-- ----------------------------
DROP TABLE IF EXISTS `student_resume`;
CREATE TABLE `student_resume`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `resume_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '简历名称',
  `student_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '所属学生',
  `resume_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '简历文件id',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `student_resume_student_id_fk`(`student_id` ASC) USING BTREE,
  CONSTRAINT `student_resume_student_id_fk` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生简历表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student_resume
-- ----------------------------
INSERT INTO `student_resume` VALUES (1, '简历1', 2, 'http://localhost:8080/internship/file/download/e8e82b6d55784426aa21ed39c53968cf.pdf', b'0', '2023-11-18 19:02:01', '2023-12-02 22:52:16');
INSERT INTO `student_resume` VALUES (2, '前端开发', 2, 'http://localhost:8080/internship/file/download/09de91517f36446c85d9f0b63d74d173.pdf', b'0', '2023-12-02 22:51:00', '2023-12-02 22:52:14');
INSERT INTO `student_resume` VALUES (3, 'Java实习生', 1, 'http://localhost:8080/internship/file/download/54c61bee55444a4794724a4c847c06fa.pdf', b'0', '2023-12-03 17:45:35', '2023-12-03 17:45:35');
INSERT INTO `student_resume` VALUES (4, '前端开发实习生', 8, 'http://localhost:8080/internship/file/download/0f94a6dbdc914b2db31daa3d8f98519d.pdf', b'1', '2023-12-03 23:42:45', '2023-12-04 00:04:35');
INSERT INTO `student_resume` VALUES (5, '前端开发实习生简历', 8, 'http://localhost:8080/internship/file/download/d1c8213be6ab4afe9a4deff79f5600d1.pdf', b'0', '2023-12-04 00:04:59', '2023-12-04 00:04:59');
INSERT INTO `student_resume` VALUES (6, 'Java实习生简历', 9, 'http://localhost:8080/internship/file/download/d094e5a5a81b4365bd27ca7142e7aeee.pdf', b'0', '2023-12-04 00:18:30', '2023-12-04 00:18:30');
INSERT INTO `student_resume` VALUES (7, 'Java开发实习简历', 2, 'http://localhost:8080/internship/file/download/111cced6bc93434481518709b5c33ec1.pdf', b'0', '2023-12-04 00:47:36', '2023-12-04 00:47:36');
INSERT INTO `student_resume` VALUES (8, '项目实习生', 1, 'http://localhost:8080/internship/file/download/65dddc7fbe9e467e9c6782f60f8d5fd7.docx', b'0', '2024-03-17 10:54:25', '2024-03-17 10:54:25');
INSERT INTO `student_resume` VALUES (9, 'Java工程师', 10, 'http://localhost:8080/internship/file/download/bdab097fbd9c464a8795a087614bf917.pdf', b'0', '2024-04-15 17:30:39', '2024-04-15 17:30:39');
INSERT INTO `student_resume` VALUES (10, '前端', 10, 'http://localhost:8080/internship/file/download/73e35c1a92404ae1ad999427400d4122.pdf', b'0', '2024-04-15 17:30:51', '2024-04-15 17:30:51');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型名称',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型编码',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '状态（0-正常 ,1-停用）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unx_dict_code`(`code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '性别', 'gender', 0, '性别', b'0', '2019-12-06 19:03:32', '2021-08-07 15:52:45');
INSERT INTO `sys_dict` VALUES (11, '授权方式', 'grant_type', 0, 'oauth2授权方式', b'0', '2020-10-17 08:09:50', '2021-08-07 15:52:45');
INSERT INTO `sys_dict` VALUES (24, '服务路由列表', 'micro_service', 0, '设置URL权限标识使用', b'0', '2021-06-17 00:13:43', '2021-08-07 15:52:45');
INSERT INTO `sys_dict` VALUES (25, '请求方式', 'request_method', 0, '设置URL权限标识使用', b'0', '2021-06-17 00:18:07', '2021-08-07 15:52:48');
INSERT INTO `sys_dict` VALUES (26, '测试', 'test', 0, NULL, b'1', '2021-08-08 18:44:42', '2023-12-09 14:43:49');

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典项名称',
  `value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典项值',
  `dict_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典编码',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '状态（0 正常 1停用）',
  `defaulted` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否默认（0否 1是）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES (1, '男', '1', 'gender', 1, 0, 0, '性别男', b'0', '2021-08-07 16:06:22', '2021-08-08 18:18:13');
INSERT INTO `sys_dict_item` VALUES (2, '女', '2', 'gender', 2, 0, 0, '性别女', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:14');
INSERT INTO `sys_dict_item` VALUES (5, '未知', '0', 'gender', 1, 0, 0, '', b'1', '2021-08-07 16:07:07', '2023-12-09 14:43:54');
INSERT INTO `sys_dict_item` VALUES (6, '密码模式', 'password', 'grant_type', 1, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:16');
INSERT INTO `sys_dict_item` VALUES (7, '授权码模式', 'authorization_code', 'grant_type', 1, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:16');
INSERT INTO `sys_dict_item` VALUES (8, '客户端模式', 'client_credentials', 'grant_type', 1, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:17');
INSERT INTO `sys_dict_item` VALUES (9, '刷新模式', 'refresh_token', 'grant_type', 1, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:17');
INSERT INTO `sys_dict_item` VALUES (10, '简化模式', 'implicit', 'grant_type', 1, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:18');
INSERT INTO `sys_dict_item` VALUES (38, '系统服务', 'admin', 'micro_service', 0, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:19');
INSERT INTO `sys_dict_item` VALUES (39, '会员服务', 'ums', 'micro_service', 3, 1, 0, '', b'1', '2021-08-07 16:07:07', '2023-11-18 10:44:01');
INSERT INTO `sys_dict_item` VALUES (40, '商品服务', 'pms', 'micro_service', 2, 1, 0, '', b'1', '2021-08-07 16:07:07', '2023-11-18 10:43:59');
INSERT INTO `sys_dict_item` VALUES (41, '订单服务', 'oms', 'micro_service', 4, 1, 0, '', b'1', '2021-08-07 16:07:07', '2023-11-18 10:44:02');
INSERT INTO `sys_dict_item` VALUES (42, '营销服务', 'sms', 'micro_service', 5, 1, 0, '', b'1', '2021-08-07 16:07:07', '2023-11-18 10:44:04');
INSERT INTO `sys_dict_item` VALUES (43, '不限', '*', 'request_method', 1, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:21');
INSERT INTO `sys_dict_item` VALUES (44, 'GET', 'GET', 'request_method', 2, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:22');
INSERT INTO `sys_dict_item` VALUES (45, 'POST', 'POST', 'request_method', 3, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:23');
INSERT INTO `sys_dict_item` VALUES (46, 'PUT', 'PUT', 'request_method', 4, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:23');
INSERT INTO `sys_dict_item` VALUES (47, 'DELETE', 'DELETE', 'request_method', 5, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:23');
INSERT INTO `sys_dict_item` VALUES (48, 'PATCH', 'PATCH', 'request_method', 6, 0, 0, '', b'0', '2021-08-07 16:07:07', '2021-08-08 18:18:25');
INSERT INTO `sys_dict_item` VALUES (49, '测试', 'test', 'test', 1, 0, 0, '', b'1', '2021-08-08 18:45:22', '2023-12-09 14:43:48');
INSERT INTO `sys_dict_item` VALUES (50, '文件服务', 'upload', 'micro_service', 6, 0, 0, '', b'0', '2021-08-22 16:46:57', '2021-08-22 16:46:57');
INSERT INTO `sys_dict_item` VALUES (51, '学生管理', 'student', 'micro_service', 1, 0, 0, '', b'0', '2023-11-12 23:06:45', '2023-11-12 23:06:45');
INSERT INTO `sys_dict_item` VALUES (52, '老师管理', 'teacher', 'micro_service', 2, 0, 0, '', b'0', '2023-11-13 23:26:56', '2023-11-13 23:26:56');
INSERT INTO `sys_dict_item` VALUES (53, '企业服务', 'enterprise', 'micro_service', 2, 0, 0, '', b'0', '2023-11-14 23:44:15', '2023-11-14 23:44:15');
INSERT INTO `sys_dict_item` VALUES (54, '企业岗位服务', 'enterprisePosition', 'micro_service', 3, 0, 0, '', b'0', '2023-11-18 10:43:54', '2023-11-18 10:43:54');
INSERT INTO `sys_dict_item` VALUES (55, '班级服务', 'classes', 'micro_service', 4, 0, 0, '', b'0', '2023-11-18 16:36:56', '2023-11-18 16:36:56');
INSERT INTO `sys_dict_item` VALUES (56, '学生简历服务', 'studentResume', 'micro_service', 5, 0, 0, '', b'0', '2023-11-18 17:24:45', '2023-11-18 17:24:45');
INSERT INTO `sys_dict_item` VALUES (57, '实习申请记录', 'internshipApply', 'micro_service', 7, 0, 0, '', b'0', '2023-11-30 00:56:41', '2023-11-30 00:56:41');
INSERT INTO `sys_dict_item` VALUES (58, '实习周报', 'internshipWeekly', 'micro_service', 10, 0, 0, '', b'0', '2023-12-09 14:44:26', '2023-12-09 14:44:26');
INSERT INTO `sys_dict_item` VALUES (59, '实习周报评分', 'internshipWeeklyScore', 'micro_service', 10, 0, 0, '', b'0', '2023-12-10 14:54:31', '2023-12-10 14:54:31');
INSERT INTO `sys_dict_item` VALUES (60, '公告管理', 'notice', 'micro_service', 9, 0, 0, '', b'0', '2023-12-24 23:02:20', '2023-12-24 23:02:20');
INSERT INTO `sys_dict_item` VALUES (61, '实习面试记录', 'internshipInterview', 'micro_service', 11, 0, 0, '', b'0', '2024-02-12 21:50:55', '2024-02-12 21:50:55');
INSERT INTO `sys_dict_item` VALUES (62, '实习审核', 'internshipAudit', 'micro_service', 1, 0, 0, '', b'0', '2024-02-13 21:11:07', '2024-02-13 21:11:07');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父菜单ID',
  `route_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由名称',
  `route_path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由路径',
  `component` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `redirect` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '跳转路径',
  `icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单图标',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `visible` tinyint(1) NULL DEFAULT 1 COMMENT '状态：0-禁用 1-开启',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统认证中心', 0, 'admin', '/admin', 'Layout', '', 'system', 7, 1, b'0', '2021-07-26 19:53:54', '2023-12-24 23:01:27');
INSERT INTO `sys_menu` VALUES (2, '用户管理', 1, 'admin-user', 'user', 'admin/user/index', '', 'ums-admin', 0, 1, b'0', '2021-07-31 12:28:31', '2021-09-08 21:20:33');
INSERT INTO `sys_menu` VALUES (3, '菜单和权限管理', 1, 'admin-menu', 'menu', 'admin/menu/index', '', 'ums-menu', 2, 1, b'0', '2021-08-07 17:49:15', '2023-11-18 11:59:02');
INSERT INTO `sys_menu` VALUES (4, '角色管理', 1, 'admin-role', 'role', 'admin/role/index', '', 'peoples', 3, 1, b'0', '2021-08-07 17:50:26', '2021-08-22 16:37:49');
INSERT INTO `sys_menu` VALUES (5, '部门管理', 1, 'admin-dept', 'dept', 'admin/dept/index', '', 'dict', 1, 0, b'1', '2021-08-07 17:51:04', '2023-11-27 22:45:41');
INSERT INTO `sys_menu` VALUES (6, '字典管理', 1, 'admin-dict', 'dict', 'admin/dict/index', '', 'education', 4, 1, b'0', '2021-08-07 17:51:44', '2021-08-22 16:37:58');
INSERT INTO `sys_menu` VALUES (7, '客户端管理', 1, 'admin-client', 'client', 'admin/client/index', '', 'clipboard', 5, 0, b'1', '2021-08-07 17:52:39', '2023-11-27 22:45:44');
INSERT INTO `sys_menu` VALUES (8, '商品管理', 0, 'pms', '/pms', 'Layout', '', 'product', 1, 0, b'1', '2021-08-11 22:37:49', '2023-11-27 22:43:40');
INSERT INTO `sys_menu` VALUES (9, '品牌管理', 8, 'pms-brand', 'brand', 'pms/brand/index', '', 'product-brand', 3, 1, b'1', '2021-08-11 23:01:59', '2023-11-27 22:43:33');
INSERT INTO `sys_menu` VALUES (10, '分类管理', 8, 'pms-category', 'category', 'pms/category/index', '', 'product-cate', 2, 1, b'1', '2021-08-11 23:02:30', '2023-11-27 22:43:34');
INSERT INTO `sys_menu` VALUES (11, '商品列表', 8, 'pms-list-goods', 'list-goods', 'pms/goods/index', '', 'product-list', 0, 1, b'1', '2021-08-16 19:18:33', '2023-11-27 22:43:37');
INSERT INTO `sys_menu` VALUES (12, '商品上架', 8, 'pms-save-goods', 'save-goods', 'pms/goods/detail', '', 'product-add', 1, 1, b'1', '2021-08-16 21:56:17', '2023-11-27 22:43:36');
INSERT INTO `sys_menu` VALUES (13, '文件管理', 0, 'upload', '/upload', 'Layout', '', 'upload', 6, 0, b'1', '2021-08-22 16:45:01', '2023-12-03 13:48:49');
INSERT INTO `sys_menu` VALUES (14, '会员管理', 0, 'ums', '/ums', 'Layout', '', 'ums-role', 3, 0, b'1', '2021-08-22 17:45:55', '2023-11-27 22:43:14');
INSERT INTO `sys_menu` VALUES (15, '会员列表', 14, 'ums-list-member', 'list-member', 'ums/member/index', '', 'ums-resource', 1, 1, b'1', '2021-08-22 17:46:58', '2023-11-27 22:43:13');
INSERT INTO `sys_menu` VALUES (16, '测试', 14, NULL, 'test', 'admin/user/test', '', '', 1, 1, b'1', '2021-09-08 22:03:18', '2021-09-08 22:13:24');
INSERT INTO `sys_menu` VALUES (17, '订单管理', 0, 'oms', '/oms', 'Layout', '', 'order', 2, 0, b'1', '2021-09-16 21:39:08', '2023-11-27 22:43:28');
INSERT INTO `sys_menu` VALUES (18, '订单列表', 17, 'order-list', 'order-list', 'oms/order/index', '', 'product-list', 1, 1, b'1', '2021-09-16 22:07:07', '2023-11-27 22:43:26');
INSERT INTO `sys_menu` VALUES (19, '订单详情', 17, 'orderDetail', 'orderDetail', 'oms/order/orderDetail', '', 'order-return-reason', 2, 1, b'1', '2021-09-18 23:42:30', '2023-11-27 22:43:25');
INSERT INTO `sys_menu` VALUES (20, '订单发货', 17, NULL, 'orderDelivery', 'oms/order/orderDelivery', '', 'order-return', 3, 1, b'1', '2021-09-19 11:42:42', '2023-11-27 22:43:23');
INSERT INTO `sys_menu` VALUES (21, '学生信息管理', 0, 'student', '/student', 'Layout', '', 'peoples', 3, 1, b'0', '2023-11-12 22:59:45', '2023-12-24 23:01:15');
INSERT INTO `sys_menu` VALUES (22, '学生信息管理', 21, 'student-manage', 'manage', 'student/manage/index', '', 'people', 0, 1, b'0', '2023-11-12 23:02:17', '2023-11-12 23:02:17');
INSERT INTO `sys_menu` VALUES (23, '教师信息管理', 21, 'student-teacher', 'teacher', 'student/teacher/index', '', 'user', 1, 1, b'0', '2023-11-13 23:26:15', '2023-11-18 16:36:37');
INSERT INTO `sys_menu` VALUES (24, '企业信息管理', 0, 'enterprise', '/enterprise', 'Layout', '', 'guide', 4, 1, b'0', '2023-11-14 23:42:35', '2023-12-24 23:01:18');
INSERT INTO `sys_menu` VALUES (25, '企业信息管理', 24, 'enterprise-manage', 'manage', 'enterprise/manage/index', '', 'international', 0, 1, b'0', '2023-11-14 23:43:46', '2023-11-14 23:43:46');
INSERT INTO `sys_menu` VALUES (26, '企业岗位管理', 24, 'enterprise-position', 'position', 'enterprise/position/index', '', 'product-add', 1, 1, b'0', '2023-11-18 10:43:11', '2023-11-18 10:43:11');
INSERT INTO `sys_menu` VALUES (27, '班级管理', 21, 'student-classes', 'classes', 'student/classes/index', '', 'list', 2, 1, b'0', '2023-11-18 16:36:26', '2023-11-18 16:36:26');
INSERT INTO `sys_menu` VALUES (28, '个人简历', 32, 'student-resume', 'resume', 'student/resume/index', '', 'upload', 3, 1, b'0', '2023-11-18 17:23:42', '2024-02-13 21:15:34');
INSERT INTO `sys_menu` VALUES (29, '企业招聘', 24, 'enterprise-recruit', 'recruit', 'enterprise/apply/index', '', 'documentation', 2, 1, b'0', '2023-11-28 00:23:54', '2023-11-28 00:23:54');
INSERT INTO `sys_menu` VALUES (30, '实习申请', 32, 'internship-apply', 'internship-apply', 'internship/apply/index', '', 'documentation', 4, 1, b'0', '2023-12-02 17:19:44', '2024-02-13 21:15:36');
INSERT INTO `sys_menu` VALUES (31, '个人中心', 0, 'user-center', '/profile/index', 'profile/index', '', 'ums', 1, 1, b'0', '2023-12-02 18:32:46', '2023-12-02 18:42:19');
INSERT INTO `sys_menu` VALUES (32, '学生实习业务', 0, 'student-internship', '/student-internship', 'Layout', '', 'order', 5, 1, b'0', '2023-12-09 14:40:00', '2023-12-24 23:01:24');
INSERT INTO `sys_menu` VALUES (33, '学生实习周报', 32, 'student-internship-weekly', 'internship-weekly', 'internship/weekly/index', '', 'documentation', 2, 1, b'0', '2023-12-09 14:41:29', '2024-02-13 21:26:04');
INSERT INTO `sys_menu` VALUES (34, '我的实习周报', 32, 'student-my-weekly', 'my-weekly', 'internship/myWeekly/index', '', 'email', 7, 0, b'0', '2023-12-09 14:42:55', '2024-03-16 22:04:43');
INSERT INTO `sys_menu` VALUES (35, '我的实习申请', 32, 'internship-my-apply', 'internship-my-apply', 'internship/myApply/index', '', 'offline', 5, 0, b'0', '2023-12-09 15:31:23', '2024-03-16 22:01:49');
INSERT INTO `sys_menu` VALUES (36, '系统运营管理', 0, 'operate', '/operate', 'Layout', '', 'edit', 2, 1, b'0', '2023-12-21 00:14:20', '2023-12-24 23:39:25');
INSERT INTO `sys_menu` VALUES (37, '公告管理', 36, 'notice-manage', '/notice-manage', 'notice/manage/index', '', 'link', 1, 1, b'0', '2023-12-24 23:40:12', '2023-12-24 23:40:12');
INSERT INTO `sys_menu` VALUES (38, '企业面试管理', 24, 'enterprise-interview', 'enterprise-interview', 'enterprise/interview/index', '', 'form', 5, 1, b'0', '2024-02-12 21:49:27', '2024-02-12 21:49:27');
INSERT INTO `sys_menu` VALUES (39, '我的实习面试', 32, 'internship-my-interview', 'internship-my-interview', 'internship/myInterview/index', '', 'skill', 6, 1, b'0', '2024-02-13 01:20:00', '2024-02-13 21:15:44');
INSERT INTO `sys_menu` VALUES (40, '学生实习审核', 32, 'student-internship-audit', 'student-internship-audit', 'internship/audit/index', '', 'log', 1, 1, b'0', '2024-02-13 21:15:26', '2024-02-13 21:26:08');
INSERT INTO `sys_menu` VALUES (41, '我的实习', 32, 'my-internship', 'my-internship', 'internship/myInternship/index', '', 'star', 8, 1, b'0', '2024-03-06 22:47:18', '2024-03-06 22:53:16');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `menu_id` bigint NULL DEFAULT NULL COMMENT '菜单模块ID',
  `url_perm` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL权限标识',
  `btn_perm` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '按钮权限标识',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_permission_id_permission_name`(`id` ASC, `name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 155 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (1, '查看用户', 2, 'GET:/admin/user/list/**', 'admin:user:view', b'0', '2021-07-31 12:30:38', '2023-11-12 23:47:23');
INSERT INTO `sys_permission` VALUES (2, '编辑用户', 2, 'PUT:/admin/user/update', 'admin:user:edit', b'0', '2021-07-31 12:34:44', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (3, '新增用户', 2, 'POST:/admin/user/add', 'admin:user:add', b'0', '2021-07-31 12:35:17', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (4, '删除用户', 2, 'DELETE:/admin/user/delete', 'admin:user:delete', b'0', '2021-07-31 12:35:37', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (5, '更改密码或状态', 2, 'PATCH:/admin/user/update', 'admin:user:reset', b'0', '2021-08-06 17:23:26', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (10, '测试', 2, 'GET:/admin/abc', 'admin:user:test', b'1', '2021-08-08 02:49:49', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (11, '测试按钮', 2, 'POST:/api/v1/', 'admin:user:test', b'1', '2021-08-08 15:25:17', '2021-08-08 15:34:13');
INSERT INTO `sys_permission` VALUES (12, '测试', 2, 'PUT:/admin/test', 'admin:user:test', b'1', '2021-08-08 15:50:56', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (13, '测试', 2, 'POST:/api/v1/admin/user/update', 'admin:user:test', b'1', '2021-08-08 18:59:03', '2021-08-20 00:19:44');
INSERT INTO `sys_permission` VALUES (14, '查看部门', 5, 'GET:/admin/dept/list/**', 'admin:dept:view', b'0', '2021-08-08 22:22:05', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (15, '编辑部门', 5, 'PUT:/admin/dept/update', 'admin:dept:update', b'0', '2021-08-08 22:22:57', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (16, '新增部门', 5, 'POST:/admin/dept/add', 'admin:dept:add', b'0', '2021-08-08 22:24:46', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (17, '删除部门', 5, 'DELETE:/admin/dept/delete', 'admin:dept:delete', b'0', '2021-08-08 22:26:43', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (18, '更改部门状态', 5, 'PATCH:/admin/dept/update', 'admin:dept:status', b'0', '2021-08-08 22:27:56', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (19, '查看菜单', 3, 'GET:/admin/menu/list/**', 'admin:menu:view', b'0', '2021-08-08 22:29:01', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (20, '新增菜单', 3, 'POST:/admin/menu/add', 'admin:menu:add', b'0', '2021-08-08 22:29:21', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (21, '编辑菜单', 3, 'PUT:/admin/menu/update', 'admin:menu:update', b'0', '2021-08-08 22:29:53', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (22, '删除菜单', 3, 'DELETE:/admin/menu/delete', 'admin:menu:delete', b'0', '2021-08-08 22:30:20', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (23, '更改菜单状态', 3, 'PATCH:/admin/menu/update', 'admin:menu:status', b'0', '2021-08-08 22:31:33', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (24, '查看角色', 4, 'GET:/admin/role/list/**', 'admin:role:view', b'0', '2021-08-08 22:32:25', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (25, '新增角色', 4, 'POST:/admin/role/add', 'admin:role:add', b'0', '2021-08-08 22:32:59', '2023-11-13 00:04:09');
INSERT INTO `sys_permission` VALUES (26, '编辑角色', 4, 'PUT:/admin/role/update', 'admin:role:update', b'0', '2021-08-08 22:33:24', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (27, '删除角色', 4, 'DELETE:/admin/role/delete', 'admin:role:delete', b'0', '2021-08-08 22:33:47', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (28, '查看角色菜单', 4, 'GET:/admin/role/menus/**', 'admin:role:menus:view', b'0', '2021-08-08 22:34:57', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (29, '修改角色菜单', 4, 'PUT:/admin/role/menus/**', 'admin:role:menus:update', b'0', '2021-08-08 22:35:55', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (30, '查看角色权限', 4, 'GET:/admin/role/permissions/**', 'admin:role:permissions:view', b'0', '2021-08-08 22:36:52', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (31, '修改角色权限', 4, 'PUT:/admin/role/permissions/**', 'admin:role:permissions:update', b'0', '2021-08-08 22:37:42', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (32, '更改角色状态', 4, 'PATCH:/admin/role/update', 'admin:role:status', b'0', '2021-08-08 22:39:57', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (33, '查看字典', 6, 'GET:/admin/dict/list/**', 'admin:dict:view', b'0', '2021-08-08 22:41:23', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (34, '查看字典项', 6, 'GET:/admin/dict-item/list/**', 'admin:dict-item:view', b'0', '2021-08-08 22:41:55', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (35, '新增字典', 6, 'POST:/admin/dict/add', 'admin:dict:add', b'0', '2021-08-08 22:42:40', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (36, '新增字典项', 6, 'POST:/admin/dict-item/add', 'admin:dict-item:add', b'0', '2021-08-08 22:43:05', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (37, '编辑字典', 6, 'PUT:/admin/dict/update', 'admin:dict:update', b'0', '2021-08-08 22:43:58', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (38, '编辑字典项', 6, 'PUT:/admin/dict-item/update', 'admin:dict-item:update', b'0', '2021-08-08 22:44:27', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (39, '删除字典', 6, 'DELETE:/admin/dict/delete', 'admin:dict:delete', b'0', '2021-08-08 22:44:56', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (40, '删除字典项', 6, 'DELETE:/admin/dict-item/delete', 'admin:dict-item:delete', b'0', '2021-08-08 22:45:19', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (41, '更改字典状态', 6, 'PATCH:/admin/dict/update', 'admin:dict:status', b'0', '2021-08-08 22:46:22', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (42, '更改字典项状态', 6, 'PATCH:/admin/dict-item/update', 'admin:dict-item:status', b'0', '2021-08-08 22:47:01', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (43, '查看客户端', 7, 'GET:/admin/oauth-client/list/**', 'admin:oauth-client:view', b'0', '2021-08-08 22:47:37', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (44, '新增客户端', 7, 'POST:/admin/oauth-client/add', 'admin:oauth-client:add', b'0', '2021-08-08 22:48:26', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (45, '编辑客户端', 7, 'PUT:/admin/oauth-client/update', 'admin:oauth-client:update', b'0', '2021-08-08 22:48:56', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (46, '删除客户端', 7, 'DELETE:/admin/oauth-client/delete', 'admin:oauth-client:delete', b'0', '2021-08-08 22:50:11', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (47, '查看权限', 3, 'GET:/admin/permission/list/**', 'admin:permission:view', b'0', '2021-08-08 22:53:12', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (48, '新增权限', 3, 'POST:/admin/permission/add', 'admin:permission:add', b'0', '2021-08-08 22:53:46', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (49, '编辑权限', 3, 'PUT:/admin/permission/update', 'admin:permission:update', b'0', '2021-08-08 22:54:37', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (50, '删除权限', 3, 'DELETE:/admin/permission/delete', 'admin:permission:delete', b'0', '2021-08-08 22:55:01', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (51, '查看商品列表', 11, 'GET:/pms/spu/list**', 'pms:goods:list', b'0', '2021-08-20 00:34:11', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (52, '添加商品', 11, 'POST:/pms/spu/save', 'pms:goods:add', b'1', '2021-08-20 00:36:07', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (53, '查看商品详情', 11, 'GET:/pms/spu/list/**', 'pms:goods:detail', b'1', '2021-08-20 00:40:55', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (54, '删除商品', 11, 'DELETE:/pms/spu/delete', 'pms:goods:delete', b'0', '2021-08-20 01:30:14', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (55, '更改商品状态', 11, 'PATCH:/pms/spu/update', 'pms:goods:publishStatus', b'0', '2021-08-20 01:32:40', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (56, '查看SKU库存', 11, 'GET:/pms/sku/list', 'pms:goods:list:sku', b'0', '2021-08-20 01:33:54', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (57, '编辑SKU库存', 11, 'PUT:/pms/sku/edit-sku', 'pms:goods:edit:sku', b'0', '2021-08-20 01:34:58', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (58, '上架商品', 12, 'POST:/pms/spu/save', 'pms:goods:save', b'0', '2021-08-20 01:41:05', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (59, '修改商品', 12, 'PUT:/pms/spu/update', 'pms:goods:update', b'0', '2021-08-20 01:41:54', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (60, '查看商品详情', 12, 'GET:/pms/spu/detail/**', 'pms:goods:detail', b'0', '2021-08-20 01:43:47', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (61, '查看品牌', 9, 'GET:/pms/brand/list/**', 'pms:brand:list', b'0', '2021-08-20 15:53:08', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (62, '新增品牌', 9, 'POST:/pms/brand/add', 'pms:brand:add', b'0', '2021-08-20 15:53:31', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (63, '修改品牌', 9, 'PUT:/pms/brand/update', 'pms:brand:update', b'0', '2021-08-20 15:54:16', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (64, '删除品牌', 9, 'DELETE:/pms/brand/delete', 'pms:brand:delete', b'0', '2021-08-20 15:54:35', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (65, '更改品牌状态', 9, 'PATCH:/pms/brand/update', 'pms:brand:status', b'0', '2021-08-20 15:55:06', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (66, '查看分类', 10, 'GET:/pms/category/list/**', 'pms:category:list', b'0', '2021-08-20 15:56:32', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (67, '新增分类', 10, 'POST:/pms/category/add', 'pms:category:add', b'0', '2021-08-20 15:57:15', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (68, '修改分类', 10, 'PUT:/pms/category/update', 'pms:category:update', b'0', '2021-08-20 15:57:42', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (69, '删除分类', 10, 'DELETE:/pms/category/delete', 'pms:category:delete', b'0', '2021-08-20 15:58:27', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (70, '查看商品属性', 10, 'GET:/pms/attribute/list/**', 'pms:attribute:list', b'0', '2021-08-20 15:59:50', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (71, '新增商品属性', 10, 'POST:/pms/attribute/add', 'pms:attribute:add', b'0', '2021-08-20 16:00:34', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (72, '修改商品属性', 10, 'PUT:/pms/attribute/update', 'pms:attribute:update', b'0', '2021-08-20 16:13:39', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (73, '删除商品属性', 10, 'DELETE:/pms/attribute/delete', 'pms:attribute:delete', b'0', '2021-08-20 16:14:05', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (74, '设置商品属性', 10, 'PATCH:/pms/attribute/update', 'pms:attribute:set', b'0', '2021-08-20 16:15:13', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (75, '查看商品参数', 10, 'GET:/pms/spec-group/list/**', 'pms:spec-group:list', b'0', '2021-08-20 16:17:39', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (76, '保存商品规格参数', 10, 'POST:/pms/spec-group/save/**', 'pms:spec-group:save', b'0', '2021-08-20 16:20:29', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (77, '分配用户角色', 2, 'PUT:/admin/user/roles', 'admin:user:roles', b'0', '2021-08-20 22:03:30', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (78, '上传文件', 13, 'POST:/upload/file', NULL, b'0', '2021-08-22 16:48:28', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (79, '删除文件', 13, 'DELETE:/upload/file', NULL, b'0', '2021-08-22 16:48:41', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (80, '测试', 13, 'POST:/pms/test', '', b'0', '2021-09-07 15:58:50', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (81, '新增商品规格参数组', 10, 'POST:/pms/spec-group/add', 'pms:sprc-group:add', b'1', '2021-12-05 23:24:19', '2023-11-13 00:07:32');
INSERT INTO `sys_permission` VALUES (82, '查看商品规格参数项', 10, 'GET:/pms/spec-param/list**', 'pms:spec-param:list', b'0', '2021-12-05 23:28:15', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (83, '查看订单列表', 18, 'GET:/oms/order/list**', 'oms:order:list', b'0', '2021-12-05 23:33:57', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (84, '查看订单明细', 18, 'GET:/oms/order/detail/**', 'oms:order:detail', b'0', '2021-12-05 23:34:44', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (85, '更改订单', 18, 'PUT:/oms/order/update', 'oms:order:update', b'0', '2021-12-05 23:35:32', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (86, '删除订单', 18, 'DELETE:/oms/order/delete', 'oms:order:delete', b'0', '2021-12-05 23:36:23', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (87, '订单发货', 18, 'PUT:/oms/order-delivery/order-shipment', 'oms:order:shipment', b'0', '2021-12-05 23:38:01', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (88, '查询订单物流', 18, 'GET:/oms/order-delivery/**', 'oms:order:delivery', b'0', '2021-12-05 23:38:59', '2023-11-13 00:07:24');
INSERT INTO `sys_permission` VALUES (89, '新增学生信息', 22, 'POST:/student/add', 'student:manage:add', b'0', '2023-11-12 23:08:15', '2023-11-12 23:08:15');
INSERT INTO `sys_permission` VALUES (90, '修改学生信息', 22, 'PUT:/student/update', 'student:manage:update', b'0', '2023-11-12 23:08:58', '2023-11-12 23:08:58');
INSERT INTO `sys_permission` VALUES (91, '删除学生信息', 22, 'DELETE:/student/delete', 'student:manage:delete', b'0', '2023-11-12 23:09:48', '2023-11-12 23:09:48');
INSERT INTO `sys_permission` VALUES (92, '查看学生信息', 22, 'GET:/student/list/**', 'student:manage:list', b'0', '2023-11-13 00:09:07', '2023-11-13 00:09:07');
INSERT INTO `sys_permission` VALUES (93, '查询老师信息', 23, 'GET:/teacher/list/**', 'teacher:manage:list', b'0', '2023-11-13 23:28:58', '2023-11-13 23:28:58');
INSERT INTO `sys_permission` VALUES (94, '新增老师信息', 23, 'POST:/teacher/add', 'teacher:manage:add', b'0', '2023-11-13 23:29:24', '2023-11-13 23:29:24');
INSERT INTO `sys_permission` VALUES (95, '修改老师信息', 23, 'PUT:/teacher/update', 'teacher:manage:update', b'0', '2023-11-13 23:29:57', '2023-11-13 23:29:57');
INSERT INTO `sys_permission` VALUES (96, '删除老师信息', 23, 'DELETE:/teacher/delete', 'teacher:manage:delete', b'0', '2023-11-13 23:30:21', '2023-11-13 23:30:21');
INSERT INTO `sys_permission` VALUES (97, '查询企业信息', 25, 'GET:/enterprise/list/**', 'enterprise:manage:list', b'0', '2023-11-14 23:44:50', '2023-11-14 23:44:50');
INSERT INTO `sys_permission` VALUES (98, '新增企业信息', 25, 'POST:/enterprise/add', 'enterprise:manage:add', b'0', '2023-11-14 23:45:16', '2023-11-14 23:45:16');
INSERT INTO `sys_permission` VALUES (99, '修改企业信息', 25, 'PUT:/enterprise/update', 'enterprise:manage:update', b'0', '2023-11-14 23:45:34', '2023-11-14 23:45:34');
INSERT INTO `sys_permission` VALUES (100, '删除企业信息', 25, 'DELETE:/enterprise/delete', 'enterprise:manage:delete', b'0', '2023-11-14 23:45:47', '2023-11-14 23:45:47');
INSERT INTO `sys_permission` VALUES (101, '查看企业岗位', 26, 'GET:/enterprisePosition/list/**', 'enterprise_position:manage:list', b'0', '2023-11-18 10:45:05', '2023-11-18 10:45:05');
INSERT INTO `sys_permission` VALUES (102, '新增岗位', 26, 'POST:/enterprisePosition/add', 'enterprise_position:manage:add', b'0', '2023-11-18 10:45:20', '2023-11-18 10:45:20');
INSERT INTO `sys_permission` VALUES (103, '修改岗位信息', 26, 'PUT:/enterprisePosition/update', 'enterprise_position:manage:update', b'0', '2023-11-18 10:45:45', '2023-11-18 10:45:45');
INSERT INTO `sys_permission` VALUES (104, '删除岗位', 26, 'DELETE:/enterprisePosition/delete', 'enterprise_position:manage:delete', b'1', '2023-11-18 10:46:08', '2024-02-14 00:27:56');
INSERT INTO `sys_permission` VALUES (105, '停止招聘', 26, 'POST:/enterprisePosition/stopPosition', 'enterprise_position:manage:stop', b'0', '2023-11-18 11:59:58', '2023-11-18 11:59:58');
INSERT INTO `sys_permission` VALUES (106, '查看班级', 27, 'GET:/classes/list/**', 'classes:manage:list', b'0', '2023-11-18 16:37:51', '2023-11-18 16:37:51');
INSERT INTO `sys_permission` VALUES (107, '新增班级', 27, 'POST:/classes/add', 'classes:manage:add', b'0', '2023-11-18 16:38:03', '2023-11-18 16:38:03');
INSERT INTO `sys_permission` VALUES (108, '修改班级', 27, 'PUT:/classes/update', 'classes:manage:update', b'0', '2023-11-18 16:38:16', '2023-11-18 16:38:16');
INSERT INTO `sys_permission` VALUES (109, '删除班级', 27, 'DELETE:/classes/delete', 'classes:manage:delete', b'0', '2023-11-18 16:38:32', '2023-11-18 16:38:32');
INSERT INTO `sys_permission` VALUES (110, '查看简历', 28, 'GET:/studentResume/list/**', 'studnet:resume:list', b'0', '2023-11-18 17:25:26', '2023-11-18 17:25:26');
INSERT INTO `sys_permission` VALUES (111, '删除简历', 28, 'DELETE:/studentResume/delete', 'studnet:resume:delete', b'0', '2023-11-18 17:25:43', '2023-11-18 17:25:43');
INSERT INTO `sys_permission` VALUES (112, '上传简历', 28, 'POST:/studentResume/add', 'studnet:resume:add', b'0', '2023-11-18 17:26:06', '2023-11-18 17:26:06');
INSERT INTO `sys_permission` VALUES (113, '应聘回复', 29, 'POST:/internshipApply/reply', 'internship_apply:manage:reply', b'1', '2023-11-30 00:57:42', '2023-12-03 23:47:50');
INSERT INTO `sys_permission` VALUES (114, '应聘通过/拒绝/回复', 29, 'PUT:/internshipApply/update', 'internship_apply:manage:pass', b'0', '2023-11-30 00:58:16', '2023-11-30 00:58:16');
INSERT INTO `sys_permission` VALUES (115, '应聘详情', 29, 'GET:/internshipApply/list/**', 'internship_apply:manage:list', b'0', '2023-12-02 14:19:21', '2023-12-03 15:12:55');
INSERT INTO `sys_permission` VALUES (116, '申请实习', 30, 'POST:/internshipApply/add', 'studnet:internship-apply:apply', b'0', '2023-12-02 17:21:43', '2023-12-02 17:21:43');
INSERT INTO `sys_permission` VALUES (117, '查询岗位信息', 30, 'GET:/enterprisePosition/list/**', 'studnet:internship-apply:list', b'0', '2023-12-03 16:39:39', '2023-12-03 16:39:39');
INSERT INTO `sys_permission` VALUES (118, '查询企业信息', 30, 'GET:/enterprise/list/**', 'studnet:internship-apply:enterprise-list', b'0', '2023-12-03 17:00:53', '2023-12-03 17:00:53');
INSERT INTO `sys_permission` VALUES (119, '绑定学生信息', 31, 'POST:/admin/user/biddingStudent', 'admin:user:biddingStudent', b'0', '2023-12-03 17:29:36', '2023-12-03 17:29:36');
INSERT INTO `sys_permission` VALUES (120, '绑定企业信息', 31, 'POST:/admin/user/biddingEnterprise', 'admin:user:biddingEnterprise', b'0', '2023-12-03 17:30:02', '2023-12-03 17:30:02');
INSERT INTO `sys_permission` VALUES (121, '查看学生实习周报', 33, 'GET:/internshipWeekly/list/**', 'internship:weekly:list', b'0', '2023-12-09 14:46:32', '2023-12-09 14:46:32');
INSERT INTO `sys_permission` VALUES (122, '周报评分', 33, 'POST:/internshipWeeklyScore/add', 'internship:weekly:score', b'0', '2023-12-09 14:47:39', '2023-12-09 14:47:39');
INSERT INTO `sys_permission` VALUES (123, '查看我的实习周报', 34, 'GET:/internshipWeekly/myList/**', 'internship:weekly:myList', b'1', '2023-12-09 14:58:43', '2024-03-16 22:04:34');
INSERT INTO `sys_permission` VALUES (124, '修改我的实习周报', 34, 'POST:/internshipWeekly/update', 'internship:weekly:update', b'1', '2023-12-09 14:59:16', '2024-03-16 22:04:04');
INSERT INTO `sys_permission` VALUES (125, '提交实习周报', 30, 'POST:/internshipWeekly/add', 'internship:weekly:add', b'1', '2023-12-09 15:00:07', '2023-12-09 15:31:45');
INSERT INTO `sys_permission` VALUES (126, '提交实习周报', 35, 'POST:/internshipWeekly/add', 'internship:weekly:add', b'1', '2023-12-09 15:32:13', '2024-03-16 22:01:44');
INSERT INTO `sys_permission` VALUES (127, '查看我的实习申请', 35, 'GET:/internshipApply/myList/**', 'internship:apply:myList', b'1', '2023-12-09 15:32:56', '2024-03-16 22:01:43');
INSERT INTO `sys_permission` VALUES (128, '查看教师信息', 30, 'GET:/teacher/list/**', 'internship:teacher:list', b'0', '2023-12-10 02:26:57', '2023-12-10 02:26:57');
INSERT INTO `sys_permission` VALUES (129, '绑定教师信息', 31, 'POST:/admin/user/biddingTeacher', 'admin:user:biddingTeacher', b'0', '2023-12-10 14:52:51', '2023-12-10 14:52:51');
INSERT INTO `sys_permission` VALUES (130, '查看周报评分', 34, 'GET:/internshipWeeklyScore/list/**', 'internship:weekly-score:list', b'1', '2023-12-10 15:23:00', '2024-03-16 22:03:40');
INSERT INTO `sys_permission` VALUES (131, '公告列表', 37, 'GET:/notice/list/**', 'notice:manage:list', b'0', '2023-12-24 23:02:51', '2023-12-24 23:40:34');
INSERT INTO `sys_permission` VALUES (132, '新增公告', 37, 'POST:/notice/add', 'notice:manage:add', b'0', '2023-12-24 23:03:05', '2023-12-24 23:40:34');
INSERT INTO `sys_permission` VALUES (133, '修改公告信息', 37, 'POST:/notice/update', 'notice:manage:update', b'0', '2023-12-24 23:03:34', '2023-12-24 23:40:34');
INSERT INTO `sys_permission` VALUES (134, '删除公告', 37, 'DELETE:/notice/delete', 'notice:manage:delete', b'0', '2023-12-24 23:03:54', '2023-12-24 23:40:34');
INSERT INTO `sys_permission` VALUES (135, '企业面试记录', 38, 'GET:/internshipInterview/list/**', 'interview:manage:list', b'0', '2024-02-12 21:53:36', '2024-02-14 00:05:09');
INSERT INTO `sys_permission` VALUES (136, '操作/修改面试信息', 38, 'POST:/internshipInterview/update', 'interview:manage:update', b'0', '2024-02-12 22:03:03', '2024-02-12 22:03:03');
INSERT INTO `sys_permission` VALUES (137, '面试不合格', 38, 'POST:/internshipInterview/update', 'interview:manage:reject', b'1', '2024-02-12 22:05:49', '2024-02-13 01:08:33');
INSERT INTO `sys_permission` VALUES (138, '取消面试', 38, 'POST:/internshipInterview/update', 'interview:manage:cancel', b'1', '2024-02-12 22:07:22', '2024-02-13 01:08:31');
INSERT INTO `sys_permission` VALUES (139, '发起面试', 29, 'POST:/internshipInterview/add', 'internship_apply:manage:addInterview', b'0', '2024-02-13 00:58:45', '2024-02-13 00:58:45');
INSERT INTO `sys_permission` VALUES (140, '查看我的实习面试', 39, 'GET:/internshipInterview/myList', 'interview:manage:myList', b'0', '2024-02-13 01:21:08', '2024-02-13 01:21:08');
INSERT INTO `sys_permission` VALUES (141, '提交实习送审', 35, 'POST:/internshipAudit/add', 'internship:audit:add', b'1', '2024-02-13 21:12:36', '2024-03-16 22:01:42');
INSERT INTO `sys_permission` VALUES (142, '查看学生实习审核记录', 40, 'GET:/internshipAudit/list/**', 'internship:audit:list', b'0', '2024-02-13 21:16:29', '2024-02-13 21:16:29');
INSERT INTO `sys_permission` VALUES (143, '通过/拒绝/取消学生实习审核', 40, 'POST:/internshipAudit/update', 'internship:audit:update', b'0', '2024-02-13 21:17:03', '2024-02-13 21:17:03');
INSERT INTO `sys_permission` VALUES (144, '查看实习审核结果', 35, 'GET:/internshipAudit/auditDetail/**', 'internship-apply:audit:list', b'1', '2024-02-13 23:48:32', '2024-03-16 22:01:41');
INSERT INTO `sys_permission` VALUES (145, '查看实习岗位', 40, 'GET:/enterprisePosition/list/**', 'internship:audit:showPosition', b'0', '2024-02-13 23:51:31', '2024-02-13 23:51:31');
INSERT INTO `sys_permission` VALUES (146, '查看实习审核结果', 41, 'GET:/internshipAudit/auditDetail/**', 'internship-apply:audit:list', b'0', '2024-03-16 22:01:54', '2024-03-16 22:01:54');
INSERT INTO `sys_permission` VALUES (147, '提交实习送审', 41, 'POST:/internshipAudit/add', 'internship:audit:add', b'0', '2024-03-16 22:02:15', '2024-03-16 22:02:15');
INSERT INTO `sys_permission` VALUES (148, '查看我的实习申请', 41, 'GET:/internshipApply/myList/**', 'internship:apply:myList', b'0', '2024-03-16 22:02:43', '2024-03-16 22:02:43');
INSERT INTO `sys_permission` VALUES (149, '提交实习周报', 41, 'POST:/internshipWeekly/add', 'internship:weekly:add', b'0', '2024-03-16 22:03:10', '2024-03-16 22:03:10');
INSERT INTO `sys_permission` VALUES (150, '查看周报评分', 41, 'GET:/internshipWeeklyScore/list/**', 'internship:weekly-score:list', b'0', '2024-03-16 22:03:41', '2024-03-16 22:03:41');
INSERT INTO `sys_permission` VALUES (151, '修改我的实习周报', 41, 'POST:/internshipWeekly/update', 'internship:weekly:update', b'0', '2024-03-16 22:04:06', '2024-03-16 22:04:06');
INSERT INTO `sys_permission` VALUES (152, '查看我的实习周报', 41, 'GET:/internshipWeekly/myList/**', 'internship:weekly:myList', b'0', '2024-03-16 22:04:35', '2024-03-16 22:04:35');
INSERT INTO `sys_permission` VALUES (153, '查看我的实习送审记录', 41, 'GET:/internshipAudit/myList/**', 'internship:audit:myList', b'0', '2024-03-17 10:47:31', '2024-03-17 10:47:31');
INSERT INTO `sys_permission` VALUES (154, '查看实习岗位', 41, 'GET:/enterprisePosition/list/**', 'internship:audit:myShowPosition', b'0', '2024-03-17 10:52:40', '2024-03-17 10:52:40');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色编码',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `sort` int NULL DEFAULT NULL COMMENT '显示顺序',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '角色状态（0正常 1停用）',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unx_role_name`(`name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'ROOT', ' 系统最高权限者', 0, 0, b'0', '2021-07-26 14:56:51', '2021-08-18 12:15:53');
INSERT INTO `sys_role` VALUES (2, '系统管理员', 'admin', '拥有系统管理菜单的所有权限', 1, 0, b'0', '2021-07-26 12:39:54', '2021-08-28 18:28:51');
INSERT INTO `sys_role` VALUES (71, '教师', 'teacher', '拥有管理班级，管理学生信息权限', 2, 0, b'0', '2023-12-03 13:44:02', '2023-12-03 13:44:02');
INSERT INTO `sys_role` VALUES (72, '学生', 'student', '学生，可以投递简历', 4, 0, b'0', '2023-12-03 13:47:12', '2023-12-03 17:08:52');
INSERT INTO `sys_role` VALUES (73, '企业管理者', 'enterprise-manage', '拥有管理企业信息的权限', 7, 0, b'0', '2023-12-03 13:53:08', '2023-12-03 13:53:08');
INSERT INTO `sys_role` VALUES (74, '企业', 'enterprise', '查询企业信息，审批学生的实习申请', 1, 0, b'0', '2023-12-03 13:58:52', '2023-12-03 13:58:52');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 25);
INSERT INTO `sys_role_menu` VALUES (1, 23);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (1, 10);
INSERT INTO `sys_role_menu` VALUES (1, 9);
INSERT INTO `sys_role_menu` VALUES (1, 11);
INSERT INTO `sys_role_menu` VALUES (1, 12);
INSERT INTO `sys_role_menu` VALUES (1, 17);
INSERT INTO `sys_role_menu` VALUES (1, 18);
INSERT INTO `sys_role_menu` VALUES (1, 22);
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 41);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 8);
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 13);
INSERT INTO `sys_role_menu` VALUES (1, 14);
INSERT INTO `sys_role_menu` VALUES (1, 15);
INSERT INTO `sys_role_menu` VALUES (1, 16);
INSERT INTO `sys_role_menu` VALUES (1, 26);
INSERT INTO `sys_role_menu` VALUES (65, 2);
INSERT INTO `sys_role_menu` VALUES (66, 2);
INSERT INTO `sys_role_menu` VALUES (2, 31);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 6);
INSERT INTO `sys_role_menu` VALUES (73, 24);
INSERT INTO `sys_role_menu` VALUES (73, 25);
INSERT INTO `sys_role_menu` VALUES (73, 26);
INSERT INTO `sys_role_menu` VALUES (71, 31);
INSERT INTO `sys_role_menu` VALUES (71, 21);
INSERT INTO `sys_role_menu` VALUES (71, 22);
INSERT INTO `sys_role_menu` VALUES (71, 23);
INSERT INTO `sys_role_menu` VALUES (71, 27);
INSERT INTO `sys_role_menu` VALUES (71, 32);
INSERT INTO `sys_role_menu` VALUES (71, 40);
INSERT INTO `sys_role_menu` VALUES (71, 33);
INSERT INTO `sys_role_menu` VALUES (72, 31);
INSERT INTO `sys_role_menu` VALUES (72, 32);
INSERT INTO `sys_role_menu` VALUES (72, 28);
INSERT INTO `sys_role_menu` VALUES (72, 30);
INSERT INTO `sys_role_menu` VALUES (72, 35);
INSERT INTO `sys_role_menu` VALUES (72, 39);
INSERT INTO `sys_role_menu` VALUES (72, 34);
INSERT INTO `sys_role_menu` VALUES (72, 41);
INSERT INTO `sys_role_menu` VALUES (74, 31);
INSERT INTO `sys_role_menu` VALUES (74, 24);
INSERT INTO `sys_role_menu` VALUES (74, 25);
INSERT INTO `sys_role_menu` VALUES (74, 26);
INSERT INTO `sys_role_menu` VALUES (74, 29);
INSERT INTO `sys_role_menu` VALUES (74, 38);

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色id',
  `permission_id` bigint NULL DEFAULT NULL COMMENT '资源id',
  INDEX `permission_id`(`permission_id` ASC) USING BTREE,
  INDEX `role_id`(`role_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (2, 18);
INSERT INTO `sys_role_permission` VALUES (2, 17);
INSERT INTO `sys_role_permission` VALUES (2, 16);
INSERT INTO `sys_role_permission` VALUES (2, 15);
INSERT INTO `sys_role_permission` VALUES (2, 14);
INSERT INTO `sys_role_permission` VALUES (2, 50);
INSERT INTO `sys_role_permission` VALUES (2, 49);
INSERT INTO `sys_role_permission` VALUES (2, 48);
INSERT INTO `sys_role_permission` VALUES (2, 47);
INSERT INTO `sys_role_permission` VALUES (2, 23);
INSERT INTO `sys_role_permission` VALUES (2, 22);
INSERT INTO `sys_role_permission` VALUES (2, 21);
INSERT INTO `sys_role_permission` VALUES (2, 20);
INSERT INTO `sys_role_permission` VALUES (2, 19);
INSERT INTO `sys_role_permission` VALUES (2, 32);
INSERT INTO `sys_role_permission` VALUES (2, 31);
INSERT INTO `sys_role_permission` VALUES (2, 30);
INSERT INTO `sys_role_permission` VALUES (2, 29);
INSERT INTO `sys_role_permission` VALUES (2, 28);
INSERT INTO `sys_role_permission` VALUES (2, 27);
INSERT INTO `sys_role_permission` VALUES (2, 26);
INSERT INTO `sys_role_permission` VALUES (2, 25);
INSERT INTO `sys_role_permission` VALUES (2, 24);
INSERT INTO `sys_role_permission` VALUES (2, 42);
INSERT INTO `sys_role_permission` VALUES (2, 41);
INSERT INTO `sys_role_permission` VALUES (2, 40);
INSERT INTO `sys_role_permission` VALUES (2, 39);
INSERT INTO `sys_role_permission` VALUES (2, 38);
INSERT INTO `sys_role_permission` VALUES (2, 37);
INSERT INTO `sys_role_permission` VALUES (2, 36);
INSERT INTO `sys_role_permission` VALUES (2, 35);
INSERT INTO `sys_role_permission` VALUES (2, 34);
INSERT INTO `sys_role_permission` VALUES (2, 33);
INSERT INTO `sys_role_permission` VALUES (2, 46);
INSERT INTO `sys_role_permission` VALUES (2, 45);
INSERT INTO `sys_role_permission` VALUES (2, 44);
INSERT INTO `sys_role_permission` VALUES (2, 43);
INSERT INTO `sys_role_permission` VALUES (2, 92);
INSERT INTO `sys_role_permission` VALUES (2, 91);
INSERT INTO `sys_role_permission` VALUES (2, 90);
INSERT INTO `sys_role_permission` VALUES (2, 89);
INSERT INTO `sys_role_permission` VALUES (2, 96);
INSERT INTO `sys_role_permission` VALUES (2, 95);
INSERT INTO `sys_role_permission` VALUES (2, 100);
INSERT INTO `sys_role_permission` VALUES (2, 99);
INSERT INTO `sys_role_permission` VALUES (2, 98);
INSERT INTO `sys_role_permission` VALUES (2, 97);
INSERT INTO `sys_role_permission` VALUES (2, 112);
INSERT INTO `sys_role_permission` VALUES (2, 111);
INSERT INTO `sys_role_permission` VALUES (2, 110);
INSERT INTO `sys_role_permission` VALUES (2, 113);
INSERT INTO `sys_role_permission` VALUES (74, 97);
INSERT INTO `sys_role_permission` VALUES (74, 105);
INSERT INTO `sys_role_permission` VALUES (74, 104);
INSERT INTO `sys_role_permission` VALUES (74, 103);
INSERT INTO `sys_role_permission` VALUES (74, 102);
INSERT INTO `sys_role_permission` VALUES (74, 101);
INSERT INTO `sys_role_permission` VALUES (74, 113);
INSERT INTO `sys_role_permission` VALUES (2, 77);
INSERT INTO `sys_role_permission` VALUES (2, 5);
INSERT INTO `sys_role_permission` VALUES (2, 3);
INSERT INTO `sys_role_permission` VALUES (2, 2);
INSERT INTO `sys_role_permission` VALUES (2, 1);
INSERT INTO `sys_role_permission` VALUES (73, 105);
INSERT INTO `sys_role_permission` VALUES (73, 104);
INSERT INTO `sys_role_permission` VALUES (73, 103);
INSERT INTO `sys_role_permission` VALUES (73, 102);
INSERT INTO `sys_role_permission` VALUES (73, 101);
INSERT INTO `sys_role_permission` VALUES (73, 100);
INSERT INTO `sys_role_permission` VALUES (73, 99);
INSERT INTO `sys_role_permission` VALUES (73, 98);
INSERT INTO `sys_role_permission` VALUES (73, 97);
INSERT INTO `sys_role_permission` VALUES (71, 92);
INSERT INTO `sys_role_permission` VALUES (71, 91);
INSERT INTO `sys_role_permission` VALUES (71, 90);
INSERT INTO `sys_role_permission` VALUES (71, 89);
INSERT INTO `sys_role_permission` VALUES (72, 112);
INSERT INTO `sys_role_permission` VALUES (72, 111);
INSERT INTO `sys_role_permission` VALUES (72, 110);
INSERT INTO `sys_role_permission` VALUES (74, 120);
INSERT INTO `sys_role_permission` VALUES (72, 119);
INSERT INTO `sys_role_permission` VALUES (72, 128);
INSERT INTO `sys_role_permission` VALUES (72, 118);
INSERT INTO `sys_role_permission` VALUES (72, 117);
INSERT INTO `sys_role_permission` VALUES (72, 116);
INSERT INTO `sys_role_permission` VALUES (72, 93);
INSERT INTO `sys_role_permission` VALUES (71, 94);
INSERT INTO `sys_role_permission` VALUES (71, 93);
INSERT INTO `sys_role_permission` VALUES (71, 129);
INSERT INTO `sys_role_permission` VALUES (71, 122);
INSERT INTO `sys_role_permission` VALUES (71, 121);
INSERT INTO `sys_role_permission` VALUES (74, 122);
INSERT INTO `sys_role_permission` VALUES (74, 121);
INSERT INTO `sys_role_permission` VALUES (72, 130);
INSERT INTO `sys_role_permission` VALUES (72, 124);
INSERT INTO `sys_role_permission` VALUES (72, 123);
INSERT INTO `sys_role_permission` VALUES (71, 109);
INSERT INTO `sys_role_permission` VALUES (71, 108);
INSERT INTO `sys_role_permission` VALUES (71, 107);
INSERT INTO `sys_role_permission` VALUES (71, 106);
INSERT INTO `sys_role_permission` VALUES (72, 140);
INSERT INTO `sys_role_permission` VALUES (74, 136);
INSERT INTO `sys_role_permission` VALUES (74, 135);
INSERT INTO `sys_role_permission` VALUES (74, 139);
INSERT INTO `sys_role_permission` VALUES (74, 115);
INSERT INTO `sys_role_permission` VALUES (74, 114);
INSERT INTO `sys_role_permission` VALUES (72, 144);
INSERT INTO `sys_role_permission` VALUES (72, 141);
INSERT INTO `sys_role_permission` VALUES (72, 127);
INSERT INTO `sys_role_permission` VALUES (72, 126);
INSERT INTO `sys_role_permission` VALUES (71, 145);
INSERT INTO `sys_role_permission` VALUES (71, 143);
INSERT INTO `sys_role_permission` VALUES (71, 142);
INSERT INTO `sys_role_permission` VALUES (72, 154);
INSERT INTO `sys_role_permission` VALUES (72, 153);
INSERT INTO `sys_role_permission` VALUES (72, 152);
INSERT INTO `sys_role_permission` VALUES (72, 151);
INSERT INTO `sys_role_permission` VALUES (72, 150);
INSERT INTO `sys_role_permission` VALUES (72, 149);
INSERT INTO `sys_role_permission` VALUES (72, 148);
INSERT INTO `sys_role_permission` VALUES (72, 147);
INSERT INTO `sys_role_permission` VALUES (72, 146);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `nick_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `student_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '绑定学生编号',
  `enterprise_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '绑定企业id',
  `teacher_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '绑定教师id',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户头像',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `status` tinyint UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户状态（0正常 1禁用）',
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unx_user_name`(`user_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'root', '超级管理员', 'B7o2532X2KdiVQ6FmVYOPg==', '', NULL, NULL, 'http://localhost:8080/internship/file/download/6956ce9174c94275a34ddeda5df8229d.jpg', '13025438446', 0, NULL, b'0', '2023-11-06 23:41:32', '2023-12-05 00:06:29');
INSERT INTO `sys_user` VALUES (2, 'admin001', '系统管理员1', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, NULL, NULL, 'http://localhost:8080/internship/file/download/af89cafa930e402584b1c5fa61cc79b4.jpg', '15176216721', 0, NULL, b'0', '2023-12-03 14:19:30', '2023-12-03 14:23:21');
INSERT INTO `sys_user` VALUES (3, 'is0001', '许老师', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, NULL, 5, 'http://localhost:8080/internship/file/download/7a25ee61f0f746b2908e9517829561be.jpg', '18761276216', 0, NULL, b'0', '2023-12-03 14:27:49', '2024-03-16 22:37:10');
INSERT INTO `sys_user` VALUES (4, 'is0002', '李鸣宇', 'B7o2532X2KdiVQ6FmVYOPg==', 'C1_zhangsan', NULL, NULL, 'http://localhost:8080/internship/file/download/5a0cb8210b53439aa11948c5e89a3ebe.jpg', '19895651651', 0, NULL, b'0', '2023-12-03 14:34:03', '2023-12-03 17:41:24');
INSERT INTO `sys_user` VALUES (5, 'is0003', '企业阿里巴巴', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, 1, NULL, 'http://localhost:8080/internship/file/download/673b7cb4bb4f4edf84b8a4af770d64b1.jpg', '15653232132', 0, NULL, b'0', '2023-12-03 14:37:02', '2023-12-03 23:33:45');
INSERT INTO `sys_user` VALUES (6, 'is0004', '企业腾讯', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, 2, NULL, 'http://localhost:8080/internship/file/download/63b9f0a680b543aa822d961303c6e249.jpg', '16592135131', 0, NULL, b'0', '2023-12-03 14:38:47', '2023-12-03 23:44:13');
INSERT INTO `sys_user` VALUES (7, 'is0005', '企业管理员', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, NULL, NULL, 'http://localhost:8080/internship/file/download/f9f7bea703c14e38a9b68185d7811700.png', '19513133156', 0, NULL, b'0', '2023-12-03 14:39:13', '2023-12-03 14:40:07');
INSERT INTO `sys_user` VALUES (8, 'istest01', '刘五', 'B7o2532X2KdiVQ6FmVYOPg==', 'C1_liuwu', NULL, NULL, 'http://localhost:8080/internship/file/download/ad31f67f2f9146788de24f8a8d590d78.jpg', '18951323515', 0, NULL, b'0', '2023-12-03 23:41:28', '2024-02-13 20:21:30');
INSERT INTO `sys_user` VALUES (9, 'teststudnet01', '测试学生', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, NULL, NULL, 'http://localhost:8080/internship/file/download/cb704a55a38744919b4aa85b6bfc6ac7.jpg', '16313165131', 0, NULL, b'0', '2023-12-04 00:02:44', '2023-12-04 00:02:51');
INSERT INTO `sys_user` VALUES (10, 'teste01', '测试企业-腾讯', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, NULL, NULL, 'http://localhost:8080/internship/file/download/f78d333fc7d94e7492d3d0efc4899872.jpg', '19625635625', 0, NULL, b'0', '2023-12-04 00:03:28', '2023-12-04 00:03:39');
INSERT INTO `sys_user` VALUES (11, '3c8efadf2c69450eac82d8584708f5b4', '演示学生账号', 'B7o2532X2KdiVQ6FmVYOPg==', 'test_studnet_01', NULL, NULL, 'http://localhost:8080/internship/file/download/3a70f5b1ceb145fc9ce2069b7a7ebd3a.jpg', '16595262628', 0, NULL, b'1', '2023-12-04 00:15:46', '2024-02-26 23:05:11');
INSERT INTO `sys_user` VALUES (12, 'yse01', '演示企业-阿里巴巴', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, 1, NULL, 'http://localhost:8080/internship/file/download/afaf423771c648c3b1d5462d210e0663.jpg', '18563151165', 0, NULL, b'0', '2023-12-04 00:16:28', '2023-12-04 00:19:38');
INSERT INTO `sys_user` VALUES (13, 'ysstu01', '李四', 'B7o2532X2KdiVQ6FmVYOPg==', 'C1_lisi', NULL, NULL, 'http://localhost:8080/internship/file/download/aa37fa5d0a804c72ad72b1218aacd362.jpg', '14752458554', 0, NULL, b'0', '2023-12-04 00:45:42', '2023-12-04 00:47:01');
INSERT INTO `sys_user` VALUES (14, 'ysee01', '演示企业-腾讯账号', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, 2, NULL, 'http://localhost:8080/internship/file/download/5d18320f309549aab8ce3d70200e195d.jpg', '18956165151', 0, NULL, b'0', '2023-12-04 00:46:16', '2023-12-04 00:48:57');
INSERT INTO `sys_user` VALUES (20, 'ysstudent01', '测试学生', 'B7o2532X2KdiVQ6FmVYOPg==', 'test_studnet_01', NULL, NULL, 'http://localhost:8080/internship/file/download/898bc4e71e914102a90a7749aa544c7a.jpg', '18951603216', 0, NULL, b'0', '2024-02-26 23:05:48', '2024-02-26 23:06:17');
INSERT INTO `sys_user` VALUES (21, 'stu001', '小王', 'B7o2532X2KdiVQ6FmVYOPg==', 'stu001', NULL, NULL, 'http://localhost:8080/internship/file/download/ca0bf1ab10204a1c96abe22044f2f0f9.png', '18777524552', 0, NULL, b'0', '2024-04-15 17:22:21', '2024-04-15 17:29:59');
INSERT INTO `sys_user` VALUES (22, 'Dr001', '石老师', 'Rxa4fg0evi8HrInnmyIXPA==', NULL, NULL, 6, '', '14777854774', 0, NULL, b'0', '2024-04-15 17:24:00', '2024-04-15 22:33:08');
INSERT INTO `sys_user` VALUES (23, 'Dr002', '陈老师', 'B7o2532X2KdiVQ6FmVYOPg==', NULL, NULL, 7, '', '18777386002', 0, NULL, b'0', '2024-04-15 22:45:35', '2024-04-15 22:46:55');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (3, 71);
INSERT INTO `sys_user_role` VALUES (4, 72);
INSERT INTO `sys_user_role` VALUES (5, 74);
INSERT INTO `sys_user_role` VALUES (6, 74);
INSERT INTO `sys_user_role` VALUES (7, 73);
INSERT INTO `sys_user_role` VALUES (8, 72);
INSERT INTO `sys_user_role` VALUES (9, 72);
INSERT INTO `sys_user_role` VALUES (10, 74);
INSERT INTO `sys_user_role` VALUES (11, 72);
INSERT INTO `sys_user_role` VALUES (12, 74);
INSERT INTO `sys_user_role` VALUES (13, 72);
INSERT INTO `sys_user_role` VALUES (14, 74);
INSERT INTO `sys_user_role` VALUES (20, 72);
INSERT INTO `sys_user_role` VALUES (21, 72);
INSERT INTO `sys_user_role` VALUES (22, 71);
INSERT INTO `sys_user_role` VALUES (23, 71);

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `teacher_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教师姓名',
  `teacher_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '教师工号',
  `classes_ids` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级id数组',
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '职位',
  `phone_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识（0未删除 1已删除）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unx_teacher_code`(`teacher_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教师表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES (1, '吴老师', 'T_wu', '1,2,5', '数学老师A', '15599176423', b'0', '2023-11-13 23:31:11', '2024-01-07 15:25:35');
INSERT INTO `teacher` VALUES (2, '王老师', 'w_01', '4', '语文老师', '18888956312', b'0', '2023-11-13 23:46:32', '2024-01-07 15:25:35');
INSERT INTO `teacher` VALUES (3, '李老师', 'teacher_li', '4', '计算机教师', '18595651303', b'0', '2023-12-03 23:59:57', '2024-01-07 15:25:35');
INSERT INTO `teacher` VALUES (4, '刘老师', 'teacher_liu', '4,1,2,3', '数学教师', '15596325461', b'0', '2023-12-04 00:12:21', '2024-01-07 15:25:35');
INSERT INTO `teacher` VALUES (5, '许老师', 'teacher_xu', '1', '计算机老师', '18966515132', b'0', '2023-12-04 00:37:01', '2024-01-07 15:25:35');
INSERT INTO `teacher` VALUES (6, '石老师', 'tea_001', '4', '教授', '14777852333', b'0', '2024-04-15 17:27:38', '2024-04-15 17:27:38');
INSERT INTO `teacher` VALUES (7, '陈老师', 'tea_002', '1', '教授', '14411254225', b'0', '2024-04-15 22:46:43', '2024-04-15 22:46:43');

SET FOREIGN_KEY_CHECKS = 1;
