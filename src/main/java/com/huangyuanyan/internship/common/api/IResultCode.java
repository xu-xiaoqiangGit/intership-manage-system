package com.huangyuanyan.internship.common.api;

public interface IResultCode {

    /**
     * 获取响应码
     */
    String getCode();

    /**
     * 获取提示信息
     */
    String getMessage();
}
