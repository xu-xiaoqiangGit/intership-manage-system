package com.huangyuanyan.internship.common.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * 接口统一返回体
 *
 * @param <T> 数据
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ResponseResult<T> implements Serializable {

    private String code;

    private String message;

    private T data;

    private Integer total;

    public ResponseResult() {

    }

    public ResponseResult(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResponseResult(String code, String message, T data, Integer total) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.total = total;
    }

    /**
     * 成功返回结果
     */
    public static <T> ResponseResult<T> success() {
        return new ResponseResult<T>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), null);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> ResponseResult<T> success(T data) {
        return new ResponseResult<T>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data  获取的数据
     * @param total 总记录数
     */
    public static <T> ResponseResult<T> success(T data, Long total) {
        return new ResponseResult<T>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data, total.intValue());
    }

    /**
     * 成功返回结果
     *
     * @param message 提示信息
     */
    public static <T> ResponseResult<T> success(String message) {
        return new ResponseResult<T>(ResultCode.SUCCESS.getCode(), message, null);
    }

    /**
     * 成功返回结果
     *
     * @param data    获取的数据
     * @param message 提示信息
     */
    public static <T> ResponseResult<T> success(T data, String message) {
        return new ResponseResult<T>(ResultCode.SUCCESS.getCode(), message, data);
    }

    /**
     * 失败返回结果
     *
     * @param resultCode 响应码
     */
    public static <T> ResponseResult<T> failed(IResultCode resultCode) {
        return new ResponseResult<T>(resultCode.getCode(), resultCode.getMessage(), null);
    }

    /**
     * 失败返回结果
     *
     * @param iResultCode 响应码
     */
    public static <T> ResponseResult<T> failed(IResultCode iResultCode, String message) {
        return new ResponseResult<T>(iResultCode.getCode(), message, null);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> ResponseResult<T> failed(String message) {
        return new ResponseResult<T>(ResultCode.FAILED.getCode(), message, null);
    }

    /**
     * 失败返回结果
     */
    public static <T> ResponseResult<T> failed() {
        return failed(ResultCode.FAILED);
    }

    /**
     * 判定是否成功
     *
     * @param result 结果
     */
    public static <T> ResponseResult<T> judge(boolean result) {
        if (result) {
            return ResponseResult.success();
        }
        return ResponseResult.failed();
    }

}
