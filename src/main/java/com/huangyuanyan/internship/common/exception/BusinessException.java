package com.huangyuanyan.internship.common.exception;

import com.huangyuanyan.internship.common.api.IResultCode;

/**
 * @Description 业务异常
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:53
 */
public class BusinessException extends BaseException {

    private IResultCode resultCode;

    public BusinessException(String message, IResultCode resultCode) {
        super(message);
        this.resultCode = resultCode;
    }

    public BusinessException(IResultCode resultCode) {
        super(resultCode.getMessage());
        this.resultCode = resultCode;
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(String message) {
        super(message);
    }

    public IResultCode getResultCode() {
        return resultCode;
    }
}
