package com.huangyuanyan.internship.common.exception;

import com.huangyuanyan.internship.common.api.IResultCode;

/**
 * @Description 断言处理类
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:52
 */
public class Asserts {

    /**
     * 失败
     * @param condition
     * @param message
     */
    public static void failed(boolean condition, String message) {
        if (condition) {
            throw new BusinessException(message);
        }
    }

    public static void failed(String message) {
        throw new BusinessException(message);
    }

    public static void failed(boolean condition, IResultCode resultCode) {
        if (condition) {
            throw new BusinessException(resultCode);
        }
    }
}
