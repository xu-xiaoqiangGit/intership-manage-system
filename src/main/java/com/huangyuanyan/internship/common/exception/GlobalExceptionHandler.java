package com.huangyuanyan.internship.common.exception;

import cn.dev33.satoken.exception.NotLoginException;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * @Description 全局异常捕获组件
 * @Author huangyuanyan
 * @Date Created in 2023-10-16 0:16
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 自定义API异常处理方法
     */
    @ExceptionHandler(value = BaseException.class)
    public ResponseResult<?> handlerApiException(BaseException e) {
        logger.error("业务发生异常：{}", e.getMessage(), e);
        return ResponseResult.failed(e.getMessage());
    }

    @ExceptionHandler(value = NotLoginException.class)
    public ResponseResult<?> handlerNotLoginException(NotLoginException e) {
        logger.error("token 失效：{}", e.getMessage(), e);
        return ResponseResult.failed(ResultCode.TOKEN_INVALID_OR_EXPIRED);
    }

    /**
     * 缺少请求体异常处理器
     */
    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public ResponseResult<?> handlerMissingServletRequestParameterException(MissingServletRequestParameterException e) {
        logger.error("缺少请求体异常：{}", e.getMessage(), e);
        return ResponseResult.failed(ResultCode.PARAM_ERROR, "请求参数：" + e.getParameterName() + "不能为空");
    }

    /**
     * 参数校验异常处理器
     * RequestBody参数校验
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseResult<?> handlerMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        logger.error("请求参数异常：{}", e.getMessage());
        return ResponseResult.failed(ResultCode.PARAM_ERROR, e.getBindingResult().getAllErrors()
                .stream()
                .findFirst()
                .map(ObjectError::getDefaultMessage)
                .orElse("请求参数异常"));
    }

    /**
     * 参数校验异常处理器
     * RequestParam / PathVariable 参数校验
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseResult<?> handlerConstraintViolationException(ConstraintViolationException e) {
        logger.error("请求参数异常：{}", e.getMessage());
        return ResponseResult.failed(ResultCode.PARAM_ERROR, e.getConstraintViolations()
                .stream()
                .findFirst()
                .map(ConstraintViolation::getMessage)
                .orElse("请求参数异常"));
    }

    /**
     * 运行时异常
     */
    @ExceptionHandler(value = RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseResult<?> handlerRuntimeException(RuntimeException e) {
        logger.error("运行时异常：{}", e.getMessage(), e);
        return ResponseResult.failed("系统异常");
    }

    /**
     * 异常
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseResult<?> handlerException(Exception e) {
        logger.error("系统异常：{}", e.getMessage(), e);
        return ResponseResult.failed("系统异常");
    }
}
