package com.huangyuanyan.internship.common.exception;

/**
 * @Description 自定义运行时异常
 * @Author huangyuanyan
 * @Date Created in 2023-10-16 0:16
 */
public class BaseException extends RuntimeException{

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
