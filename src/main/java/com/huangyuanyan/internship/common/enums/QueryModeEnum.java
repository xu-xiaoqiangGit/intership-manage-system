package com.huangyuanyan.internship.common.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: huangyuanyan
 * @Description: 查询模式枚举
 * @Date Created in 2023-08-03 23:03
 */
public enum QueryModeEnum {

    PAGE("page"),   //分页
    LIST("list"),   //列表
    TREE("tree"),   //树状
    CASCADE("cascade"); //级联

    @Getter
    @Setter
    private String code;

    QueryModeEnum(String code) {
        this.code = code;
    }

    public static QueryModeEnum getByCode(String code) {
        for (QueryModeEnum modeEnum : values()) {
            if (modeEnum.getCode().equals(code)) {
                return modeEnum;
            }
        }
        // 默认分页查询
        return PAGE;
    }

}
