package com.huangyuanyan.internship.common.util;

import cn.hutool.json.JSONUtil;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @Author: huangyuanyan
 * @Description: 响应工具类
 * @Date Created in 2023-10-26 21:38
 */
public class ResponseUtils {

    /**
     * response 写入错误信息
     *
     * @param response
     * @param resultCode 结果code
     * @throws IOException
     */
    public static void writerErrorInfo(HttpServletResponse response, ResultCode resultCode) throws IOException {
        response.setStatus(HttpStatus.OK.value());
        response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Cache-Control", "no-cache");
        String body = JSONUtil.toJsonStr(ResponseResult.failed(resultCode));
        response.getOutputStream().write(body.getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }
}
