package com.huangyuanyan.internship.common.util;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.stp.StpUtil;
import com.huangyuanyan.internship.business.admin.vo.UserVO;
import com.huangyuanyan.internship.common.constant.AuthConstant;
import lombok.extern.slf4j.Slf4j;

/**
 * session工具类
 */
@Slf4j
public class SessionUtils {

    /**
     * 获取当前登录的用户信息
     *
     * @return UserVO
     */
    public static UserVO getCurrentUser() {
        try {
            return (UserVO) StpUtil.getSession().get(AuthConstant.USER_INFO);
        } catch (Exception e) {
            log.error("获取用户失败，当前用户未登录");
            throw new NotLoginException("用户会话已过期", "login", "-1");
        }
    }

    /**
     * 获取当前登录的用户信息，未登录则返回null
     *
     * @return UserVO
     */
    public static UserVO getCurrentUserOrNull() {
        try {
            return (UserVO) StpUtil.getSession().get(AuthConstant.USER_INFO);
        } catch (Exception e) {
            log.error("获取用户失败，当前用户未登录");
            return null;
        }
    }
}
