package com.huangyuanyan.internship.common.constant;

/**
 * 全局常量
 */
public interface GlobalConstant {

    /**
     * 默认用户密码
     */
    String DEFAULT_USER_PASSWORD = "123456";

    /**
     * root角色编码
     */
    String ROOT_ROLE_CODE = "ROOT";

    /**
     * 根菜单ID
     */
    Long ROOT_MENU_ID = 0L;

    /**
     * 正常状态
     */
    Integer STATUS_YES = 0;

    /**
     * 企业角色编码
     */
    String ENTERPRISE_ROLE = "enterprise";

    /**
     * 学生角色编码
     */
    String STUDENT_ROLE = "student";

    /**
     * 教师角色编码
     */
    String TEACHER_ROLE = "teacher";
}
