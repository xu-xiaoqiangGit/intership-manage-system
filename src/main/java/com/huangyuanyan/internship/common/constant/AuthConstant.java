package com.huangyuanyan.internship.common.constant;

/**
 * 认证常量
 */
public interface AuthConstant {

    /**
     * 认证请求头key
     */
    String AUTHORIZATION_KEY = "Authorization";

    String USER_INFO = "userInfo";

    String ENCRYPT_KEY = "internship";
}
