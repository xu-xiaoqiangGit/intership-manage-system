package com.huangyuanyan.internship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-16 0:15
 */
@SpringBootApplication
@EnableTransactionManagement
public class InternshipManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(InternshipManageApplication.class, args);
    }
}
