package com.huangyuanyan.internship.business.teacher.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.classes.model.Classes;
import com.huangyuanyan.internship.business.classes.service.ClassesService;
import com.huangyuanyan.internship.business.teacher.mapper.TeacherMapper;
import com.huangyuanyan.internship.business.teacher.model.Teacher;
import com.huangyuanyan.internship.business.teacher.service.TeacherService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-13 23:00
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {

    @Resource
    private ClassesService classesService;

    @Override
    public Page<Teacher> list(Page<Teacher> page, Teacher teacher) {
        List<Teacher> teachers = this.baseMapper.list(page, teacher);
        if (CollectionUtils.isEmpty(teachers)) {
            return page;
        }

        Map<Long, Classes> classesMap = classesService.list()
                .stream().collect(Collectors.toMap(Classes::getId, Function.identity()));

        // 设置班级名称
        teachers.forEach(dbTeacher -> {
            this.setupClassesNames(classesMap, dbTeacher);
        });

        page.setRecords(teachers);
        return page;
    }

    private void setupClassesNames(Map<Long, Classes> classesMap, Teacher teacher) {
        List<Long> classesIds = teacher.splitClassesIds();

        // 获取班级名称，逗号拼接
        String classesNames = classesIds.stream().map(classesMap::get)
                .filter(Objects::nonNull)
                .map(Classes::getClassesName)
                .collect(Collectors.joining("，"));

        teacher.setClassesNames(classesNames);
    }

    @Override
    public Teacher getByTeacherCode(String teacherCode) {
        LambdaQueryWrapper<Teacher> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Teacher::getTeacherCode, teacherCode);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public Teacher getById(Serializable id) {
        Teacher teacher = super.getById(id);
        Map<Long, Classes> classesMap = classesService.list()
                .stream().collect(Collectors.toMap(Classes::getId, Function.identity()));

        // 设置班级名称
        this.setupClassesNames(classesMap, teacher);
        return teacher;
    }
}
