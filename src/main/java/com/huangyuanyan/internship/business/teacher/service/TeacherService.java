package com.huangyuanyan.internship.business.teacher.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.teacher.model.Teacher;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-13 22:59
 */
public interface TeacherService extends IService<Teacher> {

    /**
     * 分页查询教师列表
     *
     * @param page
     * @param teacher 查询条件
     * @return
     */
    Page<Teacher> list(Page<Teacher> page, Teacher teacher);

    /**
     * 通过教师工号查询
     *
     * @param teacherCode 教师工号
     * @return 教师信息
     */
    Teacher getByTeacherCode(String teacherCode);
}
