package com.huangyuanyan.internship.business.teacher.controller;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.teacher.model.Teacher;
import com.huangyuanyan.internship.business.teacher.service.TeacherService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BusinessException;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:35
 */
@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
@Slf4j
public class TeacherController {

    private final TeacherService teacherService;

    @ApiOperation(value = "教师列表分页")
    @GetMapping("/list")
    public ResponseResult<List<Teacher>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String teacherName,
            String teacherCode,
            String phoneNumber,
            Long classesId
    ) {
        // 设置查询条件
        Teacher teacher = new Teacher();
        teacher.setTeacherName(teacherName);
        teacher.setTeacherCode(teacherCode);
        teacher.setPhoneNumber(phoneNumber);
        teacher.setClassesId(classesId);
        Page<Teacher> result = teacherService.list(new Page<>(page, limit), teacher);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "教师详情")
    @GetMapping("/list/{id}")
    public ResponseResult<Teacher> get(@PathVariable("id") Long id) {
        Teacher teacher = teacherService.getById(id);
        teacher.splitClassesIds();
        return ResponseResult.success(teacher);
    }

    @ApiOperation(value = "新增教师")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Teacher teacher) {
        Teacher selectedByCode = teacherService.getByTeacherCode(teacher.getTeacherCode());
        // 判断工号是否已存在
        if (null != selectedByCode) {
            throw new BusinessException("教师工号[" + teacher.getTeacherCode() + "]已存在，请重新填写");
        }
        // 拼接班级id
        teacher.joinClassesIdList();
        boolean result = teacherService.save(teacher);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改教师")
    @PutMapping({"/update", "/personalUpdate"})
    public ResponseResult<?> update(@RequestBody Teacher teacher) {
        Teacher selectedByCode = teacherService.getByTeacherCode(teacher.getTeacherCode());
        // 判断工号是否已存在
        if (null != selectedByCode && !selectedByCode.getId().equals(teacher.getId())) {
            throw new BusinessException("教师工号[" + teacher.getTeacherCode() + "]已存在，请重新填写");
        }
        // 拼接班级id
        teacher.joinClassesIdList();
        boolean result = teacherService.updateById(teacher);
        return ResponseResult.judge(result);
    }


    @ApiOperation(value = "删除教师")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        Teacher dbTeacher = teacherService.getById(ids[0]);
        if (Objects.isNull(dbTeacher)) {
            throw new BusinessException("教师信息已被删除，无需重复操作");
        }
        // 要删除的教师数据，教师工号设为随机值，避免占用唯一索引
        dbTeacher.setTeacherCode(IdUtil.simpleUUID());
        teacherService.updateById(dbTeacher);

        boolean result = teacherService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}
