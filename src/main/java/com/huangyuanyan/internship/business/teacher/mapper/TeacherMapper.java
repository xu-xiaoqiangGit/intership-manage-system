package com.huangyuanyan.internship.business.teacher.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.teacher.model.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:34
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

    /**
     * 分页查询教师列表sql
     *
     * @param page
     * @param teacher 查询条件
     * @return
     */
    List<Teacher> list(Page<Teacher> page, @Param("teacher") Teacher teacher);
}
