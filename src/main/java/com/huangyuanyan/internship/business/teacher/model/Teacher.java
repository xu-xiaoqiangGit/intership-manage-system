package com.huangyuanyan.internship.business.teacher.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-13 22:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("teacher")
@ApiModel("教师表实体类")
public class Teacher extends BaseEntity {

    @ApiModelProperty("教师姓名")
    private String teacherName;

    @ApiModelProperty("教师工号")
    private String teacherCode;

    @ApiModelProperty("班级表主键id数组")
    private String classesIds;

    @ApiModelProperty("职位")
    private String position;

    @ApiModelProperty("联系方式")
    private String phoneNumber;

    /******以下字段用于前端展示*******/
    @TableField(exist = false)
    @ApiModelProperty("班级id")
    private Long classesId;

    @TableField(exist = false)
    @ApiModelProperty("班级名称（多个）")
    private String classesNames;

    @TableField(exist = false)
    @ApiModelProperty("班级id（多个）")
    private List<Long> classesIdList;

    /**
     * 拆分班级主键id数组
     */
    public List<Long> splitClassesIds() {
        if (StringUtils.isNotBlank(classesIds)) {
            this.classesIdList = Arrays.stream(StringUtils.split(this.classesIds, ","))
                    .map(Long::valueOf).collect(Collectors.toList());
            return this.classesIdList;
        }
        return new ArrayList<>();
    }

    /**
     * 拼接班级主键id数组
     */
    public String joinClassesIdList() {
        if (CollectionUtils.isNotEmpty(this.classesIdList)) {
            this.classesIds = this.classesIdList.stream()
                    .map(String::valueOf).collect(Collectors.joining(","));
            return this.classesIds;
        }
        return null;
    }
}
