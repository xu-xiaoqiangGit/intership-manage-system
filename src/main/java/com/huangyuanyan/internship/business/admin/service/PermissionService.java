package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.model.Permission;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface PermissionService extends IService<Permission> {

    /**
     * 查询拥有权限的角色
     *
     * @return 权限实体集合
     */
    List<Permission> listPermRoles();

    /**
     * 通过角色编码查询拥有的按钮权限数据
     *
     * @param roleCods 角色编码
     * @return 按钮权限数据
     */
    List<String> listBtnPermsByRoles(List<String> roleCods);

    /**
     * 新增权限数据
     *
     * @param permission 权限实体
     * @return 结果
     */
    boolean savePermission(Permission permission);

    /**
     * 修改权限数据
     *
     * @param permission 权限实体
     * @return 结果
     */
    boolean updatePermission(Permission permission);

    /**
     * 获取url权限集合
     *
     * @return 权限集合
     */
    Map<String, List<String>> selectUrlPermissions();
}
