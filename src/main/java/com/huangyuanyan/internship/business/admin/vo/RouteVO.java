package com.huangyuanyan.internship.business.admin.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-08-07 18:16
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
@ApiModel(description = "路由试图实体类，用于前端实现动态路由菜单加载")
public class RouteVO {

    @ApiModelProperty(value = "路径")
    private String path;

    @ApiModelProperty(value = "组件")
    private String component;

    @ApiModelProperty(value = "跳转路径")
    private String redirect;

    @ApiModelProperty(value = "是否是根菜单")
    private boolean alwaysShow;

    @ApiModelProperty(value = "路由名称，唯一")
    private String name;

    @ApiModelProperty(value = "是否隐藏")
    private Boolean hidden;

    @ApiModelProperty(value = "元属性")
    private Meta meta;

    @Data
    @AllArgsConstructor
    @ApiModel(description = "元属性实体")
    public static class Meta {
        @ApiModelProperty(value = "标题")
        private String title;

        @ApiModelProperty(value = "图标")
        private String icon;

        @ApiModelProperty(value = "进入路由的权限")
        private List<String> roles;
    }

    @ApiModelProperty(value = "子路由")
    private List<RouteVO> children;
}
