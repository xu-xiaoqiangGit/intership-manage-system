package com.huangyuanyan.internship.business.admin.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:41
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "菜单表")
@TableName("sys_menu")
public class Menu extends BaseEntity {

    @ApiModelProperty(value = "菜单名称")
    private String name;

    @ApiModelProperty(value = "父菜单ID")
    private Long parentId;

    @ApiModelProperty(value = "路由名称")
    private String routeName;

    @ApiModelProperty(value = "路由路径")
    private String routePath;

    @ApiModelProperty(value = "组件路径")
    private String component;

    @ApiModelProperty(value = "跳转路径")
    private String redirect;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态：0-禁用，1-开启")
    private Integer visible;

    @ApiModelProperty(value = "访问菜单的角色编码")
    @TableField(exist = false)
    private List<String> roles;

}