package com.huangyuanyan.internship.business.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.admin.mapper.RoleMenuMapper;
import com.huangyuanyan.internship.business.admin.model.RoleMenu;
import com.huangyuanyan.internship.business.admin.service.RoleMenuService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

    /**
     * 修改角色菜单
     *
     * @param id       角色ID
     * @param menusIds 菜单ID集合
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(Long id, List<Long> menusIds) {
        // 1.删除所有角色菜单
        this.baseMapper.delete(new LambdaQueryWrapper<RoleMenu>()
                .eq(RoleMenu::getRoleId, id)
        );
        // 2.重新添加角色菜单数据
        if (CollectionUtils.isNotEmpty(menusIds)) {
            List<RoleMenu> roleMenuList = new ArrayList<>();
            menusIds.forEach(menuId -> {
                RoleMenu roleMenu = new RoleMenu();
                roleMenu.setRoleId(id);
                roleMenu.setMenuId(menuId);
                roleMenuList.add(roleMenu);
            });
            this.saveBatch(roleMenuList);
        }
        return true;
    }

    /**
     * 通过角色ID查询角色拥有的菜单
     *
     * @param id 角色ID
     * @return 菜单ID集合
     */
    @Override
    public List<Long> listMenuIds(Long id) {
        List<RoleMenu> roleMenuList = this.baseMapper.selectList(new LambdaQueryWrapper<RoleMenu>()
                .select(RoleMenu::getMenuId)
                .eq(RoleMenu::getRoleId, id));
        return Optional.ofNullable(roleMenuList).orElse(new ArrayList<>())
                .stream()
                .map(RoleMenu::getMenuId)
                .collect(Collectors.toList());
    }
}
