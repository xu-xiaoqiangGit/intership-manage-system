package com.huangyuanyan.internship.business.admin.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-08-06 23:26
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MenuVO {

    private Long id;

    private String name;

    private Long parentId;

    private String routeName;

    private String routePath;

    private String component;

    private String redirect;

    private String icon;

    private Integer sort;

    private Integer visible;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<MenuVO> children;

}
