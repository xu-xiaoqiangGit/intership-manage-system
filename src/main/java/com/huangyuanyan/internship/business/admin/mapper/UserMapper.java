package com.huangyuanyan.internship.business.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.admin.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface UserMapper extends BaseMapper<User> {

    List<User> list(@Param("p") Page<User> page, @Param("user") User user);

    User getByUsername(String username);

    User getByUserId(Long id);
}
