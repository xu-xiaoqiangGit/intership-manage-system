package com.huangyuanyan.internship.business.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.admin.dto.RolePermissionDTO;
import com.huangyuanyan.internship.business.admin.mapper.RolePermissionMapper;
import com.huangyuanyan.internship.business.admin.model.Permission;
import com.huangyuanyan.internship.business.admin.model.RolePermission;
import com.huangyuanyan.internship.business.admin.service.PermissionService;
import com.huangyuanyan.internship.business.admin.service.RolePermissionService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
@RequiredArgsConstructor
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

    private final PermissionService permissionService;

    /**
     * 通过角色ID和菜单ID，查询角色拥有的权限
     *
     * @param id     角色ID
     * @param menuId 菜单ID
     * @return 权限ID集合
     */
    @Override
    public List<Long> listPermissionIds(Long id, Long menuId) {
        // 通过角色ID查询角色所拥有权限
        List<RolePermission> rolePermissionList = this.baseMapper.selectList(new LambdaQueryWrapper<RolePermission>()
                .select(RolePermission::getPermissionId)
                .eq(RolePermission::getRoleId, id));
        List<Long> permissionIds;
        // 过滤权限ID出来
        permissionIds = Optional.ofNullable(rolePermissionList).orElse(new ArrayList<>())
                .stream()
                .map(RolePermission::getPermissionId)
                .collect(Collectors.toList());
        // 如果有菜单ID；并且已通过角色ID查询到权限数据
        if (CollUtil.isNotEmpty(permissionIds) && ObjectUtil.isNotEmpty(menuId)) {
            // 通过菜单ID在已查询到的权限数据中二次过滤，查询到指定菜单和指定角色所拥有权限
            List<Permission> permissionList = permissionService.list(new LambdaQueryWrapper<Permission>()
                    .eq(Permission::getMenuId, menuId)
                    .in(Permission::getId, permissionIds)
            );
            // 取出ID
            permissionIds = Optional.ofNullable(permissionList).orElse(new ArrayList<>())
                    .stream()
                    .map(Permission::getId)
                    .collect(Collectors.toList());
        }
        return permissionIds;
    }

    /**
     * 修改角色权限
     *
     * @param rolePermissionDTO 角色权限数据传输对象
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(RolePermissionDTO rolePermissionDTO) {
        List<Long> delPermissionIds = listPermissionIds(rolePermissionDTO.getRoleId(), rolePermissionDTO.getMenuId());
        // 1.删除所有角色权限
        if (CollectionUtils.isNotEmpty(delPermissionIds)) {
            this.baseMapper.delete(new LambdaQueryWrapper<RolePermission>()
                    .in(RolePermission::getPermissionId, delPermissionIds)
            );
        }
        // 2.重新添加角色权限数据
        if (CollectionUtils.isNotEmpty(rolePermissionDTO.getPermissionIds())) {
            List<RolePermission> rolePermissionList = new ArrayList<>();
            rolePermissionDTO.getPermissionIds().forEach(permissionId -> {
                RolePermission rolePermission = new RolePermission();
                rolePermission.setRoleId(rolePermissionDTO.getRoleId());
                rolePermission.setPermissionId(permissionId);
                rolePermissionList.add(rolePermission);
            });
            this.saveBatch(rolePermissionList);
        }
        return true;
    }
}
