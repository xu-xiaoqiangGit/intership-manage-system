package com.huangyuanyan.internship.business.admin.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.huangyuanyan.internship.business.enterprise.model.Enterprise;
import com.huangyuanyan.internship.business.student.model.Student;
import com.huangyuanyan.internship.business.teacher.model.Teacher;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Objects;

/**
 * @Author: huangyuanyan
 * @Description: 登录用户数据
 * @Date Created in 2023-07-31 18:48
 */
@Data
// 不返回null值
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
@ApiModel("登录用户信息")
public class UserVO {

    @ApiModelProperty("用户主键id")
    private Long id;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("性别")
    private Integer gender;

    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("用户角色编码（多个）")
    private List<String> roles;

    @ApiModelProperty("用户权限")
    private List<String> perms;

    @ApiModelProperty("学生信息")
    private Student student;

    @ApiModelProperty("企业信息")
    private Enterprise enterprise;

    @ApiModelProperty("教师信息")
    private Teacher teacher;

    public Long getStudentId() {
        if (Objects.isNull(student)) {
            return null;
        }
        return student.getId();
    }

    public Long getEnterpriseId() {
        if (Objects.isNull(enterprise)) {
            return null;
        }
        return enterprise.getId();
    }

    public Long getTeacherId() {
        if (Objects.isNull(teacher)) {
            return null;
        }
        return teacher.getId();
    }
}
