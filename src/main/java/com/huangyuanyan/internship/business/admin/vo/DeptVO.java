package com.huangyuanyan.internship.business.admin.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-08-04 0:36
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class DeptVO {

    private Long id;

    private String name;

    private Long parentId;

    private String treePath;

    private Integer sort;

    private Integer status;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<DeptVO> children;

}
