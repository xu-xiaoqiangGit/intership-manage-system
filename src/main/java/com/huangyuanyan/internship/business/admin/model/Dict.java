package com.huangyuanyan.internship.business.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:37
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "字典表")
@TableName("sys_dict")
public class Dict extends BaseEntity {

    @ApiModelProperty(value = "类型名称")
    private String name;

    @ApiModelProperty(value = "类型编码")
    private String code;

    @ApiModelProperty(value = "状态：0正常,1停用")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remark;
}
