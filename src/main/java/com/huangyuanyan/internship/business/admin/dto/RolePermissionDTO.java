package com.huangyuanyan.internship.business.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "角色权限数据传输类")
public class RolePermissionDTO {

    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    @ApiModelProperty(value = "菜单ID")
    private Long menuId;

    @ApiModelProperty(value = "权限ID集合")
    private List<Long> permissionIds;

}
