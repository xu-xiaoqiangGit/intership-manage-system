package com.huangyuanyan.internship.business.admin.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:43
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "权限表")
@TableName("sys_permission")
public class Permission extends BaseEntity {

    @ApiModelProperty(value = "权限名称")
    private String name;

    @ApiModelProperty(value = "菜单模块ID")
    private Long menuId;

    @ApiModelProperty(value = "URL权限标识")
    private String urlPerm;

    @ApiModelProperty(value = "按钮权限标识")
    private String btnPerm;

    @ApiModelProperty(value = "有权限的角色编号集合")
    @TableField(exist = false)
    private List<String> roleCodes;
}
