package com.huangyuanyan.internship.business.admin.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.huangyuanyan.internship.business.admin.model.Dict;
import com.huangyuanyan.internship.business.admin.model.DictItem;
import com.huangyuanyan.internship.business.admin.service.DictItemService;
import com.huangyuanyan.internship.business.admin.service.DictService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.enums.QueryModeEnum;
import com.huangyuanyan.internship.common.exception.Asserts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author: huangyuanyan
 * @Description: 字典
 * @Date Created in 2023-08-07 16:14
 */
@Api(tags = "字典管理接口")
@RestController
@RequestMapping("/admin/dict")
@RequiredArgsConstructor
@Slf4j
public class DictController {

    private final DictService dictService;
    private final DictItemService dictItemService;

    @ApiOperation(value = "字典列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "queryMode", value = "查询模式", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "limit", value = "每页数量", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "name", value = "字典名称", paramType = "query", dataType = "String"),
    })
    @GetMapping("/list")
    public ResponseResult<?> list(
            String queryMode,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String name
    ) {
        // 1.获取查询模式
        QueryModeEnum queryModeEnum = QueryModeEnum.getByCode(queryMode);
        // 2.构建查询条件
        LambdaQueryWrapper<Dict> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(Dict::getUpdateTime);
        queryWrapper.orderByDesc(Dict::getCreateTime);
        queryWrapper.like(StrUtil.isNotBlank(name), Dict::getName, name);
        // 3.判断查询模式
        switch (queryModeEnum) {
            case PAGE:
                Page<Dict> result = dictService.page(new Page<>(page, limit), queryWrapper);
                return ResponseResult.success(result.getRecords(), result.getTotal());
            default:
                List<Dict> dictList = dictService.list(queryWrapper);
                return ResponseResult.success(dictList);
        }
    }

    @ApiOperation(value = "字典详情")
    @ApiImplicitParam(name = "id", value = "字典ID", paramType = "path", dataType = "Long", required = true)
    @GetMapping("/list/{id}")
    public ResponseResult<Dict> get(@PathVariable("id") Long id) {
        Dict dict = dictService.getById(id);
        return ResponseResult.success(dict);
    }

    @ApiOperation(value = "新增字典")
    @ApiImplicitParam(name = "dict", value = "字典实体JSON对象", paramType = "body", dataType = "Dict", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "id",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Dict dict) {
        boolean result = dictService.saveDict(dict);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改字典")
    @ApiImplicitParam(name = "dict", value = "字典实体JSON对象", paramType = "body", dataType = "Dict", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "status",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody Dict dict) {
        Asserts.failed(ObjectUtil.isEmpty(dict.getId()), ResultCode.PARAM_IS_NULL);
        Dict oldDict = dictService.getById(dict.getId());
        boolean result = dictService.updateDict(dict);
        if (result) {
            // 同步更新子字典项code
            if (!oldDict.getCode().equals(dict.getCode())) {
                dictItemService.update(new LambdaUpdateWrapper<DictItem>()
                        .set(DictItem::getDictCode, dict.getCode())
                        .eq(DictItem::getDictCode, oldDict.getCode())
                );
            }
        }
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "选择性更新", notes = "修改某些重要属性")
    @ApiImplicitParam(name = "dict", value = "字典实体JSON对象", paramType = "body", dataType = "Dict", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "createTime", "updateTime", "deleted", "revision"
    })
    @PatchMapping("/update")
    public ResponseResult<?> patch(@RequestBody Dict dict) {
        Asserts.failed(ObjectUtil.isEmpty(dict.getId()), ResultCode.PARAM_IS_NULL);
        LambdaUpdateWrapper<Dict> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(Dict::getId, dict.getId());
        updateWrapper.set(ObjectUtil.isNotEmpty(dict.getStatus()), Dict::getStatus, dict.getStatus());
        boolean result = dictService.update(updateWrapper);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除字典")
    @ApiImplicitParam(name = "ids", value = "字典ID数组", paramType = "body", dataType = "Long[]", required = true)
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        // 判断如果有字典项则无法删除
        List<String> codeList = Optional.ofNullable(dictService.listByIds(Arrays.asList(ids)))
                .orElse(null)
                .stream()
                .map(Dict::getCode)
                .collect(Collectors.toList());
        if (CollUtil.isNotEmpty(codeList)) {
            int count = dictItemService.count(new LambdaQueryWrapper<DictItem>()
                    .in(DictItem::getDictCode, codeList)
            );
            Asserts.failed(count > 0, "该字典有子字典项，无法删除！");
        }
        boolean result = dictService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }

}
