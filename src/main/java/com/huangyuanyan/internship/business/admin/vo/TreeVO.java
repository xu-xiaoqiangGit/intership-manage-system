package com.huangyuanyan.internship.business.admin.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-08-04 0:40
 */
@Data
public class TreeVO {

    private Long id;

    private String label;

    // 不返回null值
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeVO> children;

}
