package com.huangyuanyan.internship.business.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.admin.mapper.UserRoleMapper;
import com.huangyuanyan.internship.business.admin.model.UserRole;
import com.huangyuanyan.internship.business.admin.service.UserRoleService;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {
}
