package com.huangyuanyan.internship.business.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanyan.internship.business.admin.model.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    List<Permission> listPermRoles();

    List<String> listBtnPermsByRoles(@Param("roleCodes") List<String> roleCodes);

}
