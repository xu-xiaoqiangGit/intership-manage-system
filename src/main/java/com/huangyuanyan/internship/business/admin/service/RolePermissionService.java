package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.dto.RolePermissionDTO;
import com.huangyuanyan.internship.business.admin.model.RolePermission;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface RolePermissionService extends IService<RolePermission> {

    /**
     * 通过角色ID和菜单ID，查询角色拥有的权限
     *
     * @param id     角色ID
     * @param menuId 菜单ID
     * @return 权限ID集合
     */
    List<Long> listPermissionIds(Long id, Long menuId);

    /**
     * 修改角色权限
     *
     * @param rolePermissionDTO 角色权限数据传输对象
     * @return 结果
     */
    boolean update(RolePermissionDTO rolePermissionDTO);
}
