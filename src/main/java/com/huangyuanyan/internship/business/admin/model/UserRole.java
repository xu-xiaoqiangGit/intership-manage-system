package com.huangyuanyan.internship.business.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:50
 */
@Data
@Accessors(chain = true)
@TableName("sys_user_role")
public class UserRole {

    private Long userId;

    private Long roleId;
}