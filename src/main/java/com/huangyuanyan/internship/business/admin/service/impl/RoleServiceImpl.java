package com.huangyuanyan.internship.business.admin.service.impl;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.admin.mapper.RoleMapper;
import com.huangyuanyan.internship.business.admin.model.Role;
import com.huangyuanyan.internship.business.admin.model.RoleMenu;
import com.huangyuanyan.internship.business.admin.model.RolePermission;
import com.huangyuanyan.internship.business.admin.model.UserRole;
import com.huangyuanyan.internship.business.admin.service.RoleMenuService;
import com.huangyuanyan.internship.business.admin.service.RolePermissionService;
import com.huangyuanyan.internship.business.admin.service.RoleService;
import com.huangyuanyan.internship.business.admin.service.UserRoleService;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
@RequiredArgsConstructor
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    private final RoleMenuService roleMenuService;

    private final RolePermissionService rolePermissionService;

    private final UserRoleService userRoleService;

    /**
     * 删除角色列表
     *
     * @param ids 角色ID集合
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(List<Long> ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        AtomicBoolean result = new AtomicBoolean(false);
        // 1.判断该角色是否有用户，有用户则无法删除
        ids.forEach(id -> {
            Asserts.failed(ObjectUtil.isEmpty(id), ResultCode.PARAM_ERROR);
            int count = userRoleService.count(new LambdaQueryWrapper<UserRole>()
                    .eq(UserRole::getRoleId, id)
            );
            Asserts.failed(count > 0, "该角色已分配用户，无法删除！");
            // 2.删除角色菜单数据
            roleMenuService.remove(new LambdaQueryWrapper<RoleMenu>().eq(RoleMenu::getRoleId, id));
            // 3.删除角色权限数据
            rolePermissionService.remove(new LambdaQueryWrapper<RolePermission>().eq(RolePermission::getRoleId, id));
            // 4.删除角色数据
            result.set(this.removeByIds(ids));
        });
        return result.get();
    }

    /**
     * 新增角色
     *
     * @param role 角色实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveRole(Role role) {
        // 判定角色是否重复
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<Role>()
                .eq(Role::getName, role.getName())
                .or()
                .eq(Role::getCode, role.getCode())
        );
        Asserts.failed(count > 0, "此角色名称或编码重复，请检查！");
        return this.baseMapper.insert(role) > 0;
    }

    /**
     * 修改角色
     *
     * @param role 角色实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateRole(Role role) {
        Asserts.failed(ObjectUtil.isEmpty(role.getId()), ResultCode.PARAM_IS_NULL);
        // 判定角色是否重复
        int count = this.baseMapper.selectCount((new LambdaQueryWrapper<Role>()
                .ne(Role::getId, role.getId())
                .and(updateWrapper -> updateWrapper
                        .eq(Role::getName, role.getName())
                        .or()
                        .eq(Role::getCode, role.getCode()))
        ));
        Asserts.failed(count > 0, "此角色名称或编码重复，请检查！");
        return this.baseMapper.updateById(role) > 0;
    }
}
