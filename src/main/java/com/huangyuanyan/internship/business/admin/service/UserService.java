package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.model.User;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface UserService extends IService<User> {

    /**
     * 分页用户列表
     *
     * @param page 分页类
     * @param user 用户实体类
     * @return 分页类
     */
    Page<User> list(Page<User> page, User user);

    /**
     * 保存用户
     *
     * @param user 用户实体类
     * @return 结果
     */
    boolean saveUser(User user);

    /**
     * 修改用户
     *
     * @param user 用户实体类
     * @return 结果
     */
    boolean updateUser(User user);

    /**
     * 通过用户名获取用户信息
     *
     * @param username 用户名
     * @return 用户实体类
     */
    User getByUsername(String username);

    /**
     * 通过id获取用户信息
     *
     * @param id id
     * @return 用户实体类
     */
    User getByUserId(Long id);

    /**
     * 更改用户角色
     *
     * @param user 用户实体
     * @return 结果
     */
    boolean updateRole(User user);

    /**
     * 绑定学生信息
     *
     * @param user 用户
     * @return 结果
     */
    boolean biddingStudent(User user);

    /**
     * 绑定企业信息
     *
     * @param user 用户
     * @return 结果
     */
    boolean biddingEnterprise(User user);

    /**
     * 根据学生编号查询
     *
     * @param studentCode 用户
     * @return 用户
     */
    User getByStudentCode(String studentCode);

    /**
     * 绑定教师信息
     *
     * @param user 用户
     * @return 结果
     */
    boolean biddingTeacher(User user);
}
