package com.huangyuanyan.internship.business.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanyan.internship.business.admin.model.Role;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface RoleMapper extends BaseMapper<Role> {
}
