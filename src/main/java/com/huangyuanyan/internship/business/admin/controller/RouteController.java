package com.huangyuanyan.internship.business.admin.controller;

import com.huangyuanyan.internship.business.admin.service.MenuService;
import com.huangyuanyan.internship.business.admin.vo.RouteVO;
import com.huangyuanyan.internship.common.api.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-09 0:44
 */
@Api(tags = "路由接口")
@RestController
@RequestMapping("/admin/route")
@RequiredArgsConstructor
public class RouteController {

    private final MenuService menuService;

    @ApiOperation(value = "路由列表")
    @GetMapping
    public ResponseResult<List<RouteVO>> list() {
        List<RouteVO> routeVOList = menuService.listRoute();
        return ResponseResult.success(routeVOList);
    }
}
