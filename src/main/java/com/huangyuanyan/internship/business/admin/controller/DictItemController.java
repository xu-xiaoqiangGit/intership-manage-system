package com.huangyuanyan.internship.business.admin.controller;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.huangyuanyan.internship.business.admin.model.DictItem;
import com.huangyuanyan.internship.business.admin.service.DictItemService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.enums.QueryModeEnum;
import com.huangyuanyan.internship.common.exception.Asserts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-08-07 16:45
 */
@Api(tags = "字典项管理接口")
@RestController
@RequestMapping("/admin/dict-item")
@RequiredArgsConstructor
@Slf4j
public class DictItemController {

    private final DictItemService dictItemService;

    @ApiOperation(value = "字典项列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "queryMode", value = "查询模式", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "limit", value = "每页数量", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "name", value = "字典项名称", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "dictCode", value = "字典编码", paramType = "query", dataType = "String"),
    })
    @GetMapping("/list")
    public ResponseResult<?> list(
            String queryMode,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String name,
            String dictCode
    ) {
        // 1.获取查询模式
        QueryModeEnum queryModeEnum = QueryModeEnum.getByCode(queryMode);
        // 2.构建查询条件
        LambdaQueryWrapper<DictItem> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(DictItem::getSort);
        queryWrapper.orderByDesc(DictItem::getUpdateTime);
        queryWrapper.orderByDesc(DictItem::getCreateTime);
        queryWrapper.like(StrUtil.isNotBlank(name), DictItem::getName, name);
        queryWrapper.eq(StrUtil.isNotBlank(dictCode), DictItem::getDictCode, dictCode);
        // 3.判断查询模式
        switch (queryModeEnum) {
            case PAGE:
                Page<DictItem> result = dictItemService.page(new Page<>(page, limit), queryWrapper);
                return ResponseResult.success(result.getRecords(), result.getTotal());
            default:
                List<DictItem> dictList = dictItemService.list(queryWrapper);
                return ResponseResult.success(dictList);
        }
    }

    @ApiOperation(value = "字典项详情")
    @ApiImplicitParam(name = "id", value = "字典项ID", paramType = "path", dataType = "Long", required = true)
    @GetMapping("/list/{id}")
    public ResponseResult<DictItem> get(@PathVariable("id") Long id) {
        DictItem dictItem = dictItemService.getById(id);
        return ResponseResult.success(dictItem);
    }

    @ApiOperation(value = "新增字典项")
    @ApiImplicitParam(name = "dictItem", value = "字典项实体JSON对象", paramType = "body", dataType = "DictItem", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "id",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody DictItem dictItem) {
        boolean result = dictItemService.saveDictItem(dictItem);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改字典项")
    @ApiImplicitParam(name = "dictItem", value = "字典项实体JSON对象", paramType = "body", dataType = "DictItem", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "status",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody DictItem dictItem) {
        boolean result = dictItemService.updateDictItem(dictItem);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "选择性更新", notes = "修改某些重要属性")
    @ApiImplicitParam(name = "dictItem", value = "字典项实体JSON对象", paramType = "body", dataType = "DictItem", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "createTime", "updateTime", "deleted", "revision"
    })
    @PatchMapping("/update")
    public ResponseResult<?> patch(@RequestBody DictItem dictItem) {
        Asserts.failed(ObjectUtil.isEmpty(dictItem.getId()), ResultCode.PARAM_IS_NULL);
        LambdaUpdateWrapper<DictItem> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(DictItem::getId, dictItem.getId());
        updateWrapper.set(ObjectUtil.isNotEmpty(dictItem.getStatus()), DictItem::getStatus, dictItem.getStatus());
        boolean result = dictItemService.update(updateWrapper);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除字典项")
    @ApiImplicitParam(name = "ids", value = "字典项ID数组", paramType = "body", dataType = "Long[]", required = true)
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = dictItemService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }

}
