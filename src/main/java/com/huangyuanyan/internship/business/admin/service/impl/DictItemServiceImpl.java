package com.huangyuanyan.internship.business.admin.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.admin.mapper.DictItemMapper;
import com.huangyuanyan.internship.business.admin.model.DictItem;
import com.huangyuanyan.internship.business.admin.service.DictItemService;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
public class DictItemServiceImpl extends ServiceImpl<DictItemMapper, DictItem> implements DictItemService {

    /**
     * 新增字典项
     *
     * @param dictItem 字典项实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveDictItem(DictItem dictItem) {
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<DictItem>()
                .eq(DictItem::getName, dictItem.getName())
                .or()
                .eq(DictItem::getValue, dictItem.getValue())
        );
        Asserts.failed(count > 0, "字典项名称或值重复，请检查！");
        return this.baseMapper.insert(dictItem) > 0;
    }

    /**
     * 修改字典项
     *
     * @param dictItem 字典项实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateDictItem(DictItem dictItem) {
        Asserts.failed(ObjectUtil.isEmpty(dictItem.getId()), ResultCode.PARAM_IS_NULL);
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<DictItem>()
                .ne(DictItem::getId, dictItem.getId())
                .and(queryWrapper -> queryWrapper
                        .eq(DictItem::getName, dictItem.getName())
                        .or()
                        .eq(DictItem::getValue, dictItem.getValue()))
        );
        Asserts.failed(count > 0, "字典项名称或值重复，请检查！");
        return this.baseMapper.updateById(dictItem) > 0;
    }

}
