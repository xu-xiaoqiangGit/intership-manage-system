package com.huangyuanyan.internship.business.admin.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.huangyuanyan.internship.business.admin.dto.RolePermissionDTO;
import com.huangyuanyan.internship.business.admin.model.Role;
import com.huangyuanyan.internship.business.admin.service.RoleMenuService;
import com.huangyuanyan.internship.business.admin.service.RolePermissionService;
import com.huangyuanyan.internship.business.admin.service.RoleService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.enums.QueryModeEnum;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-08-03 18:27
 */
@Api(tags = "角色管理接口")
@RestController
@RequestMapping("/admin/role")
@Slf4j
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;
    private final RoleMenuService roleMenuService;
    private final RolePermissionService rolePermissionService;

    @ApiOperation(value = "角色列表分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "queryMode", value = "查询模式", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "limit", value = "每页数量", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "name", value = "角色名称", paramType = "query", dataType = "String"),
    })
    @GetMapping("/list")
    public ResponseResult<List<Role>> list(
            String queryMode,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String name
    ) {
        // 1.获取查询模式
        QueryModeEnum queryModeEnum = QueryModeEnum.getByCode(queryMode);
        // 2.获取当前用户角色
        List<String> roles = StpUtil.getRoleList();
        // 3.判断是否超级管理员
        boolean isRoot = false;
        if (CollUtil.isNotEmpty(roles)) {
            isRoot = roles.contains(GlobalConstant.ROOT_ROLE_CODE);
        }
        // 4.构建查询条件
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<Role>();
        queryWrapper.like(StrUtil.isNotBlank(name), Role::getName, name);
        // 如果不是超级管理员，则不能查看超级管理员角色
        queryWrapper.ne(!isRoot, Role::getCode, GlobalConstant.ROOT_ROLE_CODE);
        queryWrapper.orderByAsc(Role::getSort);
        queryWrapper.orderByDesc(Role::getUpdateTime);
        queryWrapper.orderByDesc(Role::getCreateTime);
        // 5.判断查询模式
        switch (queryModeEnum) {
            case PAGE:
                Page<Role> result = roleService.page(new Page<>(page, limit), queryWrapper);
                return ResponseResult.success(result.getRecords(), result.getTotal());
            case LIST:
                List<Role> roleList = roleService.list(queryWrapper);
                return ResponseResult.success(roleList);
            default:
                return ResponseResult.failed(ResultCode.QUERY_MODE_IS_NULL);
        }
    }

    @ApiOperation(value = "角色详情")
    @ApiImplicitParam(name = "id", value = "角色ID", paramType = "path", dataType = "Long", required = true)
    @GetMapping("/list/{id}")
    public ResponseResult<Role> get(@PathVariable("id") Long id) {
        Role role = roleService.getById(id);
        return ResponseResult.success(role);
    }

    @ApiOperation(value = "新增角色")
    @ApiImplicitParam(name = "role", value = "角色实体JSON对象", paramType = "body", dataType = "Role", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "id", "status",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Role role) {
        boolean result = roleService.saveRole(role);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改角色")
    @ApiImplicitParam(name = "role", value = "角色实体JSON对象", paramType = "body", dataType = "Role", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "status",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody Role role) {
        boolean result = roleService.updateRole(role);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "选择性更新", notes = "修改某些重要属性")
    @ApiImplicitParam(name = "role", value = "角色实体JSON对象", paramType = "body", dataType = "Role", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "createTime", "updateTime", "deleted", "revision"
    })
    @PatchMapping("/update")
    public ResponseResult<?> patch(@RequestBody Role role) {
        Asserts.failed(ObjectUtil.isEmpty(role.getId()), ResultCode.PARAM_IS_NULL);
        LambdaUpdateWrapper<Role> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(Role::getId, role.getId());
        updateWrapper.set(ObjectUtil.isNotEmpty(role.getStatus()), Role::getStatus, role.getStatus());
        boolean result = roleService.update(updateWrapper);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除角色")
    @ApiImplicitParam(name = "ids", value = "角色ID数组", paramType = "body", dataType = "Long[]", required = true)
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Role dbRole = roleService.getById(ids[0]);
        if (Objects.isNull(dbRole)) {
            throw new BusinessException("角色信息已被删除，无需重复操作");
        }
        dbRole.setName(IdUtil.simpleUUID());
        roleService.updateById(dbRole);

        boolean result = roleService.delete(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "角色拥有的菜单合集")
    @ApiImplicitParam(name = "id", value = "角色ID", paramType = "path", dataType = "Long", required = true)
    @GetMapping("/menus/{id}")
    public ResponseResult<List<Long>> getMenus(@PathVariable("id") Long id) {
        List<Long> menuIds = roleMenuService.listMenuIds(id);
        return ResponseResult.success(menuIds);
    }

    @ApiOperation(value = "角色拥有的权限合集")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", paramType = "path", dataType = "Long", required = true),
            @ApiImplicitParam(name = "menuId", value = "菜单ID", paramType = "query", dataType = "Long")
    })
    @GetMapping("/permissions/{id}")
    public ResponseResult<List<Long>> getPermissions(@PathVariable("id") Long id, Long menuId) {
        List<Long> permissionIds = rolePermissionService.listPermissionIds(id, menuId);
        return ResponseResult.success(permissionIds);
    }

    @ApiOperation(value = "修改角色菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", paramType = "path", dataType = "Long", required = true),
            @ApiImplicitParam(name = "role", value = "角色实体JSON对象", paramType = "body", dataType = "Role", required = true)
    })
    @ApiOperationSupport(ignoreParameters = {
            "role.id", "name", "code", "sort", "status", "permissionIds",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PutMapping("/menus/{id}")
    public ResponseResult<?> updateMenus(
            @PathVariable("id") Long id,
            @RequestBody Role role
    ) {
        List<Long> menusIds = role.getMenuIds();
        boolean result = roleMenuService.update(id, menusIds);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改角色权限")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", paramType = "path", dataType = "Long", required = true),
            @ApiImplicitParam(name = "rolePermissionDTO", value = "角色权限实体JSON对象", paramType = "body", dataType = "RolePermissionDTO", required = true)
    })
    @PutMapping("/permissions/{id}")
    public ResponseResult<?> updatePermissions(
            @PathVariable("id") Long id,
            @RequestBody RolePermissionDTO rolePermissionDTO
    ) {
        rolePermissionDTO.setRoleId(id);
        boolean result = rolePermissionService.update(rolePermissionDTO);
        return ResponseResult.judge(result);
    }

}
