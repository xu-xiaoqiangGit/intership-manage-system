package com.huangyuanyan.internship.business.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-09-20 21:48
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "控制台数据视图类")
public class DashBoardVO {

    @ApiModelProperty(value = "会员总数")
    private Integer memberTotal;

    @ApiModelProperty(value = "商品总数")
    private Integer productTotal;

    @ApiModelProperty(value = "销售额")
    private Double sales;

    @ApiModelProperty(value = "订单总数")
    private Integer orderTotal;

    @ApiModelProperty(value = "七日成交数据")
    private List<DashBoardItem> sevenDayData;

}
