package com.huangyuanyan.internship.business.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.admin.mapper.DictMapper;
import com.huangyuanyan.internship.business.admin.model.Dict;
import com.huangyuanyan.internship.business.admin.service.DictService;
import com.huangyuanyan.internship.common.exception.Asserts;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {

    /**
     * 新增字典
     *
     * @param dict 字典实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveDict(Dict dict) {
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<Dict>()
                .eq(Dict::getName, dict.getName())
                .or()
                .eq(Dict::getCode, dict.getCode())
        );
        Asserts.failed(count > 0, "字典名称或编码已重复，请检查！");
        return this.baseMapper.insert(dict) > 0;
    }

    /**
     * 修改字典
     *
     * @param dict 字典实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateDict(Dict dict) {
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<Dict>()
                .ne(Dict::getId, dict.getId())
                .and(queryWrapper -> queryWrapper
                        .eq(Dict::getName, dict.getName())
                        .or()
                        .eq(Dict::getCode, dict.getCode()))
        );
        Asserts.failed(count > 0, "字典名称或编码已重复，请检查！");
        return this.baseMapper.updateById(dict) > 0;
    }
}
