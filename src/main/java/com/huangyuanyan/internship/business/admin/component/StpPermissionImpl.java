package com.huangyuanyan.internship.business.admin.component;

import cn.dev33.satoken.stp.StpInterface;
import com.huangyuanyan.internship.business.admin.model.User;
import com.huangyuanyan.internship.business.admin.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 权限数据加载类
 * @Author huangyuanyan
 * @Date Created in 2023-11-09 0:29
 */
@Component
@AllArgsConstructor
public class StpPermissionImpl implements StpInterface {

    private final UserService userService;

    /**
     * 获取用户权限（不需要）
     *
     * @param userId    账号id
     * @param loginType 账号类型
     * @return
     */
    @Override
    public List<String> getPermissionList(Object userId, String loginType) {
        return new ArrayList<>();
    }

    /**
     * 获取用户角色
     *
     * @param userId    账号id
     * @param loginType 账号类型
     * @return 用户角色编码
     */
    @Override
    public List<String> getRoleList(Object userId, String loginType) {
        User user = userService.getByUserId(Long.valueOf(userId.toString()));
        return user.getRoleCodes();
    }
}
