package com.huangyuanyan.internship.business.admin.controller;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.huangyuanyan.internship.business.admin.model.Menu;
import com.huangyuanyan.internship.business.admin.service.MenuService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.enums.QueryModeEnum;
import com.huangyuanyan.internship.common.exception.Asserts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-08-06 22:56
 */
@Api(tags = "菜单管理接口")
@RestController
@RequestMapping("/admin/menu")
@RequiredArgsConstructor
@Slf4j
public class MenuController {

    private final MenuService menuService;

    @ApiOperation(value = "菜单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "queryMode", value = "查询模式", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "name", value = "菜单名称", paramType = "query", dataType = "String")
    })
    @GetMapping("/list")
    public ResponseResult<List> list(
            String queryMode,
            String name
    ) {
        // 1.获取查询模式
        QueryModeEnum queryModeEnum = QueryModeEnum.getByCode(queryMode);
        // 2.构建查询条件
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(Menu::getSort);
        queryWrapper.orderByDesc(Menu::getUpdateTime);
        queryWrapper.orderByDesc(Menu::getCreateTime);
        // 3.判断查询模式
        List list;
        switch (queryModeEnum) {
            case LIST:
                queryWrapper.like(StrUtil.isNotBlank(name), Menu::getName, name);
                list = menuService.listMenuVO(queryWrapper);
                break;
            case TREE:
                list = menuService.listTreeVO(queryWrapper);
                break;
            default:
                list = menuService.list(queryWrapper);
        }
        return ResponseResult.success(list);
    }

    @ApiOperation(value = "菜单详情")
    @ApiImplicitParam(name = "id", value = "菜单ID", paramType = "path", dataType = "Long", required = true)
    @GetMapping("/list/{id}")
    public ResponseResult<Menu> get(@PathVariable("id") Long id) {
        Menu menu = menuService.getById(id);
        return ResponseResult.success(menu);
    }

    @ApiOperation(value = "新增菜单")
    @ApiImplicitParam(name = "menu", value = "菜单实体JSON对象", paramType = "body", dataType = "Menu", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "id",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Menu menu) {
        boolean result = menuService.save(menu);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改菜单")
    @ApiImplicitParam(name = "menu", value = "菜单实体JSON对象", paramType = "body", dataType = "Menu", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "visible",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody Menu menu) {
        Asserts.failed(ObjectUtil.isEmpty(menu.getId()), ResultCode.PARAM_IS_NULL);
        boolean result = menuService.updateById(menu);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "选择性更新", notes = "修改某些重要属性")
    @ApiImplicitParam(name = "menu", value = "菜单实体JSON对象", paramType = "body", dataType = "Menu", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "createTime", "updateTime", "deleted", "revision"
    })
    @PatchMapping("/update")
    public ResponseResult<?> patch(@RequestBody Menu menu) {
        Asserts.failed(ObjectUtil.isEmpty(menu.getId()), ResultCode.PARAM_IS_NULL);
        LambdaUpdateWrapper<Menu> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(Menu::getId, menu.getId());
        updateWrapper.set(ObjectUtil.isNotEmpty(menu.getVisible()), Menu::getVisible, menu.getVisible());
        boolean result = menuService.update(updateWrapper);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除菜单")
    @ApiImplicitParam(name = "ids", value = "菜单ID数组", paramType = "body", dataType = "Long[]", required = true)
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        AtomicBoolean result = new AtomicBoolean(false);
        Arrays.asList(ids).forEach(id -> {
            Asserts.failed(ObjectUtil.isEmpty(id), ResultCode.PARAM_ERROR);
            // 如果有子菜单，则无法删除
            int count = menuService.count(new LambdaQueryWrapper<Menu>()
                    .eq(Menu::getParentId, id)
            );
            Asserts.failed(count > 0, "此菜单下存在子菜单，无法删除！");
            result.set(menuService.removeById(id));
            Asserts.failed(!result.get(), ResultCode.FAILED);
        });
        return ResponseResult.judge(result.get());
    }

}
