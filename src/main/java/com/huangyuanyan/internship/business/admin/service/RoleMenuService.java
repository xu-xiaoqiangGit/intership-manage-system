package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.model.RoleMenu;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface RoleMenuService extends IService<RoleMenu> {

    /**
     * 修改角色菜单
     *
     * @param id       角色ID
     * @param menusIds 菜单ID集合
     * @return 结果
     */
    boolean update(Long id, List<Long> menusIds);

    /**
     * 通过角色ID查询角色拥有的菜单
     *
     * @param id 角色ID
     * @return 菜单ID集合
     */
    List<Long> listMenuIds(Long id);
}
