package com.huangyuanyan.internship.business.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "字典数据表")
@TableName("sys_dict_item")
public class DictItem extends BaseEntity {

    @ApiModelProperty(value = "字典项名称")
    private String name;

    @ApiModelProperty(value = "字典项值")
    private String value;

    @ApiModelProperty(value = "字典编码")
    private String dictCode;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态:0-正常,1-停用")
    private Integer status;

    @ApiModelProperty(value = "是否默认:0-否,1-是")
    private Integer defaulted;

    @ApiModelProperty(value = "备注")
    private String remark;

}