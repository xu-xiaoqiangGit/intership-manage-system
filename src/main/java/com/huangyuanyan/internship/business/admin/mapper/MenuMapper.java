package com.huangyuanyan.internship.business.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanyan.internship.business.admin.model.Menu;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> listRoute();
}
