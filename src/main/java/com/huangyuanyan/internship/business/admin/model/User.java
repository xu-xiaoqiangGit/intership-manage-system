package com.huangyuanyan.internship.business.admin.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "用户表实体类")
@TableName("sys_user")
public class User extends BaseEntity {

    @ApiModelProperty(value = "用户名")
    @JsonProperty("username")
    private String userName;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "密码")
    private String password;

    private String studentCode;

    private Long enterpriseId;

    private Long teacherId;

    /**
     * 学生姓名
     */
    @TableField(exist = false)
    private String studentName;

    /**
     * 企业名称
     */
    @TableField(exist = false)
    private String enterpriseName;

    /**
     * 教师工号
     */
    @TableField(exist = false)
    private String teacherCode;

    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "用户状态:0-正常,1-禁用")
    private Integer status;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "角色ID集合")
    @TableField(exist = false)
    private List<Long> roleIds;

    @ApiModelProperty(value = "角色编码集合")
    @TableField(exist = false)
    private List<String> roleCodes;

    @ApiModelProperty(value = "角色名称")
    @TableField(exist = false)
    private String roleNames;

    @ApiModelProperty(value = "是否是ROOT角色", hidden = true)
    @TableField(exist = false)
    private Boolean isRoot;

    /**
     * 旧密码
     */
    @TableField(exist = false)
    private String oldPassword;
}

