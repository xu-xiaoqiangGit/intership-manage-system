package com.huangyuanyan.internship.business.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:48
 */
@Data
@Accessors(chain = true)
@TableName("sys_role_permission")
public class RolePermission {

    private Long roleId;

    private Long permissionId;
}
