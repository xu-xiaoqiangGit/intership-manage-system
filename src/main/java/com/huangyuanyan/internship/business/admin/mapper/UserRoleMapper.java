package com.huangyuanyan.internship.business.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanyan.internship.business.admin.model.UserRole;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {
}
