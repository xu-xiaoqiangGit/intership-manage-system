package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.model.DictItem;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface DictItemService extends IService<DictItem> {

    /**
     * 新增字典项
     *
     * @param dictItem 字典项实体
     * @return 结果
     */
    boolean saveDictItem(DictItem dictItem);

    /**
     * 修改字典项
     *
     * @param dictItem 字典项实体
     * @return 结果
     */
    boolean updateDictItem(DictItem dictItem);
}
