package com.huangyuanyan.internship.business.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.xiaoymin.knife4j.core.util.StrUtil;
import com.google.common.collect.Maps;
import com.huangyuanyan.internship.business.admin.mapper.PermissionMapper;
import com.huangyuanyan.internship.business.admin.model.Permission;
import com.huangyuanyan.internship.business.admin.service.PermissionService;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    /**
     * 查询用于权限的角色
     *
     * @return 权限实体集合
     */
    @Override
    public List<Permission> listPermRoles() {
        return this.baseMapper.listPermRoles();
    }

    /**
     * 通过角色编码查询拥有的按钮权限数据
     *
     * @param roleCods 角色编码
     * @return 按钮权限数据
     */
    @Override
    public List<String> listBtnPermsByRoles(List<String> roleCods) {
        return this.baseMapper.listBtnPermsByRoles(roleCods);
    }

    /**
     * 新增权限数据
     *
     * @param permission 权限实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean savePermission(Permission permission) {
        // 判断url权限或按钮权限是否重复
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<Permission>()
                .eq(StrUtil.isNotBlank(permission.getBtnPerm()), Permission::getBtnPerm, permission.getBtnPerm())
        );
        Asserts.failed(count > 0, "按钮权限已重复，请检查！");
        return this.baseMapper.insert(permission) > 0;
    }

    /**
     * 修改权限数据
     *
     * @param permission 权限实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updatePermission(Permission permission) {
        Asserts.failed(Objects.isNull(permission.getId()), ResultCode.PARAM_IS_NULL);
        // 判断url权限或按钮权限是否重复
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<Permission>()
                .ne(Permission::getId, permission.getId())
                .and(queryWrapper -> queryWrapper.eq
                        (StrUtil.isNotBlank(permission.getBtnPerm()),
                                Permission::getBtnPerm, permission.getBtnPerm()
                        )
                )
        );
        Asserts.failed(count > 0, "按钮权限已重复，请检查！");
        return this.baseMapper.updateById(permission) > 0;
    }

    @Override
    public Map<String, List<String>> selectUrlPermissions() {
        List<Permission> permRoles = listPermRoles();
        if (CollUtil.isEmpty(permRoles)) {
            return Maps.newHashMap();
        }

        // 根据角色编码分组
        Map<String, List<List<String>>> collect = permRoles.stream()
                .filter(item -> StrUtil.isNotBlank(item.getUrlPerm()))
                .collect(Collectors.groupingBy(
                        Permission::getUrlPerm,
                        Collectors.mapping(Permission::getRoleCodes, Collectors.toList())
                ));

        // 权限格式【key:urlPerm --> value:roleCodes】
        Map<String, List<String>> permissions = Maps.newHashMap();
        collect.forEach((k, v) -> {
            permissions.put(k, v.stream().flatMap(Collection::stream).distinct().collect(Collectors.toList()));
        });

        return permissions;
    }
}
