package com.huangyuanyan.internship.business.admin.service.impl;

import cn.dev33.satoken.secure.SaSecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.xiaoymin.knife4j.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.huangyuanyan.internship.business.admin.mapper.UserMapper;
import com.huangyuanyan.internship.business.admin.model.User;
import com.huangyuanyan.internship.business.admin.model.UserRole;
import com.huangyuanyan.internship.business.admin.service.UserRoleService;
import com.huangyuanyan.internship.business.admin.service.UserService;
import com.huangyuanyan.internship.business.enterprise.model.Enterprise;
import com.huangyuanyan.internship.business.enterprise.service.EnterpriseService;
import com.huangyuanyan.internship.business.student.model.Student;
import com.huangyuanyan.internship.business.student.service.StudentService;
import com.huangyuanyan.internship.business.teacher.model.Teacher;
import com.huangyuanyan.internship.business.teacher.service.TeacherService;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.constant.AuthConstant;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BaseException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private final UserRoleService userRoleService;
    private final StudentService studentService;
    private final EnterpriseService enterpriseService;
    private final TeacherService teacherService;

    /**
     * 分页用户列表
     *
     * @param page 分页类
     * @param user 用户实体类
     * @return 分页类
     */
    @Override
    public Page<User> list(Page<User> page, User user) {
        List<User> userList = this.baseMapper.list(page, user);
        page.setRecords(userList);
        return page;
    }

    /**
     * 保存用户
     *
     * @param user 用户实体类
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveUser(User user) {
        // 查询用户名是否重复
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<User>()
                .eq(User::getUserName, user.getUserName())
        );
        Asserts.failed(count > 0, "账户[" + user.getUserName() + "]已经存在，请核对");
        // BCrypt加密
        if (StrUtil.isNotBlank(user.getPassword())) {
            user.setPassword(SaSecureUtil.aesEncrypt(AuthConstant.ENCRYPT_KEY, user.getPassword()));
        }
        return this.save(user) && updateRole(user);
    }

    /**
     * 修改用户
     *
     * @param user 用户实体类
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateUser(User user) {
        Asserts.failed(Objects.isNull(user.getId()), ResultCode.PARAM_IS_NULL);
        // 查询用户名是否重复
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<User>()
                .eq(User::getUserName, user.getUserName())
                .ne(User::getId, user.getId())
        );
        Asserts.failed(count > 0, "此用户名重复，请检查!");
        // 3.修改用户信息
        return this.updateById(user) && updateRole(user);
    }

    /**
     * 通过用户名获取用户信息
     *
     * @param username 用户名
     * @return 用户实体类
     */
    @Override
    public User getByUsername(String username) {
        return this.baseMapper.getByUsername(username);
    }

    @Override
    public User getByUserId(Long id) {
        return this.baseMapper.getByUserId(id);
    }

    /**
     * 更改用户角色
     *
     * @param user 用户实体
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateRole(User user) {
        if (CollectionUtils.isEmpty(user.getRoleIds())) {
            return true;
        }
        Asserts.failed(Objects.isNull(user.getId()), ResultCode.PARAM_IS_NULL);
        // 1.删除用户所有的角色数据
        LambdaQueryWrapper<UserRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserRole::getUserId, user.getId());
        userRoleService.remove(queryWrapper);
        // 2.添加用户角色
        List<UserRole> userRoleList = Lists.newArrayList();
        user.getRoleIds().forEach(roleId -> userRoleList.add(new UserRole().setUserId(user.getId()).setRoleId(roleId)));
        return userRoleService.saveBatch(userRoleList);
    }

    @Override
    public boolean biddingStudent(User user) {
        User byStudentCode = getByStudentCode(user.getStudentCode());
        if (Objects.nonNull(byStudentCode)) {
            throw new BaseException("学生编号[" + user.getStudentCode() + "]已被用户绑定，不可再次绑定");
        }
        Student student = studentService.getByCode(user.getStudentCode());
        if (Objects.isNull(student)) {
            throw new BaseException("学生编号[" + user.getStudentCode() + "]对应的学生信息不存在，请检查");
        }
        User forUpdate = new User();
        forUpdate.setId(SessionUtils.getCurrentUser().getId());
        forUpdate.setStudentCode(student.getStudentCode());

        return this.updateById(forUpdate);
    }

    @Override
    public boolean biddingEnterprise(User user) {
        Enterprise enterprise = enterpriseService.getByEnterpriseName(user.getEnterpriseName());
        if (Objects.isNull(enterprise)) {
            throw new BaseException("企业[" + user.getEnterpriseName() + "]信息不存在，请检查");
        }
        User forUpdate = new User();
        forUpdate.setId(SessionUtils.getCurrentUser().getId());
        forUpdate.setEnterpriseId(enterprise.getId());

        return this.updateById(forUpdate);
    }

    @Override
    public User getByStudentCode(String studentCode) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getStudentCode, studentCode);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public boolean biddingTeacher(User user) {
        Teacher teacher = teacherService.getByTeacherCode(user.getTeacherCode());
        if (Objects.isNull(teacher)) {
            throw new BaseException("教师工号[" + user.getTeacherCode() + "]对应的信息不存在，请检查");
        }
        User forUpdate = new User();
        forUpdate.setId(SessionUtils.getCurrentUser().getId());
        forUpdate.setTeacherId(teacher.getId());
        return this.updateById(forUpdate);
    }
}
