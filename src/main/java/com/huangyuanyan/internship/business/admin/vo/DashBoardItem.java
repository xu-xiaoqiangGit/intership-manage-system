package com.huangyuanyan.internship.business.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-09-21 23:56
 */
@Data
@ApiModel(description = "控制台数据视图子类")
public class DashBoardItem {

    @ApiModelProperty(value = "日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @ApiModelProperty(value = "数量")
    private Integer count;

    @ApiModelProperty(value = "出售额度")
    private Double sales;
}
