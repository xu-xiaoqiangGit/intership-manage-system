package com.huangyuanyan.internship.business.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.admin.mapper.MenuMapper;
import com.huangyuanyan.internship.business.admin.model.Menu;
import com.huangyuanyan.internship.business.admin.service.MenuService;
import com.huangyuanyan.internship.business.admin.vo.MenuVO;
import com.huangyuanyan.internship.business.admin.vo.RouteVO;
import com.huangyuanyan.internship.business.admin.vo.TreeVO;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-26 00:12
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Override
    public List<MenuVO> listMenuVO(LambdaQueryWrapper<Menu> queryWrapper) {
        List<Menu> menuList = this.baseMapper.selectList(queryWrapper);
        Long parentId = GlobalConstant.ROOT_MENU_ID;
        // 使用stream获取集合中某个属性的最小值
        Optional<Menu> menuOptional = Optional
                .ofNullable(menuList)
                .orElse(new ArrayList<>())
                .stream()
                .min(Comparator.comparing(Menu::getParentId));
        if (menuOptional.isPresent()) {
            parentId = menuOptional.get().getParentId();
        }
        return recursionForMenuTree(parentId, menuList);
    }

    @Override
    public List<TreeVO> listTreeVO(LambdaQueryWrapper<Menu> queryWrapper) {
        List<Menu> menuList = this.baseMapper.selectList(queryWrapper);
        return recursionForTree(GlobalConstant.ROOT_MENU_ID, menuList);
    }

    /**
     * 获取路由数据
     *
     * @return 路由试图实体集合
     */
    @Override
    public List<RouteVO> listRoute() {
        List<Menu> menuList = this.baseMapper.listRoute();
        return recursionForRouteTree(GlobalConstant.ROOT_MENU_ID, menuList);
    }

    /**
     * 递归菜单列表数据
     *
     * @param parentId 父节点
     * @param menuList 菜单列表
     */
    private List<MenuVO> recursionForMenuTree(Long parentId, List<Menu> menuList) {
        List<MenuVO> menuVOList = new ArrayList<>();
        Optional.ofNullable(menuList).orElse(new ArrayList<>())
                .stream()
                .filter(menu -> menu.getParentId().equals(parentId))
                .forEach(menu -> {
                    MenuVO menuVO = new MenuVO();
                    BeanUtils.copyProperties(menu, menuVO);
                    List<MenuVO> children = recursionForMenuTree(menu.getId(), menuList);
                    menuVO.setChildren(children);
                    menuVOList.add(menuVO);
                });
        return menuVOList;
    }

    /**
     * 递归获取树形列表数据
     *
     * @param parentId 父节点ID
     * @param menuList 菜单列表
     */
    private List<TreeVO> recursionForTree(Long parentId, List<Menu> menuList) {
        List<TreeVO> treeVOList = new ArrayList<>();
        Optional.ofNullable(menuList).orElse(new ArrayList<>())
                .stream()
                .filter(menu -> menu.getParentId().equals(parentId))
                .forEach(menu -> {
                    TreeVO treeVO = new TreeVO();
                    treeVO.setId(menu.getId());
                    treeVO.setLabel(menu.getName());
                    List<TreeVO> children = recursionForTree(menu.getId(), menuList);
                    treeVO.setChildren(children);
                    treeVOList.add(treeVO);
                });
        return treeVOList;
    }

    /**
     * 递归获取路由树形列表数据
     *
     * @param parentId 父节点ID
     * @param menuList 菜单列表
     */
    private List<RouteVO> recursionForRouteTree(Long parentId, List<Menu> menuList) {
        List<RouteVO> routeVOList = new ArrayList<>();
        Optional.ofNullable(menuList).ifPresent(menus -> menus
                .stream()
                .filter(menu -> menu.getParentId().equals(parentId))
                .forEach(menu -> {
                    RouteVO routeVO = new RouteVO();
                    routeVO.setName(String.valueOf(menu.getId()));
                    routeVO.setPath(menu.getRoutePath());
                    routeVO.setRedirect(menu.getRedirect());
                    routeVO.setHidden(menu.getVisible().equals(0));
                    if (menu.getParentId().equals(GlobalConstant.ROOT_MENU_ID)) {
                        routeVO.setComponent("Layout");
                    } else {
                        routeVO.setComponent(menu.getComponent());
                    }
                    routeVO.setMeta(new RouteVO.Meta(
                            menu.getName(),
                            menu.getIcon(),
                            menu.getRoles()
                    ));
                    List<RouteVO> children = recursionForRouteTree(menu.getId(), menuList);
                    routeVO.setChildren(children);
                    if (CollectionUtils.isNotEmpty(routeVO.getChildren())) {
                        routeVO.setAlwaysShow(Boolean.TRUE);
                    }
                    routeVOList.add(routeVO);
                }));
        return routeVOList;
    }
}
