package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.model.UserRole;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface UserRoleService extends IService<UserRole> {
}
