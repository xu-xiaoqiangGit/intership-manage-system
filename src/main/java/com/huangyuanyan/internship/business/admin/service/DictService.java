package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.model.Dict;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface DictService extends IService<Dict> {

    /**
     * 新增字典
     *
     * @param dict 字典实体
     * @return 结果
     */
    boolean saveDict(Dict dict);

    /**
     * 修改字典
     *
     * @param dict 字典实体
     * @return 结果
     */
    boolean updateDict(Dict dict);
}
