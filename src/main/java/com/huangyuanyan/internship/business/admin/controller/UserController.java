package com.huangyuanyan.internship.business.admin.controller;

import cn.dev33.satoken.secure.SaSecureUtil;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.huangyuanyan.internship.business.admin.model.User;
import com.huangyuanyan.internship.business.admin.model.UserRole;
import com.huangyuanyan.internship.business.admin.service.PermissionService;
import com.huangyuanyan.internship.business.admin.service.UserRoleService;
import com.huangyuanyan.internship.business.admin.service.UserService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.constant.AuthConstant;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-07-27 16:00
 */
@Api(tags = "用户管理接口")
@RestController
@RequestMapping("/admin/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    private final UserRoleService userRoleService;

    private final PermissionService permissionService;

    @ApiOperation(value = "用户列表分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "limit", value = "每页数量", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "nickName", value = "昵称", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "phone", value = "手机号", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "status", value = "用户状态：0-正常,1-禁用", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "deptId", value = "部门ID", paramType = "query", dataType = "Long"),
    })
    @GetMapping("/list")
    public ResponseResult<List<User>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String nickName,
            String phone,
            Integer status
    ) {
        User user = new User();
        user.setNickName(nickName);
        user.setPhone(phone);
        user.setStatus(status);
        // 获取当前登录用户角色
        List<String> roles = StpUtil.getRoleList();
        if (CollUtil.isNotEmpty(roles)) {
            // 判断当前角色是否是ROOT
            user.setIsRoot(roles.contains(GlobalConstant.ROOT_ROLE_CODE));
        }
        Page<User> result = userService.list(new Page<>(page, limit), user);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "用户详情")
    @ApiImplicitParam(name = "id", value = "用户ID", paramType = "path", dataType = "Long", required = true)
    @GetMapping("/list/{id}")
    public ResponseResult<User> get(@PathVariable("id") Long id) {
        User user = userService.getById(id);
        // 查询用户角色数据
        if (null != user) {
            List<Long> roleIds = userRoleService.list(new LambdaQueryWrapper<UserRole>()
                            .select(UserRole::getRoleId)
                            .eq(UserRole::getUserId, user.getId()))
                    .stream().map(UserRole::getRoleId).collect(Collectors.toList());
            user.setRoleIds(roleIds);
        }
        return ResponseResult.success(user);
    }

    @ApiOperation(value = "新增用户")
    @ApiImplicitParam(name = "user", value = "用户实体JSON对象", paramType = "body", dataType = "User", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "id", "status", "point", "deptName", "roleCodes", "roleNames",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody User user) {
        boolean result = userService.saveUser(user);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改用户")
    @ApiImplicitParam(name = "user", value = "用户实体JSON对象", paramType = "body", dataType = "User", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "deptName", "roleCodes", "roleNames", "password", "status",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody User user) {
        boolean result = userService.updateUser(user);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "个人中心修改")
    @PutMapping("/personalUpdate")
    public ResponseResult<?> personalUpdate(@RequestBody User user) {
        User forUpdate = new User();
        forUpdate.setId(user.getId());
        forUpdate.setNickName(user.getNickName());
        forUpdate.setPhone(user.getPhone());
        forUpdate.setEmail(user.getEmail());

        boolean result = userService.updateUser(forUpdate);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改用户重要属性")
    @PatchMapping({"/update", "/updatePwd"})
    public ResponseResult<?> patch(@RequestBody User user) {
        Asserts.failed(ObjectUtil.isEmpty(user.getId()), ResultCode.PARAM_IS_NULL);

        User dbUser = userService.getById(user.getId());
        if (Objects.isNull(dbUser)) {
            throw new BusinessException("用户信息不存在");
        }
        if (StrUtil.isNotBlank(user.getOldPassword())) {
            String dePassword = SaSecureUtil.aesDecrypt(AuthConstant.ENCRYPT_KEY, dbUser.getPassword());
            if (!StringUtils.equals(dePassword, user.getOldPassword())) {
                throw new BusinessException("输入的旧密码错误，请检查");
            }
        }

        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(User::getId, user.getId());
        if (StrUtil.isNotBlank(user.getPassword())) {
            updateWrapper.set(User::getPassword, SaSecureUtil.aesEncrypt(AuthConstant.ENCRYPT_KEY, user.getPassword()));
        }
        updateWrapper.set(ObjectUtil.isNotEmpty(user.getStatus()), User::getStatus, user.getStatus());
        boolean result = userService.update(updateWrapper);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除用户")
    @ApiImplicitParam(name = "ids", value = "用户ID数组", paramType = "body", dataType = "Long[]", required = true)
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        User dbUser = userService.getById(ids[0]);
        if (Objects.isNull(dbUser)) {
            throw new BusinessException("用户已被删除，无需重复操作");
        }
        dbUser.setUserName(IdUtil.simpleUUID());
        userService.updateById(dbUser);

        boolean result = userService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "分配用户角色")
    @ApiImplicitParam(name = "user", value = "用户实体JSON对象", paramType = "body", dataType = "User", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "deptName", "roleCodes", "roleNames",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PutMapping("/roles")
    public ResponseResult<?> updateRole(@RequestBody User user) {
        boolean result = userService.updateRole(user);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "根据用户名获取用户信息")
    @ApiImplicitParam(name = "username", value = "用户名", paramType = "path", dataType = "String", required = true)
    @GetMapping("/username/{username}")
    public ResponseResult<User> getByUsername(@PathVariable("username") String username) {
        User user = userService.getByUsername(username);
        return ResponseResult.success(user);
    }

    @ApiOperation(value = "绑定学生信息")
    @PostMapping("/biddingStudent")
    public ResponseResult<Void> biddingStudent(@RequestBody User user) {
        boolean result = userService.biddingStudent(user);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "绑定企业信息")
    @PostMapping("/biddingEnterprise")
    public ResponseResult<Void> biddingEnterprise(@RequestBody User user) {
        boolean result = userService.biddingEnterprise(user);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "绑定教师信息")
    @PostMapping("/biddingTeacher")
    public ResponseResult<Void> biddingTeacher(@RequestBody User user) {
        boolean result = userService.biddingTeacher(user);
        return ResponseResult.judge(result);
    }
}
