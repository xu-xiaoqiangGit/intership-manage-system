package com.huangyuanyan.internship.business.admin.controller;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.huangyuanyan.internship.business.admin.model.Permission;
import com.huangyuanyan.internship.business.admin.service.PermissionService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.enums.QueryModeEnum;
import com.huangyuanyan.internship.common.exception.Asserts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-08-07 0:06
 */
@Api(tags = "权限管理接口")
@RestController
@RequestMapping("/admin/permission")
@RequiredArgsConstructor
@Slf4j
public class PermissionController {

    private final PermissionService permissionService;

    @ApiOperation(value = "权限列表分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "queryMode", value = "查询模式", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "limit", value = "每页数量", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "name", value = "权限名称", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "menuId", value = "菜单模块ID", paramType = "query", dataType = "Long")
    })
    @GetMapping("/list")
    public ResponseResult<List<Permission>> list(
            String queryMode,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String name,
            Long menuId
    ) {
        // 1.获取查询模式
        QueryModeEnum queryModeEnum = QueryModeEnum.getByCode(queryMode);
        // 2.构建查询条件
        LambdaQueryWrapper<Permission> queryWrapper = new LambdaQueryWrapper<Permission>();
        queryWrapper.like(StrUtil.isNotBlank(name), Permission::getName, name);
        queryWrapper.eq(ObjectUtil.isNotEmpty(menuId), Permission::getMenuId, menuId);
        queryWrapper.orderByDesc(Permission::getUpdateTime);
        queryWrapper.orderByDesc(Permission::getCreateTime);
        // 3.判断查询模式
        switch (queryModeEnum) {
            case PAGE:
                Page<Permission> result = permissionService.page(new Page<>(page, limit), queryWrapper);
                return ResponseResult.success(result.getRecords(), result.getTotal());
            case LIST:
                List<Permission> permissionList = permissionService.list(queryWrapper);
                return ResponseResult.success(permissionList);
            default:
                return ResponseResult.failed(ResultCode.QUERY_MODE_IS_NULL);
        }
    }

    @ApiOperation(value = "权限详情")
    @ApiImplicitParam(name = "id", value = "权限ID", paramType = "path", dataType = "Long", required = true)
    @GetMapping("/list/{id}")
    public ResponseResult<Permission> get(@PathVariable("id") Long id) {
        Permission permission = permissionService.getById(id);
        return ResponseResult.success(permission);
    }

    @ApiOperation(value = "新增权限")
    @ApiImplicitParam(name = "permission", value = "权限实体JSON对象", paramType = "body", dataType = "Permission", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "id", "roleCodes",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Permission permission) {
        boolean result = permissionService.savePermission(permission);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改权限")
    @ApiImplicitParam(name = "permission", value = "权限实体JSON对象", paramType = "body", dataType = "Permission", required = true)
    @ApiOperationSupport(ignoreParameters = {
            "roleCodes",
            "createTime", "updateTime", "deleted", "revision"
    })
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody Permission permission) {
        boolean result = permissionService.updatePermission(permission);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除权限")
    @ApiImplicitParam(name = "ids", value = "权限ID数组", paramType = "body", dataType = "Long[]", required = true)
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = permissionService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }

}
