package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.vo.MenuVO;
import com.huangyuanyan.internship.business.admin.vo.RouteVO;
import com.huangyuanyan.internship.business.admin.vo.TreeVO;
import com.huangyuanyan.internship.business.admin.model.Menu;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface MenuService extends IService<Menu> {

    /**
     * 菜单列表
     *
     * @param queryWrapper 条件构造器
     * @return 菜单视图实体集合
     */
    List<MenuVO> listMenuVO(LambdaQueryWrapper<Menu> queryWrapper);

    /**
     * 树状下拉数据
     *
     * @param queryWrapper 条件构造器
     * @return 树形数据集合
     */
    List<TreeVO> listTreeVO(LambdaQueryWrapper<Menu> queryWrapper);

    /**
     * 获取路由数据
     *
     * @return 路由试图实体集合
     */
    List<RouteVO> listRoute();
}
