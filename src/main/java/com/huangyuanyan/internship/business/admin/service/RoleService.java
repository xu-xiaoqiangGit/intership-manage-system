package com.huangyuanyan.internship.business.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.admin.model.Role;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-10-25 23:49
 */
public interface RoleService extends IService<Role> {

    /**
     * 删除角色列表
     *
     * @param ids 角色ID集合
     * @return 结果
     */
    boolean delete(List<Long> ids);

    /**
     * 新增角色
     *
     * @param role 角色实体
     * @return 结果
     */
    boolean saveRole(Role role);

    /**
     * 修改角色
     *
     * @param role 角色实体
     * @return 结果
     */
    boolean updateRole(Role role);
}
