package com.huangyuanyan.internship.business.internship.audit.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.internship.audit.dto.InternshipAuditQuery;
import com.huangyuanyan.internship.business.internship.audit.model.InternshipAudit;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:42
 */
public interface InternshipAuditService extends IService<InternshipAudit> {

    /**
     * 分页查询实习审核列表
     * @param page
     * @param internshipAuditQuery 查询条件
     * @return
     */
    Page<InternshipAudit> list(Page<InternshipAudit> page, InternshipAuditQuery internshipAuditQuery);

    /**
     * 新增实习审核记录
     *
     * @param internshipAudit
     * @return
     */
    boolean addInternshipAudit(InternshipAudit internshipAudit);

    /**
     * 修改审核状态
     *
     * @param internshipAudit
     * @return
     */
    boolean updateAuditStatus(InternshipAudit internshipAudit);

    /**
     * 根据实习申请id查询
     *
     * @param applyId
     * @return
     */
    InternshipAudit getByApplyId(Long applyId);
}
