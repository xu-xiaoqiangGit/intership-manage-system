package com.huangyuanyan.internship.business.internship.weekly.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeeklyScore;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-10 13:55
 */
public interface InternshipWeeklyScoreMapper extends BaseMapper<InternshipWeeklyScore> {
}
