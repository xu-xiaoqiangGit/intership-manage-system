package com.huangyuanyan.internship.business.internship.audit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.internship.audit.dto.InternshipAuditQuery;
import com.huangyuanyan.internship.business.internship.audit.model.InternshipAudit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:43
 */
public interface InternshipAuditMapper extends BaseMapper<InternshipAudit> {

    /**
     * 实习审核列表分页查询sql
     * @param page
     * @param internshipAuditQuery 查询条件
     * @return
     */
    List<InternshipAudit> list(Page<InternshipAudit> page, @Param("internshipAuditQuery") InternshipAuditQuery internshipAuditQuery);
}
