package com.huangyuanyan.internship.business.internship.audit.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description 实习审核列表查询条件
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:50
 */
@Data
public class InternshipAuditQuery {

    @ApiModelProperty("岗位名称")
    private String positionName;

    @ApiModelProperty("学生姓名")
    private String studentName;

    @ApiModelProperty("学生表主键id")
    private Long studentId;

    @ApiModelProperty("教师表主键id")
    private Long teacherId;
}
