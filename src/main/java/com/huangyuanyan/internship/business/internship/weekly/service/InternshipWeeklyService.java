package com.huangyuanyan.internship.business.internship.weekly.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.internship.weekly.dto.InternshipWeeklyQuery;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeekly;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-09 14:24
 */
public interface InternshipWeeklyService extends IService<InternshipWeekly> {

    /**
     * 分页查询实习周报列表
     *
     * @param page
     * @param internshipWeeklyQuery 查询条件
     * @return
     */
    Page<InternshipWeekly> list(Page<InternshipWeekly> page, InternshipWeeklyQuery internshipWeeklyQuery);

    /**
     * 通过实习申请记录id查询
     *
     * @param internshipApplyId
     * @return
     */
    List<InternshipWeekly> getByInternshipApplyId(Long internshipApplyId);

    /**
     * 通过实习申请记录id查询
     *
     * @param internshipApplyIds
     * @return
     */
    List<InternshipWeekly> getByInternshipApplyIds(List<Long> internshipApplyIds);

    /**
     * 标记周报已评分
     *
     * @param internshipWeeklyId 周报id
     */
    void markScored(Long internshipWeeklyId);
}
