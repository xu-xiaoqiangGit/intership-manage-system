package com.huangyuanyan.internship.business.internship.interview.controller;

import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.internship.interview.dto.InternshipInterviewQuery;
import com.huangyuanyan.internship.business.internship.interview.model.InternshipInterview;
import com.huangyuanyan.internship.business.internship.interview.service.InternshipInterviewService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-2-12 15:42
 */
@RestController
@RequestMapping("/internshipInterview")
@RequiredArgsConstructor
@Slf4j
public class InternshipInterviewController {

    private final InternshipInterviewService internshipInterviewService;

    @ApiOperation(value = "实习面试记录")
    @GetMapping("/list")
    public ResponseResult<List<InternshipInterview>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String positionName,
            String studentName
    ) {
        // 设置查询条件
        InternshipInterviewQuery query = new InternshipInterviewQuery();
        // 岗位名称
        query.setPositionName(positionName);
        // 学生姓名
        query.setStudentName(studentName);

        Page<InternshipInterview> result = internshipInterviewService.list(new Page<>(page, limit), query);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "我的实习面试记录")
    @GetMapping("/myList")
    public ResponseResult<List<InternshipInterview>> myList(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String positionName,
            String studentName
    ) {
        // 设置查询条件
        InternshipInterviewQuery query = new InternshipInterviewQuery();
        // 岗位名称
        query.setPositionName(positionName);
        // 学生姓名
        query.setStudentName(studentName);

        Page<InternshipInterview> result = internshipInterviewService.list(new Page<>(page, limit), query);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "实习面试详情")
    @GetMapping("/list/{id}")
    public ResponseResult<InternshipInterview> get(@PathVariable("id") Long id) {
        InternshipInterview interview = internshipInterviewService.getById(id);
        return ResponseResult.success(interview);
    }

    @ApiOperation(value = "新增实习面试")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody InternshipInterview internshipInterview) {
        boolean result = internshipInterviewService.addInternshipInterview(internshipInterview);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改实习面试")
    @PostMapping("/update")
    public ResponseResult<?> update(@RequestBody InternshipInterview internshipInterview) {
        boolean result = internshipInterviewService.updateById(internshipInterview);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除实习面试")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = internshipInterviewService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}
