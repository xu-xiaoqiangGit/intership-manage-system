package com.huangyuanyan.internship.business.internship.interview.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.internship.interview.dto.InternshipInterviewQuery;
import com.huangyuanyan.internship.business.internship.interview.model.InternshipInterview;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:42
 */
public interface InternshipInterviewService extends IService<InternshipInterview> {

    /**
     * 分页查询实习审核列表
     * @param page
     * @param internshipInterviewQuery 查询条件
     * @return
     */
    Page<InternshipInterview> list(Page<InternshipInterview> page, InternshipInterviewQuery internshipInterviewQuery);

    /**
     * 新增实习面试记录
     *
     * @param internshipInterview 实习面试
     * @return
     */
    boolean addInternshipInterview(InternshipInterview internshipInterview);
}
