package com.huangyuanyan.internship.business.internship.weekly.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.huangyuanyan.internship.business.internship.weekly.dto.InternshipWeeklyScoreQuery;
import com.huangyuanyan.internship.business.internship.weekly.mapper.InternshipWeeklyScoreMapper;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeeklyScore;
import com.huangyuanyan.internship.business.internship.weekly.service.InternshipWeeklyScoreService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-10 13:56
 */
@Service
public class InternshipWeeklyScoreServiceImpl extends ServiceImpl<InternshipWeeklyScoreMapper, InternshipWeeklyScore> implements InternshipWeeklyScoreService {

    @Override
    public List<InternshipWeeklyScore> list(InternshipWeeklyScoreQuery query) {
        LambdaQueryWrapper<InternshipWeeklyScore> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(InternshipWeeklyScore::getInternshipWeeklyId, query.getInternshipWeeklyId());
        // 评分人不为空，则通过评分人查询
        queryWrapper.eq(StrUtil.isNotBlank(query.getScoreMan()), InternshipWeeklyScore::getScoreMan, query.getScoreMan());
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<InternshipWeeklyScore> getByInternshipWeeklyIds(List<Long> scoredWeeklies) {
        if (CollUtil.isEmpty(scoredWeeklies)) {
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<InternshipWeeklyScore> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(InternshipWeeklyScore::getInternshipWeeklyId, scoredWeeklies);
        return baseMapper.selectList(queryWrapper);
    }
}
