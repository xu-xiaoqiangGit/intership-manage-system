package com.huangyuanyan.internship.business.internship.apply.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.enterprise.enums.PositionStatus;
import com.huangyuanyan.internship.business.enterprise.model.EnterprisePosition;
import com.huangyuanyan.internship.business.enterprise.service.EnterprisePositionService;
import com.huangyuanyan.internship.business.file.FileOperator;
import com.huangyuanyan.internship.business.internship.apply.dto.InternshipApplyQuery;
import com.huangyuanyan.internship.business.internship.apply.mapper.InternshipApplyMapper;
import com.huangyuanyan.internship.business.internship.apply.model.InternshipApply;
import com.huangyuanyan.internship.business.internship.apply.service.InternshipApplyService;
import com.huangyuanyan.internship.business.student.model.Student;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.BaseException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-20 23:15
 */
@Service
public class InternshipApplyServiceImpl extends ServiceImpl<InternshipApplyMapper, InternshipApply> implements InternshipApplyService {

    @Resource
    private FileOperator fileOperator;
    @Resource
    private EnterprisePositionService enterprisePositionService;

    @Override
    public Page<InternshipApply> list(Page<InternshipApply> page, InternshipApplyQuery internshipApplyQuery) {
        List<String> roleList = StpUtil.getRoleList();

        // 如果当前登录用户是学生，则设置学生表主键id查询 即 学生只能查看自己的实习申请记录
        if (roleList.contains(GlobalConstant.STUDENT_ROLE)) {
            Long studentId = SessionUtils.getCurrentUser().getStudentId();
            if (Objects.isNull(studentId)) {
                throw new BaseException("您还未绑定学生信息，请前往个人中心进行绑定后再查看");
            }
            internshipApplyQuery.setStudentId(studentId);
        }

        // 如果当前登录用户是企业，则设置企业表主键id查询 即 企业只能查看该企业的岗位申请记录
        if (roleList.contains(GlobalConstant.ENTERPRISE_ROLE)) {
            Long enterpriseId = SessionUtils.getCurrentUser().getEnterpriseId();
            if (Objects.isNull(enterpriseId)) {
                throw new BaseException("您还未绑定企业信息，请前往个人中心进行绑定");
            }
            internshipApplyQuery.setEnterpriseId(enterpriseId);
        }

        List<InternshipApply> internshipApplies = this.baseMapper.list(page, internshipApplyQuery);
        internshipApplies.forEach(internshipApply -> {
            // 存在学生简历
            if (StringUtils.isNotBlank(internshipApply.getResumeUrl())) {
                // 设置学生简历路径前缀，便于前端直接预览
                String name = FileUtil.getName(internshipApply.getResumeUrl());
                internshipApply.setResumeUrl(fileOperator.getPreviewUrl() + name);
            }
        });
        page.setRecords(internshipApplies);
        return page;
    }

    @Override
    public boolean addInternshipApply(InternshipApply internshipApply) {
        InternshipApply forAdd = new InternshipApply();
        forAdd.setPositionName(internshipApply.getPositionName());
        forAdd.setEnterpriseName(internshipApply.getEnterpriseName());

        // 校验实习岗位状态，已停止不允许再新增
        EnterprisePosition position = enterprisePositionService.getById(internshipApply.getEnterprisePositionId());
        if (PositionStatus.ifStop(position.getPositionStatus())) {
            throw new BaseException("该岗位已停止招聘");
        }

        forAdd.setEnterprisePositionId(internshipApply.getEnterprisePositionId());
        // 校验只有当前登录用户为学生才允许投递简历
        Student student = SessionUtils.getCurrentUser().getStudent();
        if (Objects.isNull(student)) {
            throw new BaseException("请前往个人中心绑定学生信息后，再投递简历");
        }

        forAdd.setStudentId(student.getId());
        forAdd.setStudentName(student.getStudentName());
        forAdd.setStudentCode(student.getStudentCode());
        forAdd.setGender(student.getGender());
        forAdd.setMajor(student.getMajor());
        forAdd.setGraduationYear(student.getGraduationYear());
        forAdd.setTeacherId(internshipApply.getTeacherId());
        forAdd.setStudentResumeId(internshipApply.getStudentResumeId());

        return this.save(forAdd);
    }
}
