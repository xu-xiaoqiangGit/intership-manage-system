package com.huangyuanyan.internship.business.internship.interview.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.internship.apply.enums.ApplyStatus;
import com.huangyuanyan.internship.business.internship.apply.model.InternshipApply;
import com.huangyuanyan.internship.business.internship.apply.service.InternshipApplyService;
import com.huangyuanyan.internship.business.internship.interview.dto.InternshipInterviewQuery;
import com.huangyuanyan.internship.business.internship.interview.mapper.InternshipInterviewMapper;
import com.huangyuanyan.internship.business.internship.interview.model.InternshipInterview;
import com.huangyuanyan.internship.business.internship.interview.service.InternshipInterviewService;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.BaseException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:43
 */
@Service
public class InternshipInterviewServiceImpl extends ServiceImpl<InternshipInterviewMapper, InternshipInterview> implements InternshipInterviewService {

    @Resource
    private InternshipApplyService internshipApplyService;

    @Override
    public Page<InternshipInterview> list(Page<InternshipInterview> page, InternshipInterviewQuery internshipInterviewQuery) {
        List<String> roleList = StpUtil.getRoleList();

        // 如果当前登录用户是学生设置学生主键表id,即学生只能查看自己的实习面试记录
        if (roleList.contains(GlobalConstant.STUDENT_ROLE)) {
            Long studentId = SessionUtils.getCurrentUser().getStudentId();
            if (Objects.isNull(studentId)) {
                throw new BaseException("您还未绑定学生信息，请前往个人中心进行绑定后再查看");
            }
            internshipInterviewQuery.setStudentId(studentId);
        }

        // 如果当前登录用户是企业设置企业主键表id，则只能查看该企业的实习面试记录
        if (roleList.contains(GlobalConstant.ENTERPRISE_ROLE)) {
            Long enterpriseId = SessionUtils.getCurrentUser().getEnterpriseId();
            if (Objects.isNull(enterpriseId)) {
                throw new BaseException("您还未绑定企业信息，请前往个人中心进行绑定");
            }
            internshipInterviewQuery.setEnterpriseId(enterpriseId);
        }

        List<InternshipInterview> internshipInterviews = this.baseMapper.list(page, internshipInterviewQuery);
        page.setRecords(internshipInterviews);
        return page;
    }

    @Override
    public boolean addInternshipInterview(InternshipInterview internshipInterview) {
        InternshipApply internshipApply = internshipApplyService.getById(internshipInterview.getInternshipApplyId());
        // 校验实习申请不在申请中状态不可以发起实习面试
        if (!ApplyStatus.ifApplying(internshipApply.getAuditStatus())) {
            throw new BaseException("该岗位已通过或已拒绝，无法发布实习面试");
        }

        InternshipInterview forAdd = new InternshipInterview();
        forAdd.setEnterprisePositionId(internshipApply.getEnterprisePositionId());
        forAdd.setInternshipApplyId(internshipApply.getId());
        forAdd.setStudentId(internshipApply.getStudentId());
        forAdd.setInterviewTime(internshipInterview.getInterviewTime());
        forAdd.setContactPerson(internshipInterview.getContactPerson());
        forAdd.setContactPhone(internshipInterview.getContactPhone());
        forAdd.setInterviewLocation(internshipInterview.getInterviewLocation());
        forAdd.setRemark(internshipInterview.getRemark());

        return this.save(forAdd);
    }
}
