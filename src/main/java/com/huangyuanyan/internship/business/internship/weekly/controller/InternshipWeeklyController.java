package com.huangyuanyan.internship.business.internship.weekly.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.file.FileOperator;
import com.huangyuanyan.internship.business.internship.audit.model.InternshipAudit;
import com.huangyuanyan.internship.business.internship.audit.service.InternshipAuditService;
import com.huangyuanyan.internship.business.internship.weekly.dto.InternshipWeeklyQuery;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeekly;
import com.huangyuanyan.internship.business.internship.weekly.service.InternshipWeeklyService;
import com.huangyuanyan.internship.business.student.model.Student;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BaseException;
import com.huangyuanyan.internship.common.exception.BusinessException;
import com.huangyuanyan.internship.common.util.LocalDateTimeUtils;
import com.huangyuanyan.internship.common.util.SessionUtils;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-09 14:02
 */
@RestController
@RequestMapping("/internshipWeekly")
@RequiredArgsConstructor
@Slf4j
public class InternshipWeeklyController {

    private final InternshipWeeklyService internshipWeeklyService;
    private final FileOperator fileOperator;
    private final InternshipAuditService internshipAuditService;

    @ApiOperation(value = "实习周报记录")
    @GetMapping("/list")
    public ResponseResult<List<InternshipWeekly>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String studentName,
            String positionName
    ) {
        // 设置查询条件
        InternshipWeeklyQuery query = new InternshipWeeklyQuery();
        // 岗位名称
        query.setPositionName(positionName);
        // 学生姓名
        query.setStudentName(studentName);

        Page<InternshipWeekly> result = internshipWeeklyService.list(new Page<>(page, limit), query);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "我的实习周报记录")
    @GetMapping("/myList")
    public ResponseResult<List<InternshipWeekly>> myList(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String positionName,
            Long enterpriseId
    ) {
        // 设置查询条件
        InternshipWeeklyQuery query = new InternshipWeeklyQuery();
        // 岗位名称
        query.setPositionName(positionName);
        // 企业表主键Id
        query.setEnterpriseId(enterpriseId);
        // 学生表主键id（即学生只能查看自己的实习周报）
        Long studentId = SessionUtils.getCurrentUser().getStudentId();
        if (Objects.isNull(studentId)) {
            throw new BaseException("您还未绑定学生信息，请前往个人中心进行绑定后再查看");
        }
        query.setStudentId(studentId);

        Page<InternshipWeekly> result = internshipWeeklyService.list(new Page<>(page, limit), query);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "实习周报详情")
    @GetMapping({"/list/{id}", "/myList/{id}"})
    public ResponseResult<InternshipWeekly> get(@PathVariable("id") Long id) {
        InternshipWeekly internshipWeekly = internshipWeeklyService.getById(id);
        // 设置周报附件下载地址和预览地址
        if (StringUtils.isNotBlank(internshipWeekly.getAnnexFile())) {
            String name = FileUtil.getName(internshipWeekly.getAnnexFile());
            internshipWeekly.setAnnexFileSuffix(FileNameUtil.getSuffix(name).toLowerCase());
            internshipWeekly.setDownFileUrl(internshipWeekly.getAnnexFile());
            internshipWeekly.setResumeUrl(fileOperator.getPreviewUrl() + name);
        }
        return ResponseResult.success(internshipWeekly);
    }

    @ApiOperation(value = "新增实习周报")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody InternshipWeekly internshipWeekly) {
        List<InternshipWeekly> internshipWeeklies = internshipWeeklyService.getByInternshipApplyId(internshipWeekly.getInternshipApplyId());
        // 获取本周开始时间和结束时间
        LocalDateTime weekStartTime = LocalDateTimeUtils.weekStartTime();
        LocalDateTime weekEndTime = LocalDateTimeUtils.weekEndTime();
        // 筛选本周已经提交的周报
        boolean existThisWeek = internshipWeeklies.stream().anyMatch(weekly -> {
            LocalDateTime createTime = LocalDateTimeUtils.convert(weekly.getCreateTime());
            return createTime.isAfter(weekStartTime) && createTime.isBefore(weekEndTime);
        });
        // 已存在本周提交的周报，不允许再次提交
        if (existThisWeek) {
            throw new BusinessException("所选日期[" + DateUtil.format(internshipWeekly.getCreateTime(), "yyyy-MM-dd") + "]，该周已提交周报，请选择下周时间再提交");
        }

        // 实习周报的评分教师应是实习审核时的教师
        InternshipAudit internshipAudit = internshipAuditService.getByApplyId(internshipWeekly.getInternshipApplyId());
        // 设置周报所属教师表主键id
        internshipWeekly.setTeacherId(internshipAudit.getTeacherId());

        Long studentId = internshipWeekly.getStudentId();
        Student currentStudent = SessionUtils.getCurrentUser().getStudent();
        // 如果当前用户不是学生，或学生主键id不对应，则给出错误提示
        // 因为周报只能由学生提交，所以这里不考虑教师提交周报的情况
        if (Objects.isNull(currentStudent) || !Objects.equals(studentId, currentStudent.getId())) {
            throw new BusinessException("非法用户信息，请重新登录后再操作");
        }

        internshipWeekly.setStudentName(currentStudent.getStudentName());
        internshipWeekly.setStudentCode(currentStudent.getStudentCode());

        boolean result = internshipWeeklyService.save(internshipWeekly);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改实习周报")
    @PostMapping("/update")
    public ResponseResult<?> update(@RequestBody InternshipWeekly internshipWeekly) {
        boolean result = internshipWeeklyService.updateById(internshipWeekly);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除实习周报")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = internshipWeeklyService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}

