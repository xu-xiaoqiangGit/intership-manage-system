package com.huangyuanyan.internship.business.internship.apply.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description 实习申请状态
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 17:33
 */
@AllArgsConstructor
@Getter
public enum ApplyStatus {
    /**
     * 已拒绝
     */
    REJECT(0),
    /**
     * 通过
     */
    PASS(1),
    /**
     * 申请中
     */
    APPLYING(2);

    private final Integer value;

    public static boolean ifApplying(Integer value) {
        return APPLYING.value.equals(value);
    }

    public static boolean ifPass(Integer value) {
        return PASS.value.equals(value);
    }
}
