package com.huangyuanyan.internship.business.internship.weekly.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileNameUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.ImmutableList;
import com.huangyuanyan.internship.business.file.FileOperator;
import com.huangyuanyan.internship.business.internship.weekly.dto.InternshipWeeklyQuery;
import com.huangyuanyan.internship.business.internship.weekly.mapper.InternshipWeeklyMapper;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeekly;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeeklyScore;
import com.huangyuanyan.internship.business.internship.weekly.service.InternshipWeeklyScoreService;
import com.huangyuanyan.internship.business.internship.weekly.service.InternshipWeeklyService;
import com.huangyuanyan.internship.business.teacher.service.TeacherService;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.BaseException;
import com.huangyuanyan.internship.common.model.BaseEntity;
import com.huangyuanyan.internship.common.util.SessionUtils;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-09 14:25
 */
@Service
@AllArgsConstructor
public class InternshipWeeklyServiceImpl extends ServiceImpl<InternshipWeeklyMapper, InternshipWeekly> implements InternshipWeeklyService {

    private final FileOperator fileOperator;
    private final InternshipWeeklyScoreService internshipWeeklyScoreService;
    private final TeacherService teacherService;

    @Override
    public Page<InternshipWeekly> list(Page<InternshipWeekly> page, InternshipWeeklyQuery internshipWeeklyQuery) {
        List<String> roleList = StpUtil.getRoleList();
        // 如果当前登录用户是企业，设置企业主键id查询，则只能查看该企业的实习周报
        if (roleList.contains(GlobalConstant.ENTERPRISE_ROLE)) {
            Long enterpriseId = SessionUtils.getCurrentUser().getEnterpriseId();
            if (Objects.isNull(enterpriseId)) {
                throw new BaseException("您还未绑定企业信息，请前往个人中心进行绑定");
            }
            internshipWeeklyQuery.setEnterpriseId(enterpriseId);
        }

        // 如果当前登录用户是教师，设置教师主键id，即只能查看属于自己指教学生的实习周报
        if (roleList.contains(GlobalConstant.TEACHER_ROLE)) {
            Long teacherId = SessionUtils.getCurrentUser().getTeacherId();
            if (Objects.isNull(teacherId)) {
                throw new BaseException("您还未绑定教师信息，请先前去个人中心绑定教师信息后再查看");
            }
            internshipWeeklyQuery.setTeacherId(teacherId);
        }

        List<InternshipWeekly> weeklies = this.baseMapper.list(page, internshipWeeklyQuery);

        // 获取所有已评分周报的主键id
        List<Long> scoredWeeklies = weeklies.stream().filter(weekly -> {
            return weekly.getScoreStatus().equals(1);
        }).map(BaseEntity::getId).collect(Collectors.toList());

        // 查询后分组
        Map<Long, List<InternshipWeeklyScore>> scoreGroup = internshipWeeklyScoreService.getByInternshipWeeklyIds(scoredWeeklies)
                .stream().collect(Collectors.groupingBy(InternshipWeeklyScore::getInternshipWeeklyId));

        Long currentUserId = SessionUtils.getCurrentUser().getId();
        weeklies.forEach(internshipWeekly -> {
            // 如果存在周报附件
            if (StringUtils.isNotBlank(internshipWeekly.getAnnexFile())) {
                // 设置附件下载地址和预览地址
                String name = FileUtil.getName(internshipWeekly.getAnnexFile());
                internshipWeekly.setAnnexFileSuffix(FileNameUtil.getSuffix(name).toLowerCase());
                internshipWeekly.setResumeUrl(fileOperator.getPreviewUrl() + name);
                internshipWeekly.setDownFileUrl(internshipWeekly.getAnnexFile());
            }

            // 设置是否已评分
            boolean scored = scoreGroup.getOrDefault(internshipWeekly.getId(), ImmutableList.of())
                    .stream().anyMatch(scores -> scores.getScoreId().equals(currentUserId));
            internshipWeekly.setScored(scored);
        });

        page.setRecords(weeklies);
        return page;
    }

    @Override
    public List<InternshipWeekly> getByInternshipApplyId(Long internshipApplyId) {
        LambdaQueryWrapper<InternshipWeekly> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(InternshipWeekly::getInternshipApplyId, internshipApplyId);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<InternshipWeekly> getByInternshipApplyIds(List<Long> internshipApplyIds) {
        LambdaQueryWrapper<InternshipWeekly> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(InternshipWeekly::getInternshipApplyId, internshipApplyIds);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void markScored(Long internshipWeeklyId) {
        InternshipWeekly forUpdate = new InternshipWeekly();
        forUpdate.setId(internshipWeeklyId);
        // 设置周报已评分
        forUpdate.setScoreStatus(1);
        baseMapper.updateById(forUpdate);
    }
}
