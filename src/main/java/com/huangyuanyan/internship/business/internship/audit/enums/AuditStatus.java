package com.huangyuanyan.internship.business.internship.audit.enums;

import com.huangyuanyan.internship.business.internship.apply.enums.SubmitAuditStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description 实习审核状态枚举
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 17:33
 */
@Getter
@AllArgsConstructor
public enum AuditStatus {

    AUDITING(0, "待审核", SubmitAuditStatus.NOT),
    PASS(1, "已通过", SubmitAuditStatus.AUDITED),
    REJECT(2, "已拒绝", SubmitAuditStatus.REJECT),
    CANCEL(3, "已取消", SubmitAuditStatus.NOT);

    private final Integer value;
    private final String desc;
    private final SubmitAuditStatus submitAuditStatus;

    public static AuditStatus getByValue(Integer value) {
        for (AuditStatus auditStatus : values()) {
            if (auditStatus.value.equals(value)) {
                return auditStatus;
            }
        }
        throw new IllegalArgumentException("Invalid value: " + value);
    }

    public static boolean ifAuditing(Integer value) {
        return AUDITING.value.equals(value);
    }

    public static boolean ifCancel(Integer value) {
        return CANCEL.value.equals(value);
    }

    public static boolean ifAudited(Integer value) {
        return PASS.value.equals(value) || REJECT.value.equals(value);
    }
}
