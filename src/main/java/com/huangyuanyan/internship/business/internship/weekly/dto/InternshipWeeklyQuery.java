package com.huangyuanyan.internship.business.internship.weekly.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Description 实习周报查询条件
 * @Author huangyuanyan
 * @Date Created in 2023-12-09 14:07
 */
@Data
public class InternshipWeeklyQuery {

    @ApiModelProperty("学生姓名")
    private String studentName;

    @ApiModelProperty("学生表主键id")
    private Long studentId;

    @ApiModelProperty("岗位名称")
    private String positionName;

    @ApiModelProperty("周报提交开始时间")
    private Date startTime;

    @ApiModelProperty("周报提交结束时间")
    private Date endTime;

    @ApiModelProperty("企业表主键id")
    private Long enterpriseId;

    @ApiModelProperty("班级表主键id（多个）")
    private List<Long> classesIds;

    @ApiModelProperty("教师表主键id")
    private Long teacherId;
}
