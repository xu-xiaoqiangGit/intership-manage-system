package com.huangyuanyan.internship.business.internship.audit.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.file.FileOperator;
import com.huangyuanyan.internship.business.internship.apply.enums.ApplyStatus;
import com.huangyuanyan.internship.business.internship.apply.enums.SubmitAuditStatus;
import com.huangyuanyan.internship.business.internship.apply.model.InternshipApply;
import com.huangyuanyan.internship.business.internship.apply.service.InternshipApplyService;
import com.huangyuanyan.internship.business.internship.audit.dto.InternshipAuditQuery;
import com.huangyuanyan.internship.business.internship.audit.enums.AuditStatus;
import com.huangyuanyan.internship.business.internship.audit.mapper.InternshipAuditMapper;
import com.huangyuanyan.internship.business.internship.audit.model.InternshipAudit;
import com.huangyuanyan.internship.business.internship.audit.service.InternshipAuditService;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.BaseException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:43
 */
@Service
public class InternshipAuditServiceImpl extends ServiceImpl<InternshipAuditMapper, InternshipAudit> implements InternshipAuditService {

    @Resource
    private InternshipAuditMapper internshipAuditMapper;
    @Resource
    private InternshipApplyService internshipApplyService;
    @Resource
    private FileOperator fileOperator;

    @Override
    public Page<InternshipAudit> list(Page<InternshipAudit> page, InternshipAuditQuery internshipAuditQuery) {
        List<String> roleList = StpUtil.getRoleList();

        // 如果当前登录用户是学生，设置学生表主键id，则只能学生自己的实习审核记录
        if (roleList.contains(GlobalConstant.STUDENT_ROLE)) {
            Long studentId = SessionUtils.getCurrentUser().getStudentId();
            if (Objects.isNull(studentId)) {
                throw new BaseException("您还未绑定学生信息，请前往个人中心进行绑定后再查看");
            }
            internshipAuditQuery.setStudentId(studentId);
        }

        // 如果当前登录用户是教师，设置教师表主键id，则只能查看属于学生送审教师是自己的实习审核记录
        if (roleList.contains(GlobalConstant.TEACHER_ROLE)) {
            Long teacherId = SessionUtils.getCurrentUser().getTeacherId();
            if (Objects.isNull(teacherId)) {
                throw new BaseException("您还未绑定教师信息，请先前去个人中心绑定教师信息后再查看");
            }
            internshipAuditQuery.setTeacherId(teacherId);
        }

        List<InternshipAudit> internshipAudits = this.baseMapper.list(page, internshipAuditQuery);
        // 设置简历预览地址
        internshipAudits.forEach(internshipAudit -> {
            if (StringUtils.isNotBlank(internshipAudit.getResumeUrl())) {
                String name = FileUtil.getName(internshipAudit.getResumeUrl());
                internshipAudit.setResumeUrl(fileOperator.getPreviewUrl() + name);
            }
        });
        page.setRecords(internshipAudits);
        return page;
    }

    @Override
    @Transactional
    public boolean addInternshipAudit(InternshipAudit internshipAudit) {
        InternshipApply internshipApply = internshipApplyService.getById(internshipAudit.getInternshipApplyId());
        // 实习申请通过后才能送审
        if (!ApplyStatus.ifPass(internshipApply.getAuditStatus())) {
            throw new BaseException("该实习申请未通过，不能实习送审");
        }
        // 已送审不能重复操作
        if (SubmitAuditStatus.ifSubmit(internshipApply.getSubmitAuditStatus())) {
            throw new BaseException("该实习已送审，不能重复操作");
        }

        InternshipAudit forAdd = new InternshipAudit();
        forAdd.setEnterprisePositionId(internshipApply.getEnterprisePositionId());
        forAdd.setInternshipApplyId(internshipApply.getId());
        forAdd.setStudentId(internshipApply.getStudentId());
        forAdd.setTeacherId(internshipAudit.getTeacherId());
        forAdd.setRemark(internshipAudit.getRemark());

        boolean saved = this.save(forAdd);
        if (saved) {
            // 送审成功，设置实习送审提交状态为：已提交
            InternshipApply forUpdate = new InternshipApply();
            forUpdate.setId(internshipApply.getId());
            forUpdate.setSubmitAuditStatus(SubmitAuditStatus.SUBMIT.getValue());
            internshipApplyService.updateById(forUpdate);
        }

        return saved;
    }

    @Override
    @Transactional
    public boolean updateAuditStatus(InternshipAudit internshipAudit) {
        InternshipAudit dbAudit = this.getById(internshipAudit.getId());
        // 审核状态已不是审核中不能操作修改
        if (!AuditStatus.ifAuditing(dbAudit.getAuditStatus())) {
            String desc = AuditStatus.getByValue(dbAudit.getAuditStatus()).getDesc();
            throw new BaseException("该实习审核状态已变更为[" + desc + "]，不能再次操作");
        }

        InternshipApply forUpdate = new InternshipApply();
        forUpdate.setId(dbAudit.getInternshipApplyId());
        // 审核取消，可以重新提交实习送审
        forUpdate.setSubmitAuditStatus(
                AuditStatus.getByValue(internshipAudit.getAuditStatus())
                        .getSubmitAuditStatus()
                        .getValue()
        );

        internshipApplyService.updateById(forUpdate);

        return this.updateById(internshipAudit);
    }

    @Override
    public InternshipAudit getByApplyId(Long applyId) {
        LambdaQueryWrapper<InternshipAudit> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(InternshipAudit::getInternshipApplyId, applyId);
        List<InternshipAudit> internshipAudits = baseMapper.selectList(queryWrapper);
        // 查询第一个状态为已审核的记录
        return internshipAudits.stream().filter(internshipAudit -> {
            return AuditStatus.ifAudited(internshipAudit.getAuditStatus());
        }).findFirst().orElse(null);
    }
}
