package com.huangyuanyan.internship.business.internship.interview.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description 实习面试列表查询条件
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:50
 */
@Data
public class InternshipInterviewQuery {

    @ApiModelProperty("岗位名称")
    private String positionName;

    @ApiModelProperty("学生姓名")
    private String studentName;

    @ApiModelProperty("学生表主键id")
    private Long studentId;

    @ApiModelProperty("企业表主键id")
    private Long enterpriseId;
}
