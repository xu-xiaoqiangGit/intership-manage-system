package com.huangyuanyan.internship.business.internship.weekly.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-10 10:41
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("internship_weekly_score")
@ApiModel("实习周报评分表实体类")
public class InternshipWeeklyScore extends BaseEntity {

    @ApiModelProperty("实习周报表主键id")
    private Long internshipWeeklyId;

    @ApiModelProperty("评分内容")
    private String scoreContent;

    @ApiModelProperty("分数")
    private BigDecimal score;

    @ApiModelProperty("评分人")
    private String scoreMan;

    @ApiModelProperty("评分人id")
    private Long scoreId;

}
