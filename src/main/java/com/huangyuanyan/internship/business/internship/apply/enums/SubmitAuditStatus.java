package com.huangyuanyan.internship.business.internship.apply.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description 实习送审状态
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 17:33
 */
@Getter
@AllArgsConstructor
public enum SubmitAuditStatus {
    /**
     * 未送审
     */
    NOT(0),
    /**
     * 已提交
     */
    SUBMIT(1),
    /**
     * 审核中
     */
    AUDITED(2),
    /**
     * 拒绝
     */
    REJECT(3);

    private final Integer value;

    public static boolean ifSubmit(Integer value) {
        return SUBMIT.value.equals(value);
    }
}
