package com.huangyuanyan.internship.business.internship.apply.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description 实习申请列表查询条件
 * @Author huangyuanyan
 * @Date Created in 2023-11-28 23:23
 */
@Data
public class InternshipApplyQuery {

    @ApiModelProperty("岗位名称")
    private String positionName;

    @ApiModelProperty("薪资起值")
    private BigDecimal salaryStart;

    @ApiModelProperty("薪资最高值")
    private BigDecimal salaryMax;

    @ApiModelProperty("学生主键id")
    private Long studentId;

    @ApiModelProperty("企业主键id")
    private Long enterpriseId;
}
