package com.huangyuanyan.internship.business.internship.weekly.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ArrayUtil;
import com.huangyuanyan.internship.business.admin.vo.UserVO;
import com.huangyuanyan.internship.business.enterprise.model.Enterprise;
import com.huangyuanyan.internship.business.internship.weekly.dto.InternshipWeeklyScoreQuery;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeeklyScore;
import com.huangyuanyan.internship.business.internship.weekly.service.InternshipWeeklyScoreService;
import com.huangyuanyan.internship.business.internship.weekly.service.InternshipWeeklyService;
import com.huangyuanyan.internship.business.teacher.model.Teacher;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BusinessException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-10 13:02
 */
@RestController
@RequestMapping("/internshipWeeklyScore")
@RequiredArgsConstructor
@Slf4j
public class InternshipWeeklyScoreController {

    private final InternshipWeeklyScoreService internshipWeeklyScoreService;
    private final InternshipWeeklyService internshipWeeklyService;

    @ApiOperation(value = "实习周报评分记录")
    @GetMapping("/list")
    public ResponseResult<List<InternshipWeeklyScore>> list(
            Long internshipWeeklyId,
            String scoreMan
    ) {
        // 设置查询条件
        InternshipWeeklyScoreQuery query = new InternshipWeeklyScoreQuery();
        // 实习周报表主键id
        query.setInternshipWeeklyId(internshipWeeklyId);
        // 评分人
        query.setScoreMan(scoreMan);

        List<InternshipWeeklyScore> result = internshipWeeklyScoreService.list(query);
        return ResponseResult.success(result);
    }

    @ApiOperation(value = "实习周报评分详情")
    @GetMapping({"/list/{id}"})
    public ResponseResult<InternshipWeeklyScore> get(@PathVariable("id") Long id) {
        // 根据实习周报评分表主键id查询
        InternshipWeeklyScore weeklyScore = internshipWeeklyScoreService.getById(id);
        return ResponseResult.success(weeklyScore);
    }

    @ApiOperation(value = "新增实习周报评分")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody InternshipWeeklyScore internshipWeeklyScore) {
        UserVO currentUser = SessionUtils.getCurrentUser();
        internshipWeeklyScore.setScoreId(currentUser.getId());

        List<String> roleList = StpUtil.getRoleList();
        // 判断当前用户是否为教师
        if (roleList.contains(GlobalConstant.TEACHER_ROLE)) {
            Teacher teacher = SessionUtils.getCurrentUser().getTeacher();
            if (Objects.isNull(teacher)) {
                throw new BusinessException("您还未绑定教师信息，请前往个人中心进行绑定");
            }
            // 设置评分人
            internshipWeeklyScore.setScoreMan(teacher.getTeacherName());
        }

        // 判断当前用户是否为企业
        if (roleList.contains(GlobalConstant.ENTERPRISE_ROLE)) {
            Enterprise enterprise = SessionUtils.getCurrentUser().getEnterprise();
            if (Objects.isNull(enterprise)) {
                throw new BusinessException("您还未绑定企业信息，请前往个人中心进行绑定");
            }
            // 设置评分人
            internshipWeeklyScore.setScoreMan("企业-" + enterprise.getEnterpriseName());
        }

        boolean result = internshipWeeklyScoreService.save(internshipWeeklyScore);
        // 标记周报已评分
        if (result) {
            internshipWeeklyService.markScored(internshipWeeklyScore.getInternshipWeeklyId());
        }

        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改实习周报评分")
    @PostMapping("/update")
    public ResponseResult<?> update(@RequestBody InternshipWeeklyScore internshipWeeklyScore) {
        boolean result = internshipWeeklyScoreService.updateById(internshipWeeklyScore);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除实习周报")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = internshipWeeklyScoreService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}

