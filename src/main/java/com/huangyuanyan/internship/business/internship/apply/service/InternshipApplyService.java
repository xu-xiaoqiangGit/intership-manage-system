package com.huangyuanyan.internship.business.internship.apply.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.internship.apply.dto.InternshipApplyQuery;
import com.huangyuanyan.internship.business.internship.apply.model.InternshipApply;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-20 23:00
 */
public interface InternshipApplyService extends IService<InternshipApply> {

    /**
     * 分页查询实习申请列表
     *
     * @param page
     * @param internshipApplyQuery 查询条件
     * @return
     */
    Page<InternshipApply> list(Page<InternshipApply> page, InternshipApplyQuery internshipApplyQuery);

    /**
     * 新增实习申请（学生投递简历）
     *
     * @param internshipApply 新增数据
     * @return 结果
     */
    boolean addInternshipApply(InternshipApply internshipApply);
}
