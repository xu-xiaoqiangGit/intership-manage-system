package com.huangyuanyan.internship.business.internship.apply.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.internship.apply.dto.InternshipApplyQuery;
import com.huangyuanyan.internship.business.internship.apply.model.InternshipApply;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-20 23:15
 */
public interface InternshipApplyMapper extends BaseMapper<InternshipApply> {

    /**
     * 自定义实习申请列表分页查询sql
     * @param page
     * @param internshipApplyQuery 查询条件
     * @return
     */
    List<InternshipApply> list(Page<InternshipApply> page, @Param("internshipApplyQuery") InternshipApplyQuery internshipApplyQuery);
}
