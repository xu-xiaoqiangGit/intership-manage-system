package com.huangyuanyan.internship.business.internship.weekly.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description 实习周报评分查询条件
 * @Author huangyuanyan
 * @Date Created in 2023-12-10 13:58
 */
@Data
public class InternshipWeeklyScoreQuery {

    @ApiModelProperty("实习周报表主键Id")
    private Long internshipWeeklyId;

    @ApiModelProperty("评分人")
    private String scoreMan;
}
