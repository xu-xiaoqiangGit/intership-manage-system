package com.huangyuanyan.internship.business.internship.audit.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-02-13 15:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("internship_audit")
@ApiModel("实习审核表实体类")
public class InternshipAudit extends BaseEntity {

    @ApiModelProperty("企业岗位表主键id")
    private Long enterprisePositionId;

    @ApiModelProperty("实习申请表主键id")
    private Long internshipApplyId;

    @ApiModelProperty("学生表主键id")
    private Long studentId;

    @ApiModelProperty("教师表主键id")
    private Long teacherId;

    @ApiModelProperty("审核状态（0-待审核, 1-通过, 2-不通过, 3-已取消）")
    private Integer auditStatus;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("审核备注")
    private String auditRemark;

    /*******以下字段用于前端展示********/
    @TableField(exist = false)
    private String resumeUrl;

    @TableField(exist = false)
    private String enterpriseName;

    @TableField(exist = false)
    private String positionName;

    @TableField(exist = false)
    private String teacherName;

    @TableField(exist = false)
    private String studentName;

    @TableField(exist = false)
    private String studentPhone;

    @TableField(exist = false)
    private Integer gender;

    @TableField(exist = false)
    private BigDecimal salaryStart;

    @TableField(exist = false)
    private BigDecimal salaryMax;
}
