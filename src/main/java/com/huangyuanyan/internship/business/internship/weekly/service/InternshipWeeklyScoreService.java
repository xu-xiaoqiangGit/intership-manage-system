package com.huangyuanyan.internship.business.internship.weekly.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.internship.weekly.dto.InternshipWeeklyScoreQuery;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeeklyScore;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-10 13:55
 */
public interface InternshipWeeklyScoreService extends IService<InternshipWeeklyScore> {

    /**
     * 查询实习周报评分记录
     *
     * @param query 查询条件
     * @return
     */
    List<InternshipWeeklyScore> list(InternshipWeeklyScoreQuery query);

    /**
     * 通过实习周报id查询
     *
     * @param scoredWeeklies 实习周报id
     * @return
     */
    List<InternshipWeeklyScore> getByInternshipWeeklyIds(List<Long> scoredWeeklies);
}
