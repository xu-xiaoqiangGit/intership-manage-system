package com.huangyuanyan.internship.business.internship.interview.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.internship.interview.dto.InternshipInterviewQuery;
import com.huangyuanyan.internship.business.internship.interview.model.InternshipInterview;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:43
 */
public interface InternshipInterviewMapper extends BaseMapper<InternshipInterview> {

    /**
     * 分页查询实习面试列表sql
     *
     * @param page
     * @param internshipInterviewQuery 查询条件
     * @return
     */
    List<InternshipInterview> list(Page<InternshipInterview> page, @Param("internshipInterviewQuery") InternshipInterviewQuery internshipInterviewQuery);
}
