package com.huangyuanyan.internship.business.internship.apply.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.enterprise.enums.SalaryRange;
import com.huangyuanyan.internship.business.file.FileOperator;
import com.huangyuanyan.internship.business.internship.apply.dto.InternshipApplyQuery;
import com.huangyuanyan.internship.business.internship.apply.model.InternshipApply;
import com.huangyuanyan.internship.business.internship.apply.service.InternshipApplyService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-26 23:20
 */
@RestController
@RequestMapping("/internshipApply")
@RequiredArgsConstructor
@Slf4j
public class InternshipApplyController {

    private final InternshipApplyService internshipApplyService;
    private final FileOperator fileOperator;

    @ApiOperation(value = "实习申请记录")
    @GetMapping("/list")
    public ResponseResult<List<InternshipApply>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String positionName,
            Integer salaryQuery
    ) {
        // 设置查询条件
        InternshipApplyQuery query = new InternshipApplyQuery();
        // 岗位名称
        query.setPositionName(positionName);

        // 设置薪资区间
        Optional.ofNullable(salaryQuery).ifPresent(salary -> {
            SalaryRange salaryRange = SalaryRange.fromValue(salary);
            query.setSalaryStart(salaryRange.getSalaryStart());
            query.setSalaryMax(salaryRange.getSalaryMax());
        });
        Page<InternshipApply> result = internshipApplyService.list(new Page<>(page, limit), query);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "我的实习申请记录")
    @GetMapping("/myList")
    public ResponseResult<List<InternshipApply>> myList(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String positionName,
            Integer salaryQuery
    ) {
        // 岗位名称
        InternshipApplyQuery query = new InternshipApplyQuery();
        query.setPositionName(positionName);

        // 设置薪资区间
        Optional.ofNullable(salaryQuery).ifPresent(salary -> {
            SalaryRange salaryRange = SalaryRange.fromValue(salary);
            query.setSalaryStart(salaryRange.getSalaryStart());
            query.setSalaryMax(salaryRange.getSalaryMax());
        });
        Page<InternshipApply> result = internshipApplyService.list(new Page<>(page, limit), query);
        List<InternshipApply> internshipApplies = result.getRecords();

        return ResponseResult.success(internshipApplies, result.getTotal());
    }

    @ApiOperation(value = "实习申请详情")
    @GetMapping({"/list/{id}", "/myList/{id}"})
    public ResponseResult<InternshipApply> get(@PathVariable("id") Long id) {
        InternshipApply internshipApply = internshipApplyService.getById(id);
        // 如果存在简历
        if (StringUtils.isNotBlank(internshipApply.getResumeUrl())) {
            // 设置简历下载地址和预览地址
            String name = FileUtil.getName(internshipApply.getResumeUrl());
            internshipApply.setDownFileUrl(internshipApply.getResumeUrl());
            internshipApply.setResumeUrl(fileOperator.getPreviewUrl() + name);
        }
        // 设置薪资区间值
        SalaryRange salaryRange = SalaryRange.rangeOf(internshipApply.getSalaryStart(), internshipApply.getSalaryMax());
        internshipApply.setSalary(salaryRange.getValue());

        return ResponseResult.success(internshipApply);
    }

    @ApiOperation(value = "新增实习申请")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody InternshipApply internshipApply) {
        boolean result = internshipApplyService.addInternshipApply(internshipApply);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改实习申请")
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody InternshipApply internshipApply) {
        // 直接修改
        boolean result = internshipApplyService.updateById(internshipApply);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除实习申请")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = internshipApplyService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}

