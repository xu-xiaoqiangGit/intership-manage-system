package com.huangyuanyan.internship.business.internship.apply.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-20 22:41
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("internship_apply")
@ApiModel("实习申请表实体类")
public class InternshipApply extends BaseEntity {

    @ApiModelProperty("企业岗位表主键id")
    private Long enterprisePositionId;

    @ApiModelProperty("企业表主键id")
    @TableField(exist = false)
    private Long enterpriseId;

    @ApiModelProperty("企业名称")
    private String enterpriseName;

    @ApiModelProperty("岗位名称")
    private String positionName;

    @ApiModelProperty("学生表主键id")
    private Long studentId;

    @ApiModelProperty("学生编号")
    private String studentCode;

    @ApiModelProperty("教师表主键id")
    private Long teacherId;

    @ApiModelProperty("学生简历表主键id")
    private Long studentResumeId;

    @ApiModelProperty("审核回复")
    private String auditReply;

    @ApiModelProperty("审核状态（0-拒绝, 1-通过, 2-审核中）")
    private Integer auditStatus;

    @ApiModelProperty("送审状态（0-未送审, 1-已送审）")
    private Integer submitAuditStatus;

    /*******以下字段用于前端展示********/
    @TableField(exist = false)
    private Integer gender;

    @TableField(exist = false)
    private String studentName;

    @TableField(exist = false)
    private String phoneNumber;

    @TableField(exist = false)
    private String major;

    @TableField(exist = false)
    private String graduationYear;

    @TableField(exist = false)
    private String resumeUrl;

    @TableField(exist = false)
    private String downFileUrl;

    @TableField(exist = false)
    private String teacherName;

    @TableField(exist = false)
    private BigDecimal salaryStart;

    @TableField(exist = false)
    private BigDecimal salaryMax;

    @TableField(exist = false)
    private Integer salary;

    /**
     * 本周是否已提交实习周报
     */
    @TableField(exist = false)
    private Boolean existThisWeek;
}
