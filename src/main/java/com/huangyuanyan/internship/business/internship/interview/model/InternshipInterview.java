package com.huangyuanyan.internship.business.internship.interview.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-02-12 15:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("internship_interview")
@ApiModel("实习面试表实体类")
public class InternshipInterview extends BaseEntity {

    @ApiModelProperty("企业岗位表主键id")
    private Long enterprisePositionId;

    @ApiModelProperty("实习申请表主键id")
    private Long internshipApplyId;

    @ApiModelProperty("学生表主键id")
    private Long studentId;

    @ApiModelProperty("面试状态（0-待面试, 1-面试通过, 2-面试不合格, 3-面试已取消）")
    private Integer interviewStatus;

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("面试时间")
    private Date interviewTime;

    @ApiModelProperty("联系人")
    private String contactPerson;

    @ApiModelProperty("联系电话")
    private String contactPhone;

    @ApiModelProperty("面试地址")
    private String interviewLocation;

    @ApiModelProperty("备注")
    private String remark;

    /**********以下字段用于前端展示**********/
    @TableField(exist = false)
    private String enterpriseName;

    @TableField(exist = false)
    private String positionName;

    @TableField(exist = false)
    private String studentName;

    @TableField(exist = false)
    private String studentPhone;

    @TableField(exist = false)
    private Integer gender;

    @TableField(exist = false)
    private BigDecimal salaryStart;

    @TableField(exist = false)
    private BigDecimal salaryMax;
}
