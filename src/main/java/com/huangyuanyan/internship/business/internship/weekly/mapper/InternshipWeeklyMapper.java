package com.huangyuanyan.internship.business.internship.weekly.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.internship.weekly.dto.InternshipWeeklyQuery;
import com.huangyuanyan.internship.business.internship.weekly.model.InternshipWeekly;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-09 14:25
 */
public interface InternshipWeeklyMapper extends BaseMapper<InternshipWeekly> {

    /**
     * 分页查询实习周报列表sql
     *
     * @param page
     * @param internshipWeeklyQuery 查询条件
     * @return
     */
    List<InternshipWeekly> list(Page<InternshipWeekly> page, @Param("query") InternshipWeeklyQuery internshipWeeklyQuery);
}
