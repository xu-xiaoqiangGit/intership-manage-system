package com.huangyuanyan.internship.business.internship.weekly.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-09 13:41
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("internship_weekly")
@ApiModel("实习周报表实体类")
public class InternshipWeekly extends BaseEntity {

    @ApiModelProperty("实习申请表主键id")
    private Long internshipApplyId;

    @ApiModelProperty("企业岗位表主键id")
    private Long enterprisePositionId;

    @ApiModelProperty("企业名称")
    private String enterpriseName;

    @ApiModelProperty("岗位名称")
    private String positionName;

    @ApiModelProperty("学生表主键Id")
    private Long studentId;

    @ApiModelProperty("学生编号")
    private String studentCode;

    @ApiModelProperty("教师表主键Id")
    private Long teacherId;

    @ApiModelProperty("本周内容")
    private String thisWeeklyContent;

    @ApiModelProperty("下周内容")
    private String nextWeeklyContent;

    @ApiModelProperty("周报附件")
    private String annexFile;

    @ApiModelProperty("周报评分状态（0-未评分，1-已评分）")
    private Integer scoreStatus;

    /********以下字段用于前端展示********/
    @TableField(exist = false)
    private Long enterpriseId;

    @TableField(exist = false)
    private String studentName;

    @TableField(exist = false)
    private String resumeUrl;

    @TableField(exist = false)
    private String downFileUrl;

    @TableField(exist = false)
    private String teacherName;

    @TableField(exist = false)
    private String positionDescription;

    @TableField(exist = false)
    private String positionCity;

    @TableField(exist = false)
    private String annexFileSuffix;

    /**
     * 周报已评分
     */
    @TableField(exist = false)
    private Boolean scored;
}
