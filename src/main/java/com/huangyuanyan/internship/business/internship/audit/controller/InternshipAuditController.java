package com.huangyuanyan.internship.business.internship.audit.controller;

import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.internship.audit.dto.InternshipAuditQuery;
import com.huangyuanyan.internship.business.internship.audit.model.InternshipAudit;
import com.huangyuanyan.internship.business.internship.audit.service.InternshipAuditService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2024-2-12 15:42
 */
@RestController
@RequestMapping("/internshipAudit")
@RequiredArgsConstructor
@Slf4j
public class InternshipAuditController {

    private final InternshipAuditService internshipAuditService;

    @ApiOperation(value = "实习审核记录")
    @GetMapping("/list")
    public ResponseResult<List<InternshipAudit>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String positionName,
            String studentName
    ) {
        // 构造查询条件
        InternshipAuditQuery query = new InternshipAuditQuery();
        // 岗位名称
        query.setPositionName(positionName);
        // 学生姓名
        query.setStudentName(studentName);

        Page<InternshipAudit> result = internshipAuditService.list(new Page<>(page, limit), query);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "我的实习审核记录")
    @GetMapping("/myList")
    public ResponseResult<List<InternshipAudit>> myList(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String positionName,
            String studentName
    ) {
        // 构造查询条件
        InternshipAuditQuery query = new InternshipAuditQuery();
        // 岗位名称
        query.setPositionName(positionName);
        // 学生姓名
        query.setStudentName(studentName);

        Page<InternshipAudit> result = internshipAuditService.list(new Page<>(page, limit), query);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "实习审核详情")
    @GetMapping({"/list/{id}", "/myList/{id}"})
    public ResponseResult<InternshipAudit> get(@PathVariable("id") Long id) {
        InternshipAudit interview = internshipAuditService.getById(id);
        return ResponseResult.success(interview);
    }

    @ApiOperation(value = "查看实习审核详情")
    @GetMapping("/auditDetail/{applyId}")
    public ResponseResult<InternshipAudit> getByApplyId(@PathVariable("applyId") Long applyId) {
        InternshipAudit internshipAudit = internshipAuditService.getByApplyId(applyId);
        return ResponseResult.success(internshipAudit);
    }

    @ApiOperation(value = "新增实习审核")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody InternshipAudit internshipAudit) {
        boolean result = internshipAuditService.addInternshipAudit(internshipAudit);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改实习审核")
    @PostMapping("/update")
    public ResponseResult<?> update(@RequestBody InternshipAudit internshipAudit) {
        boolean result = internshipAuditService.updateAuditStatus(internshipAudit);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除实习审核")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = internshipAuditService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}
