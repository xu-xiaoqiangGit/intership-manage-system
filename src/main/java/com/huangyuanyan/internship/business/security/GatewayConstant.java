package com.huangyuanyan.internship.business.security;

import java.util.Arrays;
import java.util.List;

public interface GatewayConstant {

    /**
     * 忽略请求URL
     */
    List<String> IGNORE_REQUEST_URL = Arrays.asList(
            "GET:/admin/route",
            "GET:/notice/systemList",
            "GET:/notice/systemList/**",
            "*:/**/personalUpdate",
            "*:/admin/user/updatePwd",
            "*:/file/**"
    );
}
