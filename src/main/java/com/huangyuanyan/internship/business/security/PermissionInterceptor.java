package com.huangyuanyan.internship.business.security;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import com.github.xiaoymin.knife4j.core.util.StrUtil;
import com.huangyuanyan.internship.business.admin.service.PermissionService;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.constant.AuthConstant;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.util.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description 权限拦截器
 * @Author huangyuanyan
 * @Date Created in 2023-10-27 0:00
 */
@Slf4j
public class PermissionInterceptor implements HandlerInterceptor {

    @Resource
    private PermissionService permissionService;
    @Resource
    private ServerProperties serverProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取请求信息
        // 请求方法
        String method = request.getMethod();
        String path = request.getRequestURI().replaceFirst(serverProperties.getServlet().getContextPath(), "");
        // 请求路径
        String restFulPath = method + ":" + path;
        // Ant匹配器
        PathMatcher pathMatcher = new AntPathMatcher();

        // 如果匹配白名单请求，则放行
        if (GatewayConstant.IGNORE_REQUEST_URL.stream()
                .anyMatch(ignoreUrl -> pathMatcher.match(ignoreUrl, restFulPath))) {
            return true;
        }
        // token为空（未登录）拒绝访问
        String token = request.getHeader(AuthConstant.AUTHORIZATION_KEY);
        if (StrUtil.isBlank(token)) {
            ResponseUtils.writerErrorInfo(response, ResultCode.TOKEN_INVALID_OR_EXPIRED);
            return false;
        }

        // 获取资源权限角色关系列表
        Map<String, List<String>> resourceRolesRules = permissionService.selectUrlPermissions();
        // 根据请求路径判断有访问权限的角色列表
        // 访问此次请求需要的权限
        List<String> authorizedRoles = new ArrayList<>();
        resourceRolesRules.forEach((k, v) -> {
            if (pathMatcher.match(k, restFulPath)) {
                authorizedRoles.addAll(v);
            }
        });
        log.info("访问资源: {} 所需权限: {}", restFulPath, authorizedRoles);

        // 如果是超级管理员则放行
        List<String> roleList = StpUtil.getRoleList();
        if (roleList.contains(GlobalConstant.ROOT_ROLE_CODE)) {
            return true;
        }

        // 判断请求的用户角色是否有权限
        boolean result = CollUtil.isNotEmpty(authorizedRoles)
                && roleList.stream().anyMatch(authorizedRoles::contains);
        if (!result) {
            ResponseUtils.writerErrorInfo(response, ResultCode.ACCESS_UNAUTHORIZED);
        }
        return result;
    }
}
