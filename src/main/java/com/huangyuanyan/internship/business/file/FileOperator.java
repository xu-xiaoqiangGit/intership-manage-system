package com.huangyuanyan.internship.business.file;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 19:05
 */
@Component
public class FileOperator {

    private static final String FILES_DIR = "/files/";
    @Value("${server.port:9090}")
    private String port;
    @Value("${file.download.ip:localhost}")
    private String downloadIp;
    @Value("${server.servlet.context-path}")
    private String contextPath;

    /**
     * 获取文件的完整路径
     *
     * @param fileFullName
     * @return
     */
    public String getFileUploadPath(String fileFullName) {
        String uploadPath = System.getProperty("user.dir");
        return uploadPath + FILES_DIR + fileFullName;
    }

    public String getFileDir() {
        return "http://" + downloadIp + ":" + port + contextPath + "/file/download/";
    }

    public String getPreviewUrl() {
        return "http://" + downloadIp + ":" + port + contextPath + "/file/preview/";
    }
}
