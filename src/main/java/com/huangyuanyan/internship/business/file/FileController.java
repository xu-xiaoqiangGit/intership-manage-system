package com.huangyuanyan.internship.business.file;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.huangyuanyan.internship.common.api.ResponseResult;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-09 23:34
 */
@Api(tags = "上传文件接口")
@RestController
@RequestMapping("/file")
@RequiredArgsConstructor
@Slf4j
public class FileController {

    private final FileOperator fileOperator;

    /**
     * 上传文件
     *
     * @param file
     */
    @PostMapping("/upload")
    public ResponseResult<String> upload(MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();  // 文件完整的名称
        String extName = FileUtil.extName(originalFilename);  // 文件后缀名
        String uniFileFlag = IdUtil.fastSimpleUUID();
        String fileFullName = uniFileFlag + StrUtil.DOT + extName;
        // 封装完整的文件路径获取方法
        String fileUploadPath = fileOperator.getFileUploadPath(fileFullName);
        //  完整的上传文件名： D:\知识星球\partner-back/files/1231321321321321982321.jpg
        long size = file.getSize();  // 单位是 byte, size / 1024 -> kb
//        byte[] bytes = file.getBytes();
        String name = file.getName();
        log.info("{}, {}, {}", originalFilename, size, name);
        String md5 = SecureUtil.md5(file.getInputStream());
        try {
            java.io.File uploadFile = new java.io.File(fileUploadPath);
            java.io.File parentFile = uploadFile.getParentFile();
            if (!parentFile.exists()) {  // 如果父级不存在，也就是说files目录不存在，那么我要创建出来
                parentFile.mkdirs();
            }
            file.transferTo(uploadFile);
        } catch (Exception e) {
            log.error("文件上传失败", e);
            return ResponseResult.failed("文件上传失败");
        }

        String url = fileOperator.getFileDir() + fileFullName;
        return ResponseResult.success(url, "操作成功");
    }

    /**
     * 下载文件
     *
     * @param fileFullName 文件全名
     * @param response
     * @throws IOException
     */
    @GetMapping("/download/{fileFullName}")
    @CrossOrigin
    public void downloadFile(@PathVariable String fileFullName,
                             HttpServletResponse response) throws IOException {
        String fileUploadPath = fileOperator.getFileUploadPath(fileFullName);
        byte[] bytes = FileUtil.readBytes(fileUploadPath);
        response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileFullName, "UTF-8"));  // 附件下载
        OutputStream os = response.getOutputStream();
        os.write(bytes);
        os.flush();
        os.close();
    }

    /**
     * 删除文件
     *
     * @param path 文件路径
     * @return
     */
    @GetMapping("/remove")
    @CrossOrigin
    public ResponseResult<?> removeFile(String path) {
        if (StrUtil.isBlank(path)) {
            return ResponseResult.success();
        }
        int lastIndex = path.lastIndexOf("/");
        String fileName = path.substring(lastIndex + 1);
        FileUtil.del(fileOperator.getFileUploadPath(fileName));
        return ResponseResult.success();
    }

    /**
     * 预览文件
     *
     * @param fileName 文件名
     * @param response
     * @throws Exception
     */
    @GetMapping("/preview/{fileName}")
    @CrossOrigin
    public void preview(@PathVariable String fileName, HttpServletResponse response) throws Exception {
        if (StrUtil.isBlank(fileName)) {
            return;
        }

        // pdf文件，返回浏览器预览请求头
        if (FileNameUtil.getSuffix(fileName).equalsIgnoreCase("pdf")) {
            String fileUploadPath = fileOperator.getFileUploadPath(fileName);
            response.setHeader("Content-Disposition", "inline;fileName=" + URLEncoder.encode(fileName, "UTF-8"));

            PDDocument document = PDDocument.load(new File(fileUploadPath));
            PDDocumentInformation documentInformation = document.getDocumentInformation();
            documentInformation.setTitle(fileName);
            document.setDocumentInformation(documentInformation);
            document.save(response.getOutputStream());
            return;
        }

        downloadFile(fileName, response);
    }
}
