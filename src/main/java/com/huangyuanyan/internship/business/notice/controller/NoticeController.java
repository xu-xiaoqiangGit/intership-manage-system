package com.huangyuanyan.internship.business.notice.controller;

import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.admin.vo.UserVO;
import com.huangyuanyan.internship.business.notice.dto.NoticeQuery;
import com.huangyuanyan.internship.business.notice.model.Notice;
import com.huangyuanyan.internship.business.notice.service.NoticeService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.util.SessionUtils;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-19 0:47
 */
@RestController
@RequestMapping("/notice")
@RequiredArgsConstructor
@Slf4j
public class NoticeController {

    private final NoticeService noticeService;

    @ApiOperation(value = "公告记录")
    @GetMapping("/list")
    public ResponseResult<List<Notice>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String noticeTitle,
            Integer status
    ) {
        // 设置公告列表查询条件
        NoticeQuery noticeQuery = new NoticeQuery();
        // 公告标题
        noticeQuery.setNoticeTitle(noticeTitle);
        // 公告状态
        noticeQuery.setStatus(status);

        Page<Notice> result = noticeService.list(new Page<>(page, limit), noticeQuery);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "系统公告")
    @GetMapping("/systemList")
    public ResponseResult<List<Notice>> list() {
        // 首页获取公告
        NoticeQuery noticeQuery = new NoticeQuery();
        // 只获取启用的公告
        noticeQuery.setStatus(1);

        Page<Notice> result = noticeService.list(new Page<>(1, 5), noticeQuery);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "公告详情")
    @GetMapping({"/list/{id}", "/systemList/{id}"})
    public ResponseResult<Notice> get(@PathVariable("id") Long id) {
        Notice notice = noticeService.getById(id);
        return ResponseResult.success(notice);
    }

    @ApiOperation(value = "新增公告")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Notice notice) {
        UserVO currentUser = SessionUtils.getCurrentUser();
        notice.setPublisherId(currentUser.getId());
        boolean result = noticeService.save(notice);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改公告")
    @PostMapping("/update")
    public ResponseResult<?> update(@RequestBody Notice notice) {
        boolean result = noticeService.updateById(notice);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除公告")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = noticeService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}
