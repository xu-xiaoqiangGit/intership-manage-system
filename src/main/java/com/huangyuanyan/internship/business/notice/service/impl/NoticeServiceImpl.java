package com.huangyuanyan.internship.business.notice.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.notice.dto.NoticeQuery;
import com.huangyuanyan.internship.business.notice.mapper.NoticeMapper;
import com.huangyuanyan.internship.business.notice.model.Notice;
import com.huangyuanyan.internship.business.notice.service.NoticeService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 20233-12-19 0:44
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Override
    public Page<Notice> list(Page<Notice> page, NoticeQuery noticeQuery) {
        List<Notice> notices = this.baseMapper.list(page, noticeQuery);
        page.setRecords(notices);
        return page;
    }
}
