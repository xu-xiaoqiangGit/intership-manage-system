package com.huangyuanyan.internship.business.notice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.notice.dto.NoticeQuery;
import com.huangyuanyan.internship.business.notice.model.Notice;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-19 0:44
 */
public interface NoticeService extends IService<Notice> {

    /**
     * 分页查询公告列表
     *
     * @param page
     * @param noticeQuery 查询条件
     * @return
     */
    Page<Notice> list(Page<Notice> page, NoticeQuery noticeQuery);
}
