package com.huangyuanyan.internship.business.notice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.notice.dto.NoticeQuery;
import com.huangyuanyan.internship.business.notice.model.Notice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-19 0:44
 */
public interface NoticeMapper extends BaseMapper<Notice> {

    List<Notice> list(Page<Notice> page, @Param("noticeQuery") NoticeQuery noticeQuery);
}
