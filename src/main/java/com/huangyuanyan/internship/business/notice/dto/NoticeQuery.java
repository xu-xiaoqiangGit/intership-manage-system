package com.huangyuanyan.internship.business.notice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description 公告列表查询条件
 * @Author huangyuanyan
 * @Date Created in 2023-12-21 23:37
 */
@Data
@ApiModel("公告列表查询条件")
public class NoticeQuery {

    @ApiModelProperty("公告标题")
    private String noticeTitle;

    @ApiModelProperty("公告状态")
    private Integer status;
}
