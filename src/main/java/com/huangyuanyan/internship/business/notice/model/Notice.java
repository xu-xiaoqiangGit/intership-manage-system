package com.huangyuanyan.internship.business.notice.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-12-19 0:42
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("notice")
@ApiModel("公告表实体类")
public class Notice extends BaseEntity {

    @ApiModelProperty("公告标题")
    private String noticeTitle;

    @ApiModelProperty("公告海报")
    private String noticeBanner;

    @ApiModelProperty("公告内容")
    private String noticeContent;

    @ApiModelProperty("发布人id")
    private Long publisherId;

    @ApiModelProperty("接收对象 0-全体, 1-学生, 2-教师, 3-企业")
    private Integer recipient;

    @ApiModelProperty("公告状态（0停用 1正常）")
    private Integer status;

    @ApiModelProperty("发布人名字")
    @TableField(exist = false)
    private String publisherName;
}
