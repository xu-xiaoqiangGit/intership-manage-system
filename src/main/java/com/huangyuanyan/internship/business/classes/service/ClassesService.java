package com.huangyuanyan.internship.business.classes.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.classes.model.Classes;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 16:12
 */
public interface ClassesService extends IService<Classes> {

    /**
     * 分页查询班级列表
     *
     * @param page
     * @param classes
     * @return
     */
    Page<Classes> list(Page<Classes> page, Classes classes);

    /**
     * 通过编号查询
     *
     * @param classesCode 班级编号
     * @return
     */
    Classes selectByCode(String classesCode);
}
