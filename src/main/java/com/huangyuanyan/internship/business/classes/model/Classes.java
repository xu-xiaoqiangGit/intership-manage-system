package com.huangyuanyan.internship.business.classes.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 16:11
 */
@ApiModel("班级表实体类")
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("classes")
public class Classes extends BaseEntity {

    @ApiModelProperty("班级名称")
    private String classesName;

    @ApiModelProperty("班级编号")
    private String classesCode;
}
