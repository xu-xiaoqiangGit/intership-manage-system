package com.huangyuanyan.internship.business.classes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.classes.model.Classes;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 16:13
 */
public interface ClassesMapper extends BaseMapper<Classes> {

    /**
     * 自定义查询班级列表sql
     * @param page
     * @param classes
     * @return
     */
    List<Classes> list(Page<Classes> page, @Param("classes") Classes classes);
}
