package com.huangyuanyan.internship.business.classes.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.classes.mapper.ClassesMapper;
import com.huangyuanyan.internship.business.classes.model.Classes;
import com.huangyuanyan.internship.business.classes.service.ClassesService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 16:13
 */
@Service
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, Classes> implements ClassesService {

    @Override
    public Page<Classes> list(Page<Classes> page, Classes classes) {
        // 自定义sql查询
        List<Classes> classesList = this.baseMapper.list(page, classes);
        page.setRecords(classesList);
        return page;
    }

    @Override
    public Classes selectByCode(String classesCode) {
        // 班级编号查询
        LambdaQueryWrapper<Classes> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Classes::getClassesCode, classesCode);
        return baseMapper.selectOne(queryWrapper);
    }
}
