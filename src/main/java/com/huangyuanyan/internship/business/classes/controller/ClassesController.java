package com.huangyuanyan.internship.business.classes.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.classes.model.Classes;
import com.huangyuanyan.internship.business.classes.service.ClassesService;
import com.huangyuanyan.internship.business.student.model.Student;
import com.huangyuanyan.internship.business.student.service.StudentService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BusinessException;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:35
 */
@RestController
@RequestMapping("/classes")
@RequiredArgsConstructor
@Slf4j
public class ClassesController {

    private final ClassesService classesService;
    private final StudentService studentService;

    @ApiOperation(value = "班级列表分页")
    @GetMapping("/list")
    public ResponseResult<List<Classes>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String classesName
    ) {
        // 设置班级名称查询条件
        Classes classes = new Classes();
        classes.setClassesName(classesName);
        Page<Classes> result = classesService.list(new Page<>(page, limit), classes);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "班级详情")
    @GetMapping("/list/{id}")
    public ResponseResult<Classes> get(@PathVariable("id") Long id) {
        // 通过主键id查询班级信息
        Classes classes = classesService.getById(id);
        return ResponseResult.success(classes);
    }

    @ApiOperation(value = "新增班级")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Classes classes) {
        Classes selectedByCode = classesService.selectByCode(classes.getClassesCode());
        // 班级编号需保持唯一，新增前判断编号是否存在
        if (null != selectedByCode) {
            throw new BusinessException("班级编号[" + classes.getClassesCode() + "]已存在，请重新填写");
        }
        boolean result = classesService.save(classes);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改班级")
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody Classes classes) {
        Classes selectedByCode = classesService.selectByCode(classes.getClassesCode());
        // 班级编号需保持唯一，修改前判断编号是否已存在
        if (null != selectedByCode && !selectedByCode.getId().equals(classes.getId())) {
            throw new BusinessException("班级编号[" + classes.getClassesCode() + "]已存在，请重新填写");
        }
        boolean result = classesService.updateById(classes);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除班级")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        // 参数校验id不能为空
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        // 通过班级主键id查询班级下的学生
        List<Student> students = studentService.listByClassesIds(Arrays.asList(ids));
        // 班级还有学生信息，不允许删除
        if (CollUtil.isNotEmpty(students)) {
            throw new BusinessException("该学生下存在学生信息，不能删除");
        }

        Classes dbClasses = classesService.getById(ids[0]);
        // 数据库中不存在该班级信息
        if (Objects.isNull(dbClasses)) {
            throw new BusinessException("班级信息已被删除，无需重复操作");
        }
        // 修改要删除的班级编号，避免占用班级编号唯一索引
        dbClasses.setClassesCode(IdUtil.simpleUUID());
        classesService.updateById(dbClasses);

        boolean result = classesService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}
