package com.huangyuanyan.internship.business.login.controller;

import cn.dev33.satoken.secure.SaSecureUtil;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import com.huangyuanyan.internship.business.admin.model.User;
import com.huangyuanyan.internship.business.admin.service.PermissionService;
import com.huangyuanyan.internship.business.admin.service.UserService;
import com.huangyuanyan.internship.business.admin.vo.UserVO;
import com.huangyuanyan.internship.business.enterprise.model.Enterprise;
import com.huangyuanyan.internship.business.enterprise.service.EnterpriseService;
import com.huangyuanyan.internship.business.login.dto.LoginDTO;
import com.huangyuanyan.internship.business.login.model.OAuthToken;
import com.huangyuanyan.internship.business.student.model.Student;
import com.huangyuanyan.internship.business.student.service.StudentService;
import com.huangyuanyan.internship.business.teacher.model.Teacher;
import com.huangyuanyan.internship.business.teacher.service.TeacherService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.constant.AuthConstant;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.BaseException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * @Author: huangyuanyan
 * @Description:
 * @Date Created in 2023-11-09 21:18
 */
@Api(tags = "认证中心")
@RestController
@RequestMapping("/login")
@Slf4j
@RequiredArgsConstructor
public class LoginController {

    private final UserService userService;
    private final PermissionService permissionService;
    private final StudentService studentService;
    private final EnterpriseService enterpriseService;
    private final TeacherService teacherService;

    @ApiOperation(value = "登录")
    @PostMapping
    public ResponseResult<OAuthToken> login(@RequestBody LoginDTO loginDTO) {
        User user = userService.getByUsername(loginDTO.getUsername());
        if (null == user) {
            throw new BaseException("该账户不存在！");
        }
        if (!user.getStatus().equals(GlobalConstant.STATUS_YES)) {
            throw new BaseException("该账户已被禁用！");
        }

        String dePassword = SaSecureUtil.aesDecrypt(AuthConstant.ENCRYPT_KEY, user.getPassword());
        if (!StringUtils.equals(dePassword, loginDTO.getPassword())) {
            throw new BaseException("用户名或密码错误，请检查");
        }

        // 登录
        StpUtil.login(user.getId());

        // 查询当前用户信息
        UserVO userVO = getCurrentUser().getData();

        // 将用户信息放入session（便于后续随时随地获取）
        StpUtil.getSession().set(AuthConstant.USER_INFO, userVO);
        String tokenValue = StpUtil.getTokenInfo().getTokenValue();
        return ResponseResult.success(new OAuthToken(tokenValue));
    }

    @ApiOperation("获取当前登陆的用户信息")
    @GetMapping("/me")
    public ResponseResult<UserVO> getCurrentUser() {
        UserVO userVO = new UserVO();

        // 1.用户基本信息
        Long userId = StpUtil.getLoginIdAsLong();
        User user = userService.getById(userId);
        BeanUtil.copyProperties(user, userVO);

        // 2.用户角色信息
        List<String> roles = StpUtil.getRoleList();
        userVO.setRoles(roles);

        // 3.用户按钮权限信息
        List<String> btnPermsByRoles = permissionService.listBtnPermsByRoles(roles);
        userVO.setPerms(btnPermsByRoles);

        // 设置用户学生信息
        if (Objects.nonNull(user.getStudentCode())) {
            Student student = studentService.getByCode(user.getStudentCode());
            userVO.setStudent(student);
        }

        // 设置用户企业信息
        if (Objects.nonNull(user.getEnterpriseId())) {
            Enterprise enterprise = enterpriseService.getById(user.getEnterpriseId());
            userVO.setEnterprise(enterprise);
        }

        // 设置用户教师信息
        if (Objects.nonNull(user.getTeacherId())) {
            Teacher teacher = teacherService.getById(user.getTeacherId());
            userVO.setTeacher(teacher);
        }

        return ResponseResult.success(userVO);
    }

    @ApiOperation(value = "注销")
    @PostMapping("/logout")
    public ResponseResult<String> logout() {
        UserVO userVO = getCurrentUser().getData();
        StpUtil.logout(userVO.getId());
        return ResponseResult.success("注销成功");
    }
}
