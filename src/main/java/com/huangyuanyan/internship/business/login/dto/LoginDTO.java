package com.huangyuanyan.internship.business.login.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description 登录参数
 * @Author huangyuanyan
 * @Date Created in 2023-11-07 0:19
 */
@Data
public class LoginDTO {

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;
}
