package com.huangyuanyan.internship.business.login.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OAuthToken {

    @ApiModelProperty("访问令牌")
    private String access_token;

    public OAuthToken accessToken(String access_token){
        this.access_token = access_token;
        return this;
    }
}
