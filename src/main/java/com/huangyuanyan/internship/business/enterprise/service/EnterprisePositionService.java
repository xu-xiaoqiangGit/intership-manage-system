package com.huangyuanyan.internship.business.enterprise.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.enterprise.model.EnterprisePosition;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-16 0:07
 */
public interface EnterprisePositionService extends IService<EnterprisePosition> {

    /**
     * 分页查询企业岗位列表
     *
     * @param page
     * @param enterprise
     * @return
     */
    Page<EnterprisePosition> list(Page<EnterprisePosition> page, EnterprisePosition enterprise);

    /**
     * 停止岗位招聘
     *
     * @param id id
     * @return
     */
    Boolean stop(Long id);

    /**
     * 根据企业id查询
     *
     * @param enterpriseIds 企业id
     * @return
     */
    List<EnterprisePosition> listByEnterpriseIds(List<Long> enterpriseIds);
}
