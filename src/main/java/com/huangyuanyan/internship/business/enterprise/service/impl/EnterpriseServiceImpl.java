package com.huangyuanyan.internship.business.enterprise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.enterprise.mapper.EnterpriseMapper;
import com.huangyuanyan.internship.business.enterprise.model.Enterprise;
import com.huangyuanyan.internship.business.enterprise.service.EnterpriseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-14 0:11
 */
@Service
public class EnterpriseServiceImpl extends ServiceImpl<EnterpriseMapper, Enterprise> implements EnterpriseService {

    @Override
    public Page<Enterprise> list(Page<Enterprise> page, Enterprise enterprise) {
        List<Enterprise> teachers = this.baseMapper.list(page, enterprise);
        page.setRecords(teachers);
        return page;
    }

    @Override
    public Enterprise getByEnterpriseName(String enterpriseName) {
        // 企业名称查询
        LambdaQueryWrapper<Enterprise> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Enterprise::getEnterpriseName, enterpriseName);
        return baseMapper.selectOne(queryWrapper);
    }
}
