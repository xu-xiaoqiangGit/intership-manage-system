package com.huangyuanyan.internship.business.enterprise.controller;

import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.enterprise.enums.PositionStatus;
import com.huangyuanyan.internship.business.enterprise.enums.SalaryRange;
import com.huangyuanyan.internship.business.enterprise.model.EnterprisePosition;
import com.huangyuanyan.internship.business.enterprise.service.EnterprisePositionService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BaseException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:35
 */
@RestController
@RequestMapping("/enterprisePosition")
@RequiredArgsConstructor
@Slf4j
public class EnterprisePositionController {

    private final EnterprisePositionService enterprisePositionService;

    @ApiOperation(value = "企业岗位列表分页")
    @GetMapping("/list")
    public ResponseResult<List<EnterprisePosition>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String positionName,
            Long enterpriseId,
            Integer salaryQuery,
            String positionCity
    ) {
        // 设置企业岗位查询条件
        EnterprisePosition enterprisePosition = new EnterprisePosition();
        enterprisePosition.setPositionName(positionName);
        enterprisePosition.setEnterpriseId(enterpriseId);
        enterprisePosition.setPositionCity(positionCity);

        // 如果当前用户是学生，设置学生id查询，即只能查询属于该学生的企业岗位数据
        Optional.ofNullable(SessionUtils.getCurrentUser().getStudent()).ifPresent(student -> {
            enterprisePosition.setStudentId(student.getId());
        });

        Optional.ofNullable(salaryQuery).ifPresent(salary -> {
            // 设置工资区间查询
            SalaryRange salaryRange = SalaryRange.fromValue(salary);
            enterprisePosition.setSalaryStart(salaryRange.getSalaryStart());
            enterprisePosition.setSalaryMax(salaryRange.getSalaryMax());
        });

        Page<EnterprisePosition> result = enterprisePositionService.list(new Page<>(page, limit), enterprisePosition);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "企业岗位详情")
    @GetMapping("/list/{id}")
    public ResponseResult<EnterprisePosition> get(@PathVariable("id") Long id) {
        EnterprisePosition position = enterprisePositionService.getById(id);
        SalaryRange salaryRange = SalaryRange.rangeOf(position.getSalaryStart(), position.getSalaryMax());
        position.setSalary(salaryRange.getValue());
        return ResponseResult.success(position);
    }

    @ApiOperation(value = "新增企业岗位")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody EnterprisePosition enterprisePosition) {
        SalaryRange salaryRange = SalaryRange.fromValue(enterprisePosition.getSalary());

        enterprisePosition.setSalaryStart(salaryRange.getSalaryStart());
        enterprisePosition.setSalaryMax(salaryRange.getSalaryMax());
        boolean result = enterprisePositionService.save(enterprisePosition);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改企业岗位")
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody EnterprisePosition enterprisePosition) {
        SalaryRange salaryRange = SalaryRange.fromValue(enterprisePosition.getSalary());

        enterprisePosition.setSalaryStart(salaryRange.getSalaryStart());
        enterprisePosition.setSalaryMax(salaryRange.getSalaryMax());
        boolean result = enterprisePositionService.updateById(enterprisePosition);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除企业岗位")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = enterprisePositionService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "停止岗位")
    @PostMapping("/stopPosition")
    public ResponseResult<?> stopPosition(@RequestBody Long id) {
        Asserts.failed(Objects.isNull(id), ResultCode.PARAM_IS_NULL);
        EnterprisePosition position = enterprisePositionService.getById(id);
        if (PositionStatus.ifStop(position.getPositionStatus())) {
            throw new BaseException("当前岗位已停止，请勿重复操作");
        }

        boolean result = enterprisePositionService.stop(id);
        return ResponseResult.judge(result);
    }
}
