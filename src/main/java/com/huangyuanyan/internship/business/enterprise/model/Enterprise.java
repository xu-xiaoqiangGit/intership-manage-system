package com.huangyuanyan.internship.business.enterprise.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description 企业表实体类
 * @Author huangyuanyan
 * @Date Created in 2023-11-14 0:09
 */
@ApiModel("企业表实体类")
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("enterprise")
public class Enterprise extends BaseEntity {

    @ApiModelProperty("企业名称")
    private String enterpriseName;

    @ApiModelProperty("行业")
    private String industry;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("企业logo")
    private String enterpriseLogo;

    @ApiModelProperty("联系方式")
    private String phoneNumber;
}
