package com.huangyuanyan.internship.business.enterprise.enums;

import com.huangyuanyan.internship.common.exception.BaseException;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * @Description 薪资区间
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 10:50
 */
@Getter
public enum SalaryRange {
    _3K_UNDER(0, "3K以下", BigDecimal.ZERO, new BigDecimal("3000")),
    _3K_5K(1, "3-5K", new BigDecimal("3000"), new BigDecimal("5000")),
    _5K_10K(2, "5-10K", new BigDecimal("5000"), new BigDecimal("10000")),
    _10K_20K(3, "10-20K", new BigDecimal("10000"), new BigDecimal("20000")),
    _20K_OVER(4, "20K以上", new BigDecimal("20000"), new BigDecimal("100000")),
    ;

    /**
     * 枚举值
     */
    private final Integer value;
    /**
     * 枚举说明
     */
    private final String desc;
    /**
     * 薪资区间最小值
     */
    private final BigDecimal salaryStart;
    /**
     * 薪资区间最大值
     */
    private final BigDecimal salaryMax;

    SalaryRange(Integer value, String desc, BigDecimal salaryStart, BigDecimal salaryMax) {
        this.value = value;
        this.desc = desc;
        this.salaryStart = salaryStart;
        this.salaryMax = salaryMax;
    }

    /**
     * 通过薪资枚举值获取枚举
     * @param value
     * @return
     */
    public static SalaryRange fromValue(Integer value) {
        return Arrays.stream(values())
                .filter(salaryRange -> salaryRange.value.equals(value))
                .findFirst()
                .orElseThrow(() -> new BaseException("薪资范围无效"));
    }

    /**
     * 通过薪资区间获取枚举
     * @param salaryStart
     * @param salaryMax
     * @return
     */
    public static SalaryRange rangeOf(BigDecimal salaryStart, BigDecimal salaryMax) {
        return Arrays.stream(values())
                .filter(salaryRange -> {
                    return salaryRange.salaryStart.compareTo(salaryStart) == 0
                            && salaryRange.salaryMax.compareTo(salaryMax) == 0;
                })
                .findFirst()
                .orElseThrow(() -> new BaseException("薪资范围无效"));
    }
}
