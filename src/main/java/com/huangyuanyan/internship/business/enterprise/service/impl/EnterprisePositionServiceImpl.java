package com.huangyuanyan.internship.business.enterprise.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.enterprise.enums.PositionStatus;
import com.huangyuanyan.internship.business.enterprise.mapper.EnterprisePositionMapper;
import com.huangyuanyan.internship.business.enterprise.model.EnterprisePosition;
import com.huangyuanyan.internship.business.enterprise.service.EnterprisePositionService;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.BusinessException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-16 0:08
 */
@Service
public class EnterprisePositionServiceImpl extends ServiceImpl<EnterprisePositionMapper, EnterprisePosition> implements EnterprisePositionService {

    @Override
    public Page<EnterprisePosition> list(Page<EnterprisePosition> page, EnterprisePosition enterprisePosition) {
        // 如果当前用户是企业角色，设置企业主键id查询，即只能查询属于该企业的企业岗位数据
        if (StpUtil.getRoleList().contains(GlobalConstant.ENTERPRISE_ROLE)) {
            Long enterpriseId = SessionUtils.getCurrentUser().getEnterpriseId();
            if (Objects.isNull(enterpriseId)) {
                throw new BusinessException("您还未绑定企业信息，请前往个人中心进行绑定");
            }
            enterprisePosition.setEnterpriseId(enterpriseId);
        }

        // 自定义sql查询
        List<EnterprisePosition> positions = this.baseMapper.list(page, enterprisePosition);

        // 如果当前用户是学生，设置学生简历投递状态
        Optional.ofNullable(SessionUtils.getCurrentUser().getStudent()).ifPresent(student -> {
            positions.stream().filter(ep -> Objects.equals(student.getId(), ep.getStudentId()))
                    .forEach(ep -> ep.setApplied(Boolean.TRUE));
        });
        page.setRecords(positions);
        return page;
    }

    @Override
    public Boolean stop(Long id) {
        EnterprisePosition forUpdate = new EnterprisePosition();
        forUpdate.setId(id);
        // 设置岗位招聘状态已停止
        forUpdate.setPositionStatus(PositionStatus.STOP.getValue());
        return updateById(forUpdate);
    }

    @Override
    public List<EnterprisePosition> listByEnterpriseIds(List<Long> enterpriseIds) {
        LambdaQueryWrapper<EnterprisePosition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(EnterprisePosition::getEnterpriseId, enterpriseIds);
        return baseMapper.selectList(queryWrapper);
    }
}
