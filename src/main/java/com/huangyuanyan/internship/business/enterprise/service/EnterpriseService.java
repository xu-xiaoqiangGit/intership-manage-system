package com.huangyuanyan.internship.business.enterprise.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.enterprise.model.Enterprise;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-14 0:10
 */
public interface EnterpriseService extends IService<Enterprise> {

    /**
     * 分页查询企业列表
     *
     * @param page
     * @param enterprise
     * @return
     */
    Page<Enterprise> list(Page<Enterprise> page, Enterprise enterprise);

    /**
     * 通过企业名称查询
     *
     * @param enterpriseName 企业名称
     * @return 企业信息
     */
    Enterprise getByEnterpriseName(String enterpriseName);
}
