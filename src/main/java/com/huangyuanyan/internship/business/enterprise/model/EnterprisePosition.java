package com.huangyuanyan.internship.business.enterprise.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @Description 企业岗位表实体类
 * @Author huangyuanyan
 * @Date Created in 2023-11-15 22:58
 */
@ApiModel("企业岗位表实体类")
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("enterprise_position")
public class EnterprisePosition extends BaseEntity {

    @ApiModelProperty("岗位名称")
    private String positionName;

    @ApiModelProperty("企业主键Id")
    private Long enterpriseId;

    @ApiModelProperty("起薪")
    private BigDecimal salaryStart;

    @ApiModelProperty("最高薪")
    private BigDecimal salaryMax;

    @ApiModelProperty("岗位描述")
    private String positionDescription;

    @ApiModelProperty("岗位城市")
    private String positionCity;

    @ApiModelProperty("岗位地址")
    private String positionAddress;

    @ApiModelProperty("联系方式")
    private String phoneNumber;

    @ApiModelProperty("岗位福利")
    private String welfare;

    @ApiModelProperty("岗位状态（0-已停止, 1-招聘中）")
    private Integer positionStatus;

    @TableField(exist = false)
    private Integer salary;

    @TableField(exist = false)
    private String enterpriseName;

    @TableField(exist = false)
    private String enterpriseLogo;

    @TableField(exist = false)
    private Long studentId;

    /**
     * 已投递
     */
    @TableField(exist = false)
    private Boolean applied = false;

    /**
     * 投递回复
     */
    @TableField(exist = false)
    private String auditReply;

    /**
     * 投递状态
     */
    @TableField(exist = false)
    private Integer auditStatus;
}
