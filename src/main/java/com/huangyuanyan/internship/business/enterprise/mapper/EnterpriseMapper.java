package com.huangyuanyan.internship.business.enterprise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.enterprise.model.Enterprise;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-14 0:11
 */
public interface EnterpriseMapper extends BaseMapper<Enterprise> {

    List<Enterprise> list(Page<Enterprise> page, @Param("enterprise") Enterprise enterprise);
}
