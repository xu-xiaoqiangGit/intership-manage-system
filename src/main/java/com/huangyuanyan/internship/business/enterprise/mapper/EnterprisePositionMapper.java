package com.huangyuanyan.internship.business.enterprise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.enterprise.model.EnterprisePosition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-16 0:08
 */
public interface EnterprisePositionMapper extends BaseMapper<EnterprisePosition> {

    List<EnterprisePosition> list(Page<EnterprisePosition> page, @Param("enterprisePosition") EnterprisePosition enterprisePosition);
}
