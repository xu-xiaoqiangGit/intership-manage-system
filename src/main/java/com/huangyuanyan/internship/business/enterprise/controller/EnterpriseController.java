package com.huangyuanyan.internship.business.enterprise.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.enterprise.model.Enterprise;
import com.huangyuanyan.internship.business.enterprise.model.EnterprisePosition;
import com.huangyuanyan.internship.business.enterprise.service.EnterprisePositionService;
import com.huangyuanyan.internship.business.enterprise.service.EnterpriseService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BusinessException;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:35
 */
@RestController
@RequestMapping("/enterprise")
@RequiredArgsConstructor
@Slf4j
public class EnterpriseController {

    private final EnterpriseService enterpriseService;
    private final EnterprisePositionService enterprisePositionService;

    @ApiOperation(value = "企业列表分页")
    @GetMapping("/list")
    public ResponseResult<List<Enterprise>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String enterpriseName,
            String industry,
            String address
    ) {
        // 设置企业列表查询条件
        Enterprise enterprise = new Enterprise();
        enterprise.setEnterpriseName(enterpriseName);
        enterprise.setIndustry(industry);
        enterprise.setAddress(address);
        Page<Enterprise> result = enterpriseService.list(new Page<>(page, limit), enterprise);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "企业详情")
    @GetMapping("/list/{id}")
    public ResponseResult<Enterprise> get(@PathVariable("id") Long id) {
        Enterprise enterprise = enterpriseService.getById(id);
        return ResponseResult.success(enterprise);
    }

    @ApiOperation(value = "新增企业")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Enterprise enterprise) {
        boolean result = enterpriseService.save(enterprise);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改企业")
    @PutMapping({"/update", "/personalUpdate"})
    public ResponseResult<?> update(@RequestBody Enterprise enterprise) {
        boolean result = enterpriseService.updateById(enterprise);
        return ResponseResult.judge(result);
    }


    @ApiOperation(value = "删除企业")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        List<EnterprisePosition> enterprisePositions = enterprisePositionService.listByEnterpriseIds(Arrays.asList(ids));
        if (CollUtil.isNotEmpty(enterprisePositions)) {
            throw new BusinessException("该企业下存在岗位信息，不能删除");
        }
        Enterprise dbEnterprise = enterpriseService.getById(ids[0]);
        if (Objects.isNull(dbEnterprise)) {
            throw new BusinessException("企业信息已被删除，无需重复操作");
        }
        dbEnterprise.setEnterpriseName(IdUtil.simpleUUID());
        enterpriseService.updateById(dbEnterprise);

        boolean result = enterpriseService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }
}
