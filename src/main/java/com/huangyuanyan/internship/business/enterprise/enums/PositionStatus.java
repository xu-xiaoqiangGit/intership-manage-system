package com.huangyuanyan.internship.business.enterprise.enums;

/**
 * @Description 岗位状态
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 11:53
 */
public enum PositionStatus {

    /**
     * 已停止
     */
    STOP(0),
    /**
     * 进行中
     */
    INProgress(1);

    private final Integer value;

    PositionStatus(Integer value) {
        this.value = value;
    }

    public static boolean ifStop(Integer value) {
        return STOP.value.equals(value);
    }

    public Integer getValue() {
        return value;
    }
}
