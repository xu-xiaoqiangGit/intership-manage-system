package com.huangyuanyan.internship.business.student.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.student.model.StudentResume;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 17:00
 */
public interface StudentResumeService extends IService<StudentResume> {

    /**
     * 分页查询学生简历列表
     *
     * @param page
     * @param studentResume 查询条件
     * @return
     */
    Page<StudentResume> list(Page<StudentResume> page, StudentResume studentResume);
}
