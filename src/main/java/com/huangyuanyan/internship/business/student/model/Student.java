package com.huangyuanyan.internship.business.student.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("student")
@ApiModel("学生表实体类")
public class Student extends BaseEntity {

    @ApiModelProperty("学生姓名")
    private String studentName;

    @ApiModelProperty("学生编号")
    private String studentCode;

    @ApiModelProperty("性别：1-男,2-女")
    private Integer gender;

    @ApiModelProperty("班级表主键id")
    private Long classesId;

    @ApiModelProperty("电话号码")
    private String phoneNumber;

    @ApiModelProperty("专业")
    private String major;

    @ApiModelProperty("毕业年份")
    private String graduationYear;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("家庭地址")
    private String address;

    /*****以下字段用于前端展示******/
    @ApiModelProperty("班级名称")
    @TableField(exist = false)
    private String classesName;

    @ApiModelProperty("班级表主键id")
    @TableField(exist = false)
    private List<Long> classesIds;

}
