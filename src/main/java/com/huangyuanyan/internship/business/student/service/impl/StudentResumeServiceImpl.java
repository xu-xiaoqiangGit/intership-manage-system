package com.huangyuanyan.internship.business.student.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.student.mapper.StudentResumeMapper;
import com.huangyuanyan.internship.business.student.model.StudentResume;
import com.huangyuanyan.internship.business.student.service.StudentResumeService;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.BusinessException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 17:00
 */
@Service
public class StudentResumeServiceImpl extends ServiceImpl<StudentResumeMapper, StudentResume> implements StudentResumeService {

    @Override
    public Page<StudentResume> list(Page<StudentResume> page, StudentResume studentResume) {
        LambdaQueryWrapper<StudentResume> queryWrapper = new LambdaQueryWrapper<>();

        List<String> roleList = StpUtil.getRoleList();
        // 如果当前登录用户是学生，设置学生表主键id查询，即学生只能查看自己的简历
        if (roleList.contains(GlobalConstant.STUDENT_ROLE)) {
            Long studentId = SessionUtils.getCurrentUser().getStudentId();
            if (Objects.isNull(studentId)) {
                throw new BusinessException("您还未绑定学生信息，请前往个人中心进行绑定");
            }
            queryWrapper.eq(StudentResume::getStudentId, studentId);
        }

        queryWrapper.like(StrUtil.isNotBlank(studentResume.getResumeName()), StudentResume::getResumeName, studentResume.getResumeName());
        return this.baseMapper.selectPage(page, queryWrapper);
    }
}
