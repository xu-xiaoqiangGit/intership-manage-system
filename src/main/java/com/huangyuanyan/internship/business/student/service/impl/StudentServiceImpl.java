package com.huangyuanyan.internship.business.student.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huangyuanyan.internship.business.student.mapper.StudentMapper;
import com.huangyuanyan.internship.business.student.model.Student;
import com.huangyuanyan.internship.business.student.service.StudentService;
import com.huangyuanyan.internship.business.teacher.model.Teacher;
import com.huangyuanyan.internship.business.teacher.service.TeacherService;
import com.huangyuanyan.internship.common.constant.GlobalConstant;
import com.huangyuanyan.internship.common.exception.BaseException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:33
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {

    @Resource
    private TeacherService teacherService;

    @Override
    public Page<Student> list(Page<Student> page, Student student) {
        List<String> roleList = StpUtil.getRoleList();
        // 如果当前登录用户是教师，设置教师下的班级id查询，即只能查看属于自己班级的学生
        if (roleList.contains(GlobalConstant.TEACHER_ROLE)) {
            Long teacherId = SessionUtils.getCurrentUser().getTeacherId();
            if (Objects.isNull(teacherId)) {
                throw new BaseException("您还未绑定教师信息，请先前去个人中心绑定教师信息后再查看");
            }

            Teacher dbTeacher = teacherService.getById(teacherId);
            List<Long> classesIds = dbTeacher.splitClassesIds();
            student.setClassesIds(classesIds);
        }

        List<Student> studentList = this.baseMapper.list(page, student);
        page.setRecords(studentList);
        return page;
    }

    @Override
    public Student getByCode(String studentCode) {
        LambdaQueryWrapper<Student> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Student::getStudentCode, studentCode);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public List<Student> listByClassesIds(List<Long> classesIds) {
        LambdaQueryWrapper<Student> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Student::getClassesId, classesIds);
        return baseMapper.selectList(queryWrapper);
    }
}
