package com.huangyuanyan.internship.business.student.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huangyuanyan.internship.business.student.model.Student;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:33
 */
public interface StudentService extends IService<Student> {

    /**
     * 学生列表分页查询
     *
     * @param page
     * @param student 查询条件
     * @return
     */
    Page<Student> list(Page<Student> page, Student student);

    /**
     * 根据学生编号查询
     *
     * @param studentCode 学生编号
     * @return
     */
    Student getByCode(String studentCode);

    /**
     * 根据班级id查询
     *
     * @param classesIds 班级id
     * @return
     */
    List<Student> listByClassesIds(List<Long> classesIds);
}
