package com.huangyuanyan.internship.business.student.controller;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.student.model.Student;
import com.huangyuanyan.internship.business.student.service.StudentService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BusinessException;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:35
 */
@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
@Slf4j
public class StudentController {

    private final StudentService studentService;

    @ApiOperation(value = "学生列表分页")
    @GetMapping("/list")
    public ResponseResult<List<Student>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String studentName,
            String studentCode,
            Long classesId
    ) {
        // 设置查询条件
        Student student = new Student();
        // 学生姓名
        student.setStudentName(studentName);
        // 学生编号
        student.setStudentCode(studentCode);
        // 班级id
        student.setClassesId(classesId);
        Page<Student> result = studentService.list(new Page<>(page, limit), student);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "学生详情")
    @GetMapping("/list/{id}")
    public ResponseResult<Student> get(@PathVariable("id") Long id) {
        Student student = studentService.getById(id);
        return ResponseResult.success(student);
    }

    @ApiOperation(value = "新增学生")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody Student student) {
        Student selectedByCode = studentService.getByCode(student.getStudentCode());
        // 判断学生编号是否已存在
        if (null != selectedByCode) {
            throw new BusinessException("学生编号[" + student.getStudentCode() + "]已存在，请重新填写");
        }
        boolean result = studentService.save(student);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改学生")
    @PutMapping({"/update", "/personalUpdate"})
    public ResponseResult<?> update(@RequestBody Student student) {
        Student selectedByCode = studentService.getByCode(student.getStudentCode());
        // 判断学生编号是否已存在
        if (null != selectedByCode && !selectedByCode.getId().equals(student.getId())) {
            throw new BusinessException("学生编号[" + student.getStudentCode() + "]已存在，请重新填写");
        }
        boolean result = studentService.updateById(student);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除学生")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        Student dbStudent = studentService.getById(ids[0]);
        if (Objects.isNull(dbStudent)) {
            throw new BusinessException("学生信息已被删除，无需重复操作");
        }
        // 要删除的学生数据，学生编号设为随机值，避免占用唯一索引
        dbStudent.setStudentCode(IdUtil.simpleUUID());
        studentService.updateById(dbStudent);

        boolean result = studentService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }

}
