package com.huangyuanyan.internship.business.student.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.file.FileOperator;
import com.huangyuanyan.internship.business.student.model.StudentResume;
import com.huangyuanyan.internship.business.student.service.StudentResumeService;
import com.huangyuanyan.internship.common.api.ResponseResult;
import com.huangyuanyan.internship.common.api.ResultCode;
import com.huangyuanyan.internship.common.exception.Asserts;
import com.huangyuanyan.internship.common.exception.BusinessException;
import com.huangyuanyan.internship.common.util.SessionUtils;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:35
 */
@RestController
@RequestMapping("/studentResume")
@RequiredArgsConstructor
@Slf4j
public class StudentResumeController {

    private final StudentResumeService studentResumeService;
    private final FileOperator fileOperator;

    @ApiOperation(value = "学生简历列表分页")
    @GetMapping("/list")
    public ResponseResult<List<StudentResume>> list(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            String resumeName
    ) {
        // 设置查询条件
        StudentResume studentResume = new StudentResume();
        // 简历名称
        studentResume.setResumeName(resumeName);
        Page<StudentResume> result = studentResumeService.list(new Page<>(page, limit), studentResume);
        return ResponseResult.success(result.getRecords(), result.getTotal());
    }

    @ApiOperation(value = "学生简历详情")
    @GetMapping("/list/{id}")
    public ResponseResult<StudentResume> get(@PathVariable("id") Long id) {
        StudentResume studentResume = studentResumeService.getById(id);

        String resumeUrl = studentResume.getResumeUrl();

        String name = FileUtil.getName(resumeUrl);
        studentResume.setPreviewUrl(fileOperator.getPreviewUrl() + name);

        return ResponseResult.success(studentResume);
    }

    @ApiOperation(value = "新增学生简历")
    @PostMapping("/add")
    public ResponseResult<?> add(@RequestBody StudentResume studentResume) {
        Long studentId = SessionUtils.getCurrentUser().getStudentId();
        if (Objects.isNull(studentId)) {
            throw new BusinessException("您还未绑定学生信息，请前往个人中心进行绑定");
        }
        studentResume.setStudentId(studentId);
        boolean result = studentResumeService.save(studentResume);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "修改学生简历")
    @PutMapping("/update")
    public ResponseResult<?> update(@RequestBody StudentResume studentResume) {
        boolean result = studentResumeService.updateById(studentResume);
        return ResponseResult.judge(result);
    }

    @ApiOperation(value = "删除学生简历")
    @DeleteMapping("/delete")
    public ResponseResult<?> delete(@RequestBody Long[] ids) {
        Asserts.failed(ArrayUtil.isEmpty(ids), ResultCode.PARAM_IS_NULL);
        boolean result = studentResumeService.removeByIds(Arrays.asList(ids));
        return ResponseResult.judge(result);
    }

}
