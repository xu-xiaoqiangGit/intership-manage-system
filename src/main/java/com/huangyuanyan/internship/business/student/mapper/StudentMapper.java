package com.huangyuanyan.internship.business.student.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huangyuanyan.internship.business.student.model.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-12 16:34
 */
public interface StudentMapper extends BaseMapper<Student> {

    /**
     * 分页查询学生列表ssql
     *
     * @param page
     * @param student 查询条件
     * @return
     */
    List<Student> list(Page<Student> page, @Param("student") Student student);
}
