package com.huangyuanyan.internship.business.student.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huangyuanyan.internship.business.student.model.StudentResume;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 17:00
 */
public interface StudentResumeMapper extends BaseMapper<StudentResume> {
}
