package com.huangyuanyan.internship.business.student.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huangyuanyan.internship.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description
 * @Author huangyuanyan
 * @Date Created in 2023-11-18 16:02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("student_resume")
@ApiModel("学生简历表实体类")
public class StudentResume extends BaseEntity {

    @ApiModelProperty("简历名")
    private String resumeName;

    @ApiModelProperty("学生表主键id")
    private Long studentId;

    @ApiModelProperty("简历文件地址")
    private String resumeUrl;

    @ApiModelProperty("简历文件预览地址")
    @TableField(exist = false)
    private String previewUrl;
}
